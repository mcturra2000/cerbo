#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <ctype.h>


#include <iostream>
#include <string>
using namespace std;
#include<string.h>
#include <deque>


#include "tty.h"


std::tuple<std::string, bool> pull_line(int nc)
{
	using strings = std::deque<std::string>;
	static strings backlog;

	int width = 0;
	if(backlog.size() == 0 && !cin.eof()) {
		std::string line_in;
		getline(cin, line_in);
		if(cin.eof()) goto finis;
		string str;
		for(int i = 0; i < line_in.size(); i++) {
			unsigned char c = line_in[i];
			if(c == '\t')  {
				width = (width/8 + 1) *8;
			} else {
				width++;
			}

			str += c;
			// TODO deal with ansi escape sequences
			// TODO deal with unicode
			if(width>=nc) {
				backlog.push_back(str);
				str = "";
				width = 0;
			}
		}
		backlog.push_back(str);
	}
finis:
	if(backlog.empty()) return {"", true};
	string line = backlog.front();
	backlog.pop_front();
	return {line, false};
}


void main1()
{
	term t;
	t.noncanon();
	while(1) {
		auto [nr, nc] = t.size();
		for(int i =0 ; i < nr-1; i ++) {
			auto [line, eof] = pull_line(nc);
			//getline(cin, line);
			if(eof) goto finis;
			cout << line << endl;
		}

		cout << "--- More --- ";
		cout.flush();
		int c;
		read(t.fd, &c, 1);
		cout << endl;

	}
finis:
	return;
}

int main()
{
	try {main1();}
	catch (int e) {fprintf(stderr, "Caught error %d\n", e);}
}
