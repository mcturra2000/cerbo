#pragma once


#include <termios.h>
#include <tuple>

class term {
	public:
		term();
		~term();
		std::tuple<int, int> size();
		const int term_err = 65;
		void noncanon();

		struct termios term1, term0; // the current and original term
		int fd;

};
