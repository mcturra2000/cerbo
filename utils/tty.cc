#include "tty.h"
#include <unistd.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>



term::term()
{
	fd = open("/dev/tty", O_RDONLY);
	if(fd==-1) throw term_err;
	if(tcgetattr(fd, &term1) < 0) throw term_err; // what are the current attributes?
	term0 = term1; // create a copy for later restoration
}

// set the terminal in non-canonical mode
// which will make input on char-by-char basis, not line-by-line
void term::noncanon()
{
	term1.c_lflag &= ~ICANON; // don't wait for newline
	if(tcsetattr(fd, TCSANOW, &term1)) throw term_err; // set the attributes how we wish
}

term::~term()
{
	close(fd);
	tcsetattr(STDIN_FILENO, TCSANOW, &term0); //restore old terminal
}

std::tuple<int, int> term::size()
{
	struct winsize ws;
	ioctl(fd, TIOCGWINSZ, &ws);
	return { ws.ws_row, ws.ws_col};
}
