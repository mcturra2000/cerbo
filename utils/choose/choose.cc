#include <string>
#include <vector>
#include <fstream>
#include <iostream>
using namespace std;

#include <unistd.h>

//#include <tty.h>

typedef vector<string> strings;


int main(int argc, char *argv[])
{
	if(argc!= 2) {
		fprintf(stderr, "ERR: choose takes exactly one argument\n");
		exit(1);
	}

	ifstream ifs;
	ifs.open(argv[1], ifstream::in);
	if(!ifs.is_open()) {
		fprintf(stderr, "ERR: file not found\n");
		exit(1);
	}


		
	strings choices;
	//term t;


	string choice;
	int nc = 0; // number of choices
	while(getline(ifs, choice)) {
		printf("%3d %s\n", ++nc, choice.c_str());
		//cout << choice << endl;
		choices.push_back(choice);
	}

	
	//char buf[100];
	//int n = read(t.fd, buf, sizeof(buf));
	//buf[n]  = 0;
	//string buf1{buf};
	string buf1;
	getline(cin, buf1);
	string buf2;
	for(char c: buf1) {
		if(isspace(c)) continue; // exclude spaces, tabs, carriage returns, etc.
		buf2 += c;
	}

	try {
		int n = stoi(buf2);
		if(n>choices.size()) {
			return 1;
		}
		cout << choices[n-1];
	} catch (std::invalid_argument e) {
		return 1;
	}
}
