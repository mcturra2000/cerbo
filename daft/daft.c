#include <stdio.h>

int stack[100];
int sidx = 0;

char heap[1000];
int hidx = 0;


int pop(void) { return stack[--sidx]; }
void push(int val) { stack[sidx++] = val; }
void dup(void) { stack[sidx++] = stack[sidx-1]; }

int main()
{
	int c;
	while((c = getchar()) >=0) {
		switch(c) {
			case 'S':
				push(hidx);
				while((c = getchar()) != '\n') {
					heap[hidx++] = c;
				}
				heap[hidx++] = 0;
				break;
			case '2':
				dup();
				break;
			case 'T':
				puts(heap + pop());
				break;
			case '>':
				putchar(getchar());
				break;

		}

	}
	return 0;
}
