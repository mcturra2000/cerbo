#define _XOPEN_SOURCE 500
#include <dirent.h>
#include <ftw.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <array>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>

#include "dtree.pb.h"

namespace fs = std::filesystem;
using namespace std;

/*
 * there's around 3.3M files in the Feorda system, taking around 8s in clock time
 */


int node_num =0;
std::array<int, 100> parents; // array to hold parent nodes	     
fstream strm;
const char pb_file[] = "dtree.pb";
FileTree ft_out;


static int display_info(const char *fpath, const struct stat *sb,
		int tflag, struct FTW *ftwbuf)
{
	int level = ftwbuf->level;
	printf("%-3s %2d ",
			(tflag == FTW_D) ?   "d"   : (tflag == FTW_DNR) ? "dnr" :
			(tflag == FTW_DP) ?  "dp"  : (tflag == FTW_F) ?   "f" :
			(tflag == FTW_NS) ?  "ns"  : (tflag == FTW_SL) ?  "sl" :
			(tflag == FTW_SLN) ? "sln" : "???",
			level);

#if 0
	if (tflag == FTW_NS)
		printf("-------");
	else
		printf("%7jd", (intmax_t) sb->st_size);
#endif

	// take care of parenting. Seems OK
	int parent;
	if(tflag == FTW_D) parents.at(level) = node_num;
	if(node_num == 0) {
		parent = -1;
	} else {
		parent =parents.at(level - 1);
	}

	printf("   %7d   %7d", node_num, parent);

	printf("   %-40s %d %s\n",
			fpath, ftwbuf->base, fpath + ftwbuf->base);


	Node node;
	//node.nodetype =  1 ;
	if(tflag == FTW_D) 
		node.set_nodetype(Node_Type_DIR);
	else
		node.set_nodetype(Node_Type_FILE);
	node.set_num(node_num);
	node.set_parent(parent);
	node.set_level(level);
	node.set_name(fpath+ftwbuf->base);
	ft_out.add_nodes()->CopyFrom(node);

	node_num++;
	return 0;           /* To tell nftw() to continue */
}

void make()
{
	int flags = 0;

	//Node node;
#if 0
	if (argc > 2 && strchr(argv[2], 'd') != NULL)
		flags |= FTW_DEPTH;
	if (argc > 2 && strchr(argv[2], 'p') != NULL)
		flags |= FTW_PHYS;
#endif

	flags |= FTW_PHYS; // don't follow symbolic links, as that can cause failures due too many levels
	if (nftw("..", display_info, 20, flags) == -1)
	{
		perror("nftw");
		exit(EXIT_FAILURE);
	}

	strm.open(pb_file, ios::out | ios::binary);
	if (!ft_out.SerializeToOstream(&strm)) {
		cerr << "Failed to write protobuf data." << endl;
		exit(EXIT_FAILURE);
	}
	strm.close();
}

void unpack()
{
	cout << "\n\n----- UNPAKCING\n";
	FileTree ft_in;
	strm.open(pb_file, ios::in | ios::binary);
	if (!ft_in.ParseFromIstream(&strm)) {
		cerr << "Failed to parse protobuf data." << endl;
		exit(EXIT_FAILURE);
	}

	for(auto it = ft_in.nodes().begin(); it != ft_in.nodes().end(); ++it) {
		for(int i = 0; i< it->level(); i++) cout << "  ";
		cout << it->name() << "\n";
	}
	strm.close();

}

string times(const char* str, int times)
{
	string res;
	for(int i = 0; i< times; i++) res += str;
	return res;
}

void recurse(const string& path, int level)
{
	int idx = 1;
	using dent_t = std::filesystem::directory_entry;
	vector<dent_t> dents;
	for (auto const& dir_entry : std::filesystem::directory_iterator{path}) {
		string basename{dir_entry.path().filename().string()};
		if(dir_entry.is_directory()) {
			dents.push_back(dir_entry);
			std::cout << idx << "\t" ;
			cout << times(" ", 2*level) << basename << '\n';
			//if(dir_entry.is_directory()) 			recurse(dir_entry.path(), level + 1);
			idx++;
		} else {
			std::cout << "\t" ;
			string basename{dir_entry.path().filename().string()};
			cout << times(" ", 2*level) << basename << '\n';
		}

	}

	cout << "? ";
	string num;
	getline(cin, num);
	int inum = stoi(num) -1;
	auto path1{dents[inum].path()};
	recurse(path1, level+1);
}
int main(int argc, char *argv[])
{
	GOOGLE_PROTOBUF_VERIFY_VERSION;

	make();
	unpack();

	cout << "\n\n--- NEW ITERATION METHOD ---\n\n";
	recurse("..", 0); // new way

	exit(EXIT_SUCCESS);
}

