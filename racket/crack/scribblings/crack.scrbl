#lang scribble/manual
@(require (for-label racket))

   
@title{Crack: Carter's racket library}

2024-05-17 created


Blah blah blah. See trace.scrbl for example. 

To build this document:
@verbatim|{
raco setup -l crack
}|
from base directory


@(hyperlink "https://docs.racket-lang.org/scribble/base.html#%28def._%28%28lib._scribble%2Fbase..rkt%29._verbatim%29%29"
            "How to write documentation")

Display multiple values:
@racketblock[
(display*  1 2 3) ; => 123
]

Utils include: collect, while, growth, growth% looping, slurp-file, recv-list, ince, forn,

Percentage gain:
@racketblock[
(pc from to)
(pc 200 220) ; => 10.0
]

Mean:
@racketblock[
(mean 12 13 14) ; => 13.0
]
