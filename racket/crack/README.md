# crack

My library

Setup is shown in parent directory.


Usage:
(require crack/stats)
or for everything:
(require crack)


You can make the documentation by typing 
```
raco setup -l crack
```

## utils


Display multiple values
(display*  1 2 3) ; => 123





incr - increment variable, a la lisp


Bind list to variables (like receive in srfi/8, but with lists instead
; consider using match-define instead
(recv-list (a b) '( 4 5)
	(+ a b)) ; => 9

As you'd expect:
slurp-file file-name

### collect items into a list

(collect k
         (k 5)
         (k 6))
         
returns '(5 6)         

### forn - a for loop

Default step is 1, but you can set it to anything

```
(forn (i 1 5)
      (display "> ")
      (displayln i))
```
=>
```
> 1
> 2
> 3
> 4
> 5
```

whilst

```
(forn (i 10 2 #:step  -2)
      (display "*** ")
      (displayln i))
```
=>
```
*** 10
*** 8
*** 6
*** 4
*** 2
```      


### looping
```
(looping k
      (displayln 16)
      (displayln 17)
      (k 'bye)
      (displayln 19)
      #t)
```
Outputs:
```
16
17
```
but not 19. You specify k was the exit condition. You do not have to return a value if you don't want to.

      
### while

Example:
```
(define x 1)
(while (< x 5)
       (displayln x)
       (incr x))
```


## Status

2024-04-25	Created
