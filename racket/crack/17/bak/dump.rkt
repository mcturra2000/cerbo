#lang racket
(require sugar)
(require (for-syntax syntax/parse))

(define-syntax (x->string stx)
  (syntax-parse stx
    [(_ x)
     #'(->string 'x)])) 
