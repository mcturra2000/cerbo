%{
#include "tears.h"
//extern int yylineno;
//void yyerror(const char* s);
//extern int yylex (void);
//struct num_t;
%}



%union nvalue
{
	int ival;
        double dval;
	yrs_t* yrs;
        char* str;
	num_t* nums;
}

%token SEP
%token EPIC
%token YRS
%token SHRS
%token REVS
%token OPS
%token EPS
%token SEMI
%token NUM
%token <str> WORD

%type<dval> NUM 

%type<str> epic
%type<nums> number_list 
%type<nums>  shrs revs  ops eps

%type<ival> SHRS REVS OPS EPS YRS SEMI 
%type<yrs> yrs 

%%

top : share {  }
    | top SEP share {  } 
;

share :  epic yrs shrs revs ops eps { eval_share($1, $2, $3, $4, $5, $6); }

      
epic : EPIC WORD  { $$ = $2; } ;

yrs : YRS NUM NUM { $$ = mkyrs($2, $3); } ;

shrs : SHRS number_list SEMI { dbg_numlist($2); $$ = $2; } ;

revs : REVS number_list SEMI { $$ = $2; } ;

ops : OPS number_list SEMI { $$ = $2; } ;

eps : EPS number_list SEMI { $$ = $2; } ;

number_list : NUM { $$ =  mknum_list(0, $1); }
	    | number_list NUM { $$ = mknum_list($1, $2); } 
;

%%

