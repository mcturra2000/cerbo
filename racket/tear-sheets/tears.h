#pragma once

#ifdef __cplusplus

#include <numeric>
#include <cmath>
#include <memory>
#include <string>
#include <iostream>
#include <vector>

using namespace std;


extern "C" {
#endif

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct num_t {
	struct num_t* next;
	double val;
} num_t;

typedef struct {
	int from;
	int to;
} yrs_t;

#include "y.tab.h"

extern int yyleng;


extern int yylineno;
void yyerror(const char* s);
int yylex(void);        
extern char* yytext;
extern int column; // not yet implemented
extern int yywrap(void);
void found(const char* s);
void xfound(const char* s);
void mkname();
void mknum();
num_t* mknum_list(num_t* hd, double val);
void dbg_numlist(num_t* nums);
void eval_share(char* epic, yrs_t* yrs, num_t* shrs, num_t* revs, num_t* ops, num_t* eps);
void eval();
yrs_t* mkyrs(int from, int to);


#ifdef __cplusplus
}
#endif
	
