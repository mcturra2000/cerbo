#include "tears.h"

static void show_nums(num_t* nums, const string& title, double scale = 1.0)
{
	while(nums) {
		printf("%.1f\t", nums->val * scale);
		//cout << nums->val * scale << "\t";
		nums = nums->next;
	}
	cout << title << endl;
}

static void rescale(num_t* nums, num_t* shrs)
{
	while(nums) {
		nums->val = nums->val / shrs->val * 100;
		//cout << nums->val  << "\t";
		nums = nums->next;
		shrs = shrs->next;
	}
	//cout << title << endl;
}



// FN sum_t 
typedef struct {
	const char* epic;
	double stat[6];
} sum_t;


double growth(double arr[], int size, int duration)
{
	int i1 = size -1;
	int i2 = i1 - duration;
	if(i2<0) return NAN;

	double num = arr[i1];
	double denom = arr[i2];
	double val = pow(num/denom, 1.0/duration);
	val = 100.0 * (val -1.0);
	return val;
}

static vector<sum_t> sums;

void summarise(num_t *nums, sum_t& sum, int idx_offset)
{
	// turn a linked list into an array
	double arr[20]; // morre than enough space
	int size = 0;
	while(nums) {
		arr[size++] = nums->val;
		nums = nums->next;
	}

	sum.stat[idx_offset]   = growth(arr, size, 9);
	sum.stat[idx_offset+1] = growth(arr, size, 5);
}


static void mksummary(char* epic, num_t* revs, num_t* ops, num_t* eps)
{
	sum_t s;
	s.epic = epic;
	summarise(revs, s, 0);
	summarise(ops, s, 2);
	summarise(eps, s, 4);
	sums.push_back(s);
}

void eval_share(char* epic, yrs_t* yrs, num_t* shrs, num_t* revs, num_t* ops, num_t* eps)
{
	printf("epic: %s\n", epic);
	auto& y0 = yrs->from;
	auto& y1 = yrs->to;

	// print years
	for(int i = y0; i<= y1; i++) {
		cout << i << '\t';
	}
	cout << "yrs" << endl;

	show_nums(shrs, "shrs");
	show_nums(revs, "revs");
	show_nums(ops, "ops");
	show_nums(eps, "eps", 100.0);
	//puts("");

	puts("---per share");
	rescale(revs, shrs);
	show_nums(revs, "revs");
	rescale(ops, shrs);
	show_nums(ops, "ops");
	puts("");

	mksummary(epic, revs, ops, eps);
}

static void print_summary(const sum_t& s)
{
	for(int i = 0; i< 6; i++)
		printf("%.1f\t", s.stat[i]);
	cout << s.epic << endl;
}

void eval()
{

	cout << "*** SUMMARIES***\n";
	int nout = 0;
	for(const auto& s : sums) {
		if(nout ==0) cout  
			<< "revs		ops		eps\n" 
			<<	"9yrs	5yrs	9yrs	5yrs	9yrs	5yrs\n";
		if(++nout == 15) {  nout = 0; }
		print_summary(s);
		if(nout == 0) cout << endl;
	}

	// calculate mean and median
	puts("");
	sum_t means{(char*) u8"mean"};
	sum_t medians{(char*) u8"median"};
	for(int i=0; i<6; i++) {
		vector<double> vals;
		for(const auto& s : sums) {
			auto v = s.stat[i];
			if(!isnan(v))
				vals.push_back(v);
		}
		sort(vals.begin(), vals.end());
		auto sum = std::accumulate(vals.begin(), vals.end(), 0.0);
		means.stat[i]  =  sum / vals.size();

		double mid = vals.size() -1;
		int idx0 = floor(mid/2.0), idx1 = ceil(mid/2.0);
		medians.stat[i] = (vals[idx0] + vals[idx1])/ 2.0;
		//mean  =  sum / vals.size();
		//printf("%.1f\t", mean);
		//uto num = nums->
	}
	//printf("mean\n");
	print_summary(means);
	print_summary(medians);
}
