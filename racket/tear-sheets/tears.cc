#include "tears.h"

extern FILE *yyin;

//typedef unique_ptr<string> str_ptr;

void dbg_numlist(num_t* nums)
{
	return;
	static int count = 0;

	printf("\n*** dbg_numlist: %d\n", count++);

	while(nums) {
		cout << nums->val << ' ' ;
		nums = nums->next;
	}
	cout <<endl;

	//printf("TODO found num list\n");
}


yrs_t* mkyrs(int from, int to)
{
	yrs_t* p = (yrs_t*) malloc(sizeof(yrs_t));
	p->from = from;
	p->to = to;
	return p;
}

num_t* mknum_list(num_t* hd, double val)
{

	//assert(hd);
	num_t* p = (num_t*) malloc(sizeof(num_t));
	p->next = 0;
	p->val = val;
	if(hd==0) return p; // special case where we are making a new list

	// find tail of list
	num_t* tail = hd;
	while(tail->next) tail = tail->next;
	tail->next = p;

	return hd;
}
num_t* mknum_listXXX(num_t* next, double val)
{
	num_t node{next, val};
	num_t* p = (num_t*) malloc(sizeof(num_t));
	memcpy(p, &node, sizeof(num_t));
	yylval.nums = p;
	return p;
}

void mknum()
{
	string s;
	for(int i = 0; i < yyleng; i++)
		if(yytext[i] != ',') s += yytext[i];
	yylval.dval = stod(s);
	//printf("mknum: %f\n", yylval.d);
	
}

void mkname()
{
	assert(strlen(yytext) == yyleng);
	char* s = (char*) malloc(yyleng+1);
	strncpy(s, yytext, yyleng);
	s[yyleng] = 0;
	yylval.str = s;
	//unique_ptr<string>(yytext);
	//auto p = make_unique<string>(string{yytext});
}

void found(const char* s)
{
	printf("found:%s\n", s);
}
void xfound(const char* s) {}

void yyerror (const char* s)
{
	//printf("yyerror: %s\n", s);
	fflush(stdout);
	//printf("\n%*s\n%*s\n", column, "^", column, s);
	printf("Error line: %d\n", yylineno);
}

int yywrap (void)
{
	return(1);
}




int main (int argc, char **argv)
{
        FILE *fp = fopen("tears.txt", "r");
        if(!fp) {
                puts("Cannot open trans.txt. Aborting");
                exit(1);
        }
        yyin = fp;

        int status = yyparse();
        fclose(fp);
        if(status == 1) {
                puts("Syntax error. Aborting.");
                exit(1);
        }

	eval();

	return 0;
}
