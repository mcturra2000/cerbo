%option yylineno
%{
#include "tears.h"
%}

delim [ \t\n]
white {delim}+
numlike [0-9]|"."|","
number "-"?{numlike}+
word [a-z]+


%%

{white} { /* eat up whitespace */ }
"---"	{ return SEP; }
epic	{ xfound("epic"); return EPIC; }
yrs	{ xfound("yrs"); return YRS; }
shrs	{ return SHRS; }
revs	{ return REVS; }
ops	{ return OPS; }
eps	{ return EPS;}
";"	{ return SEMI; }
{number}	{ xfound("number"); mknum(); return NUM;} 
{word}	{ xfound("word") ; mkname(); return WORD; }

