#include "mainwindow.h"
#include "./ui_mainwindow.h"

//#include <QtMultimedia>

#include <string>

#include <QTimer>
using namespace std;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    set_labels();
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(set_labels()));
    //connect(timer, SIGNAL(timeout()), this, &set_labels);
    timer->start(1000);


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    /*
    static int count = 0;
    string x = to_string(count++);
    QString x1 = QString::fromStdString(x);
    ui->labelElapsed->setText(x1);
    ui->pushButton->setAutoFillBackground(1000);
*/
    //QMediaPlayer *player = new QMediaPlayer;
    //QApplication::beep();
    //system("/bin/sh -c 'tput bel'");
    //QApplication::systemSound(Qt::EventType::CriticalStop);
    //QApplication::
    if(started.has_value()) {
        started.reset();
        ui->pushButton->setText("Start");
    } else {
        auto now = std::chrono::system_clock::now();
        started = std::chrono::system_clock::to_time_t(now);
        ui->pushButton->setText("Stop");

    }
    set_labels();

}



void MainWindow::set_labels()
{
    auto now = std::chrono::system_clock::now();
    std::time_t nowt = std::chrono::system_clock::to_time_t(now);
    std::tm * ptm = std::localtime(&nowt);
    char buffer[32];
    // Format: Mo, 15.06.2009 20:20:00
    std::strftime(buffer, 32, "%a, %d.%m.%Y %H:%M:%S", ptm);
    std::strftime(buffer, 32, "%a %d %b", ptm);
    ui->labelDate->setText(buffer);

    std::strftime(buffer, 32, "%H:%M:%S", ptm);
    ui->labelTime->setText(buffer);

    int elapsed = nowt  - started.value_or(nowt);
    int secs = elapsed % 60;
    int mins = elapsed / 60;
    snprintf(buffer, 32, "%d:%02d",  mins, secs);
    ui->labelElapsed->setText(buffer);
    //ui->labelDate->update();
    //ui->labelDate->repaint();


}
