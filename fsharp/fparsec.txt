2025-02-17

https://www.youtube.com/watch?v=34C_7halqGw

Demo code: https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqa1ByVlpITzM3RUdjazNHd3V1VS0weUo3QW1FQXxBQ3Jtc0tranhwUTA0WU1qZ3pCUW5VT1Z2dktLOWR0dG9OcnZiTllwellsZGt6QmRWYl82OGxVdUE1VHFVZDNnZ3VLRkR6S18wOGVCbkUwN3VuYWx6UjByMU9pODhBNDNVOUpGLS1iZEtnZnItWlJ0b0F2d2FLMA&q=https%3A%2F%2Fgithub.com%2FTyrrrz%2FJetBrainsDotnetDay2020&v=34C_7halqGw


open System
open FParsec

let p - pstring "hello"
let result - run p "goodby"
printfn "%O" result // => Error in Ln 1 Col 1

---------------------

let a = pstring "hello"
let b = pstring "world"
let p = a <|> b // "hello" or "world"

---------------------

type Token = Hello | World // en enum
let a = pstring "hello" >>% Token.Hello // returns a value
let b = pstring "world" >>% Token.World
let result = run p "world"
printfn "%O" result // Success: World

---------------------

Let's suppose we want to be able to write something like

filterby Category = 'Fantasy'
orderby Rating desc
skip 5
take 10

in our language syntax.


Create Query.fs as a module:

module Qry.Query
open FParsec

type BinaryExprKind = 
    | Add
    | Subract
    | Mutliply
    | Devide
    | And
    | Or
    | Equals
    | NotEquals
    | GreaterThan
    | GreaterThanOrEquals
    | LesserThan
    | LessetThanOrEquals


type Expr = 
    | IntLiteral of int
    | FloatLiteral of float
    | StringLiteral of string
    | Identifier of string
    | Binary of (Expr * Expr * BinaryExprKind) 


Example:
let a = Expr.IntLiteral 42
let b = Expr.Binary (Expr.FloatLiteral 3.14, Expr.IntLiteral 15, BinaryExprKind.GreaterThan)


type OrderDir = Ascending | Descending

type Stmt =
    | FilterBy of Expr
    | OrderBy of Expr * OrderDir
    | Skip of int
    | Take of int

type Query = {
    Statements : Stmt list
}


Now let's define the first parsers

// let's parse 'hello world'
let quote : Parser<_, unit> = skipChar '\'' // like pchar except we discard what we find
let stringLiteral = quote >>. manyCharsTill anyChar quote |>> Expr.StringLiteral 
let identifier = many1Chars (letter <|> digit) |>> Expr.identifier 

// operator precedence
let opp = OperatorPrecedenceParser<Expr, _, _>()

opp.TermParser <- choice [
    intOrFloatLiteral
    stringLiteral
    identifier
]

let ws = skipMany (skipChar ' ')
opp.AddOperator <| InfixOperator("*", ws, 1, Associativity.Left. fun x y -> Expr.Binary (x, y, BinaryExprKind.Multiply))
opp.AddOperator <| InfixOperator("/", ws, 2, Associativity.Left. fun x y -> Expr.Binary (x, y, BinaryExprKind.Divide))
opp.AddOperator <| InfixOperator("-", ws, 3, Associativity.Left. fun x y -> Expr.Binary (x, y, BinaryExprKind.Subtract))
opp.AddOperator <| InfixOperator("+", ws, 4, Associativity.Left. fun x y -> Expr.Binary (x, y, BinaryExprKind.Add))
opp.AddOperator <| InfixOperator("&&", ws, 5, Associativity.Left. fun x y -> Expr.Binary (x, y, BinaryExprKind.And))
opp.AddOperator <| InfixOperator("||", ws, 6, Associativity.Left. fun x y -> Expr.Binary (x, y, BinaryExprKind.Or))
opp.AddOperator <| InfixOperator("=", ws, 7, Associativity.None. fun x y -> Expr.Binary (x, y, BinaryExprKind.Equals))
opp.AddOperator <| InfixOperator("=/=", ws, 8, Associativity.None. fun x y -> Expr.Binary (x, y, BinaryExprKind.NotEquals))
opp.AddOperator <| InfixOperator(">", ws, 9, Associativity.None. fun x y -> Expr.Binary (x, y, BinaryExprKind.GreaterThan))
opp.AddOperator <| InfixOperator("<", ws, 10, Associativity.None. fun x y -> Expr.Binary (x, y, BinaryExprKind.LessThan))
and for >=, <=









let intOrFloatLiteral = 
    numberLiteral (NumberLiteral.DefaultFloat ||| NumberLiteralOptions.DefaultInteger) "number"
    |>> fun n -> 
        if n.IsInteget then ExprIntLiteral (int n.String)
        else Expr.FloatLiteral (float n.String)


let expr = intOrFloatLiteral<|> stringLiteral <|> identifier
but better to use
let exp choice [
    intOrFloatLiteral
    stringLiteral
    identifier
]



In Program.fs (file order matters)

open FParsec
open Qry.Query

let result = run stringLiteral "'hello world'"
printfn "%O" result // => Success: StringLiteral "hello world"

let result = run expr  "3.14" // => Success: FloatLiteral 3.14


let expr = opp.ExpressionParser 
let result = run expr "Category = 'Fantasy'" // => Success: Binary (Indetifier "Category", StringLiteral "Fantasy", Equals)



let orderDirAsc = skipString "asc" >>% oderDir.Ascending .>> ws
let orderDirDesc = skipString "des" >>% oderDir.Descending .>> ws
let orderDir orderDirAsc <|> orderDirDesc


let ws1 = skipMany1 (skipChar ' ')
let filterBy = skipString "filterby" >>. ws1 >>. expr .>> ws |>> Stmt.FilterBy
let orderBy = skipString "orderby" >>. ws1 >>. expr .>>.  orderDir .>> ws |>> Stmt.OrderBy
let skip = skipString "skip" >>. ws1 >>. pint32 .>> ws |>> Stmt.Skip
let take = skipString "take" >>. ws1 >>. pint32 .>> ws |>> Stmt.Take

let stmt = choice [
    filterBy
    orderBy
    skip
    take
]

let quety = sepEndBy stmt skipNewline |>> fun s -> { Statements = s}

let queryFull = spaces >>. query .>> spaces  .>> eof

let parse input =
    match run queryFull input with
    | Sucess (res, _, _) -> Result.Ok res
    | Fail (err, _, _) -> Result.Err err

let input = """
filterBy Category = 'Fantasy'
orderBy Rating desc
skip 5
take 10
"""

let result = parse input
match result iwht
| Result.Ok res -> printfn "%O" res
| Result.Err err -> printfn "%O" err


// we need to evaluatio it
let execute query source =
    let rec evaluate expr element =
        match expr with
        | IntLiteral i -> i :> obj
        | FloatLiteral f -> f :> obj
        | SringLiteral s -> s :> obj
        | Identifier n -> element.GetType().GetProperty(n).GetValue(element)
        | Binary (left, right, kind) ->
            let leftEvaluated = evaluate left element
            let rightEvaluated = evaluate right element

            match kind with
            | Add -> (leftEvaluated :?> float) + (rightEvaluated :?> float) :> obj
            | likewise for the other operators

    let applyFilter expr lst = 
        List.filter (func i -> evaluae expr i :?> boo) lst

    let applyOrder expr dit lst =
        match dir with
        | Ascending -> List.sortBy (fun i -> evaluate expr i :?> IComparable) lst
        | Descending -> List.sortByDescending (fun i -> evaluate expr i :?> IComparable) lst

    let applySkip count lst =
        if count < List.length lst then
            List.skip count lst
        else
            lst

    let applyTake count lst =
        if count < List.length lst then
            List.take count lst
        else
            lst

    List.fold (fun  cur stmt ->
        match stmt with
        | FilterBy expr -> applyFilter expe cur
        | OrderBy (expr, dir) -> applyOrder expr dir cur
        | Skip count -> applySkip count cur
        | Take count -> applyTake count cur) source query.Statements


















