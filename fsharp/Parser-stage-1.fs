﻿open System
open FParsec


type Line =
    | Link of string * string // url, description
    | Heading of string
    | Normal of string

let most = manySatisfy (function ' '|'\t' -> false | _ -> true)

let mkLink (u:string, d:string) = Link(u, d)
let link = skipString "=>" >>. spaces  >>. most .>> spaces .>>. restOfLine false  |>> mkLink
let heading = pchar '#' .>>. restOfLine false |>> fun (x, y) -> Heading( "#" + y)
let normalLine =    restOfLine false  |>> Normal // >>% Line.Normal


let line = 
    choice [
        link
        heading
        normalLine        
    ]


let str = """
# This is a heading
## And so is this
This is just a normal line
=> linkypoo.gmi And this is a link
"""

let lines = str.Split[|'\n'|]
for lin in lines do
    printfn "%A" lin
    let result = run line lin
    printfn "%O\n" result
