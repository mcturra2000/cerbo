/*
 * dnsgq - dns generate query
 */
#include "dns.h"

using namespace std;

void push8(bvec_t& bvec, u8 v)
{
	bvec.push_back(v);
}

u16 get16(bvec_t& bvec, int pos)
{
	u8 arr[2] = {bvec[pos], bvec[pos+1]};
	//u16 v = static_cast<u16>(static_cast<u8*>(arr));
	u16 v = *(u16*) arr;
	return ntohs(v);
}
void put16(bvec_t& bvec, int pos, u16 v)
{
	v = htons(v);
	//cout << "v=" << v << endl;
	auto v1 = static_cast<char*>(static_cast<void*>(&v));
	//cout << (int) v1[0] << " " << (int) v1[1] << endl;
	bvec[pos] = v1[0];
	bvec[pos+1] = v1[1];
}

// assumes little-endian system
void push16(bvec_t& bvec, u16 v)
{
	//v = htons(v);
	bvec.push_back(v >> 8);
	bvec.push_back(v);
}

// assumes little-endian system
void push32(bvec_t& bvec, u32 v)
{
	v = htonl(v);
	auto v1 = static_cast<char*>(static_cast<void*>(&v));
	bvec.push_back(v1[0]);
	bvec.push_back(v1[1]);
	bvec.push_back(v1[2]);
	bvec.push_back(v1[3]);
}

std::vector<std::string> split_string(std::string str )
{
	std::stringstream test(str);
	std::string segment;
	std::vector<std::string> seglist;

	while(std::getline(test, segment, '.'))
	{
		seglist.push_back(segment);
	}
	return seglist;
}

bvec_t pack_query(u16 id, string url)
{

	//auto& hdr = pkt.hdr;
	//hdr.QDCOUNT = pkt.questions.size();
	//hdr.ANCOUNT = pkt.answers.size();
	//hdr.NSCOUNT = pkt.questions.size();

	bvec_t bvec; //the resulting packet to send

	push16(bvec, id); 

	// flags1
	u8 qr = 0, opcode = 0, aa = 0, tc = 0, rd = 0;
	u16 flags1 =  (qr<<7) | (opcode<<3) | (aa<<2)  | (tc <<1) | rd; 
	push8(bvec, flags1);

	// flags2
	u8 ra = 0, z = 0, rcode = 0;
	u16 flags2 = (ra<<7) | (z<<4) | rcode;
	push8(bvec, flags2);

	push16(bvec, 1); // exactly 1 question

	// number of answers, ANCOUNT
	push16(bvec, 0);
	//int num_answers = ip == 0 ? 0 : 1;
	//push16(bvec, num_answers); // ANCOUNT
			 // 
	push16(bvec, 0); // NSCOUNT
	push16(bvec, 0); // ARCOUNT


	// QUESTION
	// qname
	auto parts{split_string(url)};
	for(const auto& p: parts) {
		if(p.size() == 0) continue;
		push8(bvec, p.size());
		for(int i=0; i < p.size(); i++) push8(bvec, p[i]);
	}
	push8(bvec, 0); // to finish qname

	// qtype
	push16(bvec, 1);

	// qclass
	push16(bvec, 1);


	return bvec;

}


void add_answer(bvec_t& bvec, const string& ip)
{
	add_answer(bvec, ip.c_str());
}
void add_answer(bvec_t& bvec, const char* ip)
{
	if(ip ==0) return;

	// increment the number of answers
	u16 v = get16(bvec, 6);
	v++;
	//cerr << v << endl;
	put16(bvec, 6, v);

	// answer is in the form of a resource record

	// NAME if an offset into bvec (usually just past the header)
	push16(bvec, (0b11<<14) | 12);

	push16(bvec, 1); // TYPE
	push16(bvec, 1); // CLASS
	push32(bvec, 60); // TTL
	push16(bvec, 4); // RDLENTH = 4, implying RDATA has IP address
	push32(bvec, ntohl(inet_addr(ip))); // RDATA: IP address
}

