/*
 * dnsgq - dns generate query
 */
#include "dns.h"


int main(int argc, char** argv)
{
	// set random number generation for IDs
	time_t t = time(NULL);
	srandom(t);

	char* ip = 0;

	int c;
	while((c = getopt(argc, argv, "a:")) != -1)
		switch(c) {
			case 'a':
				//puts("hi");
				ip = optarg;
				break;
			default:
				abort();
		}

	//fprintf(stderr, "ip is %s\n", ip);

	char *url = 0;
	for(int index = optind; index < argc; index++) {
		url = argv[index];
		//fprintf(stderr, "Non-option argument is %s\n", argv[index]);
	}




	bvec_t bvec = pack_query(random(), url);
	add_answer(bvec, ip);


	//cerr << bvec.size() << endl;
	write(STDOUT_FILENO, bvec.data(), bvec.size());


	return 0;
	//dump("dnss.bin", bvec);
}
