#include "dns.h"


size_t udpsr(u8* inbuf, size_t incount, u8* outbuf, size_t outcount, const char* server_ip, int port, int& ec)
{
	ec = 0; // indicating that there is no error
	unsigned long socket_length;

	int socket_desc = socket(AF_INET, SOCK_DGRAM, 0);
	if (socket_desc == -1) { ec = ESOCK; return -1;}

	closing s(socket_desc);

	struct sockaddr_in socket_addr{0};
	socket_addr.sin_family = AF_INET;
	socket_addr.sin_port = htons(port);
	socket_addr.sin_addr.s_addr = inet_addr(server_ip);

	auto send_sz = sendto(
			socket_desc,
			inbuf,
			incount, 
			0, // flags
			(struct sockaddr*) (&socket_addr),
			sizeof(socket_addr)
			);
	if (send_sz < incount) { ec = ESEND; return -1;}

	struct sockaddr_in cliaddr{0};
	socklen_t len = sizeof(cliaddr);
	auto recv_sz = recvfrom(socket_desc, outbuf, outcount, MSG_WAITALL, (struct sockaddr*)(&cliaddr), &len);
	if (recv_sz < 0) { ec = ERECV; return -1;}

	return recv_sz;
}

closing::closing(int fd)
{
	m_fd = fd;
}

closing::~closing()
{
	close(m_fd);
}

/*
 * caller is responsible for opening and closing fd
 */
size_t snort(int fd, u8* buf, size_t count)
{
	//uchar* ptr = buf;
	//size_t max_remaining = count;
	size_t tot_read = 0;
	while(1) {
		size_t n = read(fd, buf+tot_read, min(count-tot_read, (size_t) 512));
		tot_read += n;

		if(n<512) break;
	}

	return tot_read;
}

size_t slurp(const char* pathname, uchar* buf, int len)
{
	FILE* fp = fopen(pathname, "r");
	assert(fp);
	size_t n = fread(buf, 1, len, fp);
	printf("Len of file = %d\n", n);
	return n;
}

void dump(const char* pathname, const bvec_t& bvec)
{
	dump(pathname, (const char*) bvec.data(), bvec.size());
}

void dump(const char* pathname, const char* data, int len)
{
	//int fd = open(pathname, O_TRUNC | O_WRONLY | O_CREAT);
	//assert(fd != -1);
	FILE *fp = fopen(pathname, "w");
	assert(fp);
	fwrite(data, 1, len, fp);
	fclose(fp);
}
