/* 
 * send then receive data via UDP
 *
 * 2024-05-09	mcarter Started
 */

#include "dns.h"



int main(int argc, char** argv)
{
	u8 buf[6000];
	size_t len = snort(STDIN_FILENO, buf, sizeof(buf));

	int ec;
	len = udpsr(buf, len, buf, sizeof(buf), "212.23.3.100", 53, ec);
	if(ec!=0) exit(ec);

        write(STDOUT_FILENO, buf, len);

	return 0;
}
