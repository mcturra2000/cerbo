#include "dns.h"


#include <cstring>
#include <netdb.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/types.h> 

using namespace std;

//#define MAX 60000 
//#define PORT 7122
#define SA struct sockaddr 

const string DNS_SERVER{"212.23.3.100"};
//const string DNS_SERVER{"127.0.0.1"};
//const string DNS_SERVER{"1.1.1.1"};

// these are out table of addresses that we will handle ourselves
//vector<pair<string, string>> overrides;
/*
   void fill_overrides()
   add_overrride("

   vector<pair<const char*, const char*>>
   */

size_t make_local_response(char* buff, size_t count, u16 id, string& url, string& ip)
{
	bvec_t bvec{pack_query(id, url)};
	add_answer(bvec, ip);
	size_t len = min(bvec.size(), count);
	memcpy(buff, bvec.data(), len);
	//dump("loc.bin", buff, len);
	return len;

}

bool find_local(const char* qname, string& ip)
{
	static const auto overrides = vector<pair<string_view, int>>{
		{"readyshare", 1},
			{"inspiron", 11},
			{"vostro", 12},
			{"vivo", 13},
			{"blinkyshark", 13},
			{"pi0", 14},
			{"tozip", 14},
			{"beelink", 16},
			{"pi1", 17},
			{"pi3", 18},

			{"fake", 99} // a fake address for experimentation purposes
	};
	for(const auto& [name, ip1] : overrides) {
		const char* n1 = name.data();
		//int len = strlen(n1);
		int len = name.size();
		//cout << "size " << len;
		if((qname[0] == len) && (strncmp(qname+1, n1, len) == 0)) {
			ip = "192.168.0." + to_string(ip1);
			return true;
		}
		//cout << name << endl;
	}

	return false;
}
/*
   constexpr string mk()
   {
   string s{"hello"};
   for(int i = 0; i<2; i++) 
   s += "x";
   return s;
   }
   constexpr string s{mk()};
   */



// Function designed for chat between client and server. 
void func(int sockfd) 
{ 
	int ec = 0; //potential error code
	struct sockaddr_in cliaddr{0}; 
	socklen_t len = sizeof(cliaddr);
	char buff[60000];
	char *pbuf = buff;
	int n = recvfrom(sockfd, (char *)buff, sizeof(buff), MSG_WAITALL, ( struct sockaddr *) &cliaddr, &len);
	closing s(sockfd);
	if(n<0) {
		puts("Failed");
		exit(13);
	}

	//dump("in.bin", buff, n);

	// check to see if we need to intercept local hosts
	int num = 0;
	string ip;
	if(find_local(buff+DNS_HDR_SIZE, ip)) {
		//TODO it's a bit silly reconstructing the URL only to have it decoded again
		string url;
		char* ptr = buff+12;
		while(1) {
			char s = *ptr++;
			if(s == 0) break;
			for(int i=0; i< s; i++) url += *ptr++;
			if(*ptr != 0) url += '.';
		}
		//cerr << "url is " << url << endl;


		u16 id = *(u16*) buff; //get16(buff, 0);
		id = ntohs(id);
		num = make_local_response(buff, sizeof(buff), id, url, ip); 
		//cout << "found ip" << ip << endl;
	} else {
		num = udpsr((u8*) buff, n, (u8*) buff, sizeof(buff), DNS_SERVER.c_str(), 53, ec);
	}
	//dump("out.bin", buff, num);

	// sending back to client
	auto sent_back = sendto(sockfd, buff, num, MSG_WAITALL, ( struct sockaddr *) &cliaddr, sizeof(cliaddr));
	if(sent_back <0) {
		exit(1);
	}       


} 

void main_conn(int port) 
{ 
	cerr << "main_conn: called" << endl;

	int sockfd, connfd; 

	// socket create and verification 
	//sockfd = socket(AF_INET, SOCK_STREAM, 0); 
	sockfd = socket(AF_INET, SOCK_DGRAM, 0); 
	if (sockfd == -1) exit(11); 

	closing s(sockfd);

	struct sockaddr_in servaddr{0};
	//bzero(&servaddr, sizeof(servaddr)); 

	// assign IP, PORT 
	servaddr.sin_family = AF_INET; 
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY); 
	servaddr.sin_port = htons(port); 


	// make it reusable
#if 0
	const int       optVal = 1;
	const socklen_t optLen = sizeof(optVal);
	int rtn = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (void*) &optVal, optLen);
	assert(rtn == 0);   /* this is optional */
#endif

	// Binding newly created socket to given IP and verification 
	if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) {
		cout << "ESOCK: Socket bind failure trying to create server on port " << port <<  endl;
		exit(ESOCK);
	}


	// Function for chatting between client and server 
	func(sockfd); 


} 

//constexpr auto arr = std::experimental::make_array(1, 2, 3, 4, 5);

int main(int argc, char** argv)
{
	int port = 7122;

	// process arguments
	int c;
	while((c = getopt(argc, argv, "p:")) != -1) 
		switch(c) {
			case 'p' : 
				port = atoi(optarg);
				break;
			default:
				//cerr << "unrecognised argument " << (char)c << endl;
				abort();
		}

	for(int index = optind; index < argc; index++) {
		//url = argv[index];
		fprintf(stderr, "Non-option argument is %s\n", argv[index]);
	}



loop:
	main_conn(port);
	goto loop;
	return 0;
}

