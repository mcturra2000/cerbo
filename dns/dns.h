#pragma once

#include <experimental/array>

#include <cstdint>
#include <cstdio>
#include <fcntl.h>

#include <algorithm>
#include <arpa/inet.h>
#include <assert.h>
#include <iostream>
#include <map>
#include <stdio.h> 
#include <stdlib.h> 
#include <string> 
#include <unistd.h>
#include <vector> 
#include <sstream>

#define PACKED __attribute__((packed))

using uchar = unsigned char;
using uint = unsigned int;
using u8 = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;

using namespace std;

typedef std::vector<u8> bvec_t; // vector of types

#define ESOCK 100	// error  creating a socket
#define ESEND 101	// error in  sendto
#define ERECV 102	// error in recvfrom


#define DNS_HDR_SIZE  12 // size of DNS header is 6x 2 octets

void dump(const char* pathname, const char* data, int len);
void dump(const char* pathname, const bvec_t& bvec);
size_t slurp(const char* pathname, uchar* buf, int len);
size_t snort(int fd, u8* buf, size_t count);
size_t udpsr(u8* inbuf, size_t incount, u8* outbuf, size_t outcount, const char* server_ip, int port, int& ec);
bvec_t pack_query(u16 id, string url);
void add_answer(bvec_t& bvec, const char* ip);
void add_answer(bvec_t& bvec, const string& ip);

// automatically close a file descriptor
class closing {
	public:
		closing(int fd);
		~closing();
		int m_fd;
};



/**
 * Message header
 * (https://www.freesoft.org/CIE/RFC/1035/40.htm)
 **/
typedef struct PACKED  
{
	// Transaction ID
	u16 id;

	u8 flags1;
	u8 flags2;

	u16 QDCOUNT;
	u16 ANCOUNT;
	u16 NSCOUNT;
	u16 ARCOUNT;
} DNS_header;

// an "unpacked" version which contains the data in host (likely little-endian) format
typedef struct
{
	u16 id;

	// flags breakdown
	u8 qr, opcode, aa, tc, rd;	// flags 1
	u8 ra, z, rcode;		// flags2

	u16 QDCOUNT;
	u16 ANCOUNT;
	u16 NSCOUNT;
	u16 ARCOUNT;
} DNS_header_unpacked;

// resource record
typedef struct
{
	u16 name;
	u16 type;
	u16 cls;
	u32 ttl;
	u16 rdlength;
	std::vector<u8> rdata;
} DNS_rr_t;

// question
typedef struct
{
	std::string qname;
	u16 qtype;
	u16 qclass;
} DNS_qn;

// An unpacked packet of data
typedef struct
{
	DNS_header_unpacked hdr;

	std::vector<DNS_qn> questions;
	std::vector<DNS_rr_t> answers;
} DNS_pkt;
