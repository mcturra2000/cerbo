//#include <assert.h>


#include "dns.h"

using namespace std;

DNS_header_unpacked unpack_header(const DNS_header& hdr);

class mem {
	public:	
		mem(u8* ptr) : m_ptr(ptr) {};
		u16 read16();
		u32 read32();
		u8  read8();
		bvec_t glom(int n);
		u8* m_ptr;
};


u8 mem::read8()
{
	return *m_ptr++;
}
bvec_t mem::glom(int n)
{
	bvec_t res;
	for(int i =0; i< n; i++) {
		res.push_back(*m_ptr++);
	}
	return res;
}


u16 mem::read16()
{
	u16 res = *(u16*) m_ptr;
	m_ptr += 2;
	return ntohs(res);
}

u32 mem::read32()
{
	u32 res = *(u32*) m_ptr;
	m_ptr += 4;
	return ntohl(res);
}




DNS_rr_t read_rr(mem& m)
{
	DNS_rr_t rr;
	rr.name = m.read16();
	rr.type = m.read16();
	rr.cls  = m.read16();
	rr.ttl  = m.read32();
	rr.rdlength = m.read16();
	rr.rdata = m.glom(rr.rdlength);
	return rr;

}


DNS_pkt  decode_pkt(uchar* buf, int len)
{
	//mem m{buf};
	DNS_pkt pkt;

	DNS_header_unpacked hdr; // header in host order
	{
		DNS_header hdr_tmp = *(DNS_header*) buf;
		hdr = unpack_header(hdr_tmp);
	}
	pkt.hdr = hdr;

	// question section
	mem m{buf + sizeof(DNS_header)};
	cout << "QCOUNT " << hdr.QDCOUNT << endl;
	for(int i = 0; i< hdr.QDCOUNT; i++) {
		DNS_qn qn;
		string qname;
		while(1) {
			//cout << '.';
			char s = m.read8();
			if(s==0) break;
			for(int i = 0; i<s; i++) qname  += m.read8();
			qname += '.';
		}

		//cout << "ptr = " << (long) (ptr -buf) << endl;
		//cout << "QNAME " << qname << endl;
		// QTYPE https://en.wikipedia.org/wiki/List_of_DNS_record_types
		qn.qname = qname;
		qn.qtype = m.read16();
		qn.qclass = m.read16();
		pkt.questions.push_back(qn);
		cout << "pushed back question" << endl;
	}

	// answer section
	for(int i = 0 ; i < hdr.ANCOUNT; i++) {
		pkt.answers.push_back(read_rr(m));
		cout << "pushed back answer" << endl;
		//cout << "\nptr = " << (u64) (ptr-buf) << endl;
	}


	return pkt;

	assert(hdr.NSCOUNT == 0);

	for(int i = 0 ; i < hdr.ARCOUNT; i++) {
		read_rr(m);
		//cout << "\nptr = " << (u64) (ptr-buf) << endl;
	}

	return pkt;
}

uint red1(int* flag, int mask) 
{
	unsigned int val = *flag & mask;
	//cout << flag << " " << mask << " " << val << endl;
	while(mask) {
		*flag >>= 1;
		mask >>= 1;
	}
	return val; 
}
#define red(m) red1(&flag, m)

DNS_header_unpacked unpack_header(const DNS_header& hdr)
{
	DNS_header_unpacked res;

	res.id = ntohs(hdr.id);

	// deal with flags, starting in least-significant position
	int flag = hdr.flags1;
	res.rd = red(1);
	res.tc = red(1);
	res.aa = red(1);
	res.opcode = red(0b1111);
	res.qr = red(1);

	flag = hdr.flags2;
	res.rcode = red(4);
	res.z = red(3);
	res.ra = red(1);

	// rest of header
	res.QDCOUNT = ntohs(hdr.QDCOUNT);
	res.ANCOUNT = ntohs(hdr.ANCOUNT);
	res.NSCOUNT = ntohs(hdr.NSCOUNT);
	res.ARCOUNT = ntohs(hdr.ARCOUNT);

	return res;
}

//	DNS_header* hdr = (DNS_header*) buff;
void print_dns_hdr(const DNS_header_unpacked& hdr)
{

	//;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 32883
	cout << ";; ->>HEADER<<- opcode:" << hdr.opcode;
	cout << ", status: " << hdr.rcode;
	cout << ", id: " << hdr.id;
	cout << endl;
	//;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1
	cout << ";; flags:";
	if(hdr.qr) cout << " qr";
	if(hdr.aa) cout << " aa";
	if(hdr.tc) cout << " tc";
	if(hdr.rd) cout << " rd";
	if(hdr.ra) cout << " ra";
	cout << " QUERY: " << hdr.QDCOUNT;
	cout << ", ANSWER: " << hdr.ANCOUNT;
	cout << ", AUTHORITY: " << hdr.NSCOUNT;
	cout << ", ADDITIONAL: " << hdr.ARCOUNT;
	cout << endl;


}


u16 read16(uchar** ptr)
{
	u16 val = *(u16*) *ptr;

	*ptr += 2;
	//**ptr++;
	//**ptr = 0;
	return ntohs(val);
}

void print_pkt(DNS_pkt& pkt, uchar* buf)
{
	print_dns_hdr(pkt.hdr);


	if(pkt.questions.size() > 0) cout << ";; QUESTION SECTION:" << endl;
	for(const auto & q: pkt.questions) {
		cout << q.qname << endl;
	}

	if(pkt.answers.size()>0) cout << ";; ANSWER SECTION:" << endl;
	for(const auto & a: pkt.answers) {
		cout << a.name << '\t';
		auto& name = a.name;
		bool is_ptr = a.name && 0b1100000000000000;
		assert(is_ptr);
		if(is_ptr) {
			auto offset = a.name & 0b0011111111111111;
			cout << "is a pointer with offset "<< offset << "\n";
			while(1) {
				int n = buf[offset++];
				if(n== 0) break;
				for(int i = 0; i < n; i ++) putchar(buf[offset++]);
				putchar('.');

			}
			putchar('\t');
			putchar('\t');

		}

		cout << a.ttl << "\t";

		// class
		switch(a.cls) {
			case 1 : cout << "IN"; break;
			case 2 : cout << "CS"; break;
			case 3 : cout << "CH"; break;
			case 4 : cout << "HS"; break;
			default : cout <<"?";
		}
		cout << "\t";

		std::map<int, string> m{{1, "A"}, {2, "NS"}};
		/*
		typedef struct { int n; const char* val;} type_entry;
		type_entry  types[] = {
			{1, "A"},
			{2, "NS"},
			{5, "CNAME"
		};
		*/
		// type
		//switch(a.type) {
		//	case 1 : 

		cout << m[a.type] << "\t"; // TODO not found

	if(a.rdlength == 4) {
		// could be an IP address
		printf("%d.%d.%d.%d", a.rdata[0], a.rdata[1], a.rdata[2], a.rdata[3]);
	} else {
		printf("RDATA (%d ): ", a.rdlength);
	}


	cout << endl;
	}

	cout << ";; ADDITIONAL SECTION:" << endl;
}
int main(int argc, char** argv)
{

	//assert(argc>1);
	//printf("Analysing %s\n", argv[1]);
	uchar buf[6000];

	size_t len = snort(STDIN_FILENO, buf, sizeof(buf));
	DNS_pkt pkt{decode_pkt(buf, len)};

	print_pkt(pkt, buf);


	return 0;
}
