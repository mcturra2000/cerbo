# dns

**TODO** What does `dig google.com -p 7122` have as an additional response? I need to edit `dns` to capture the input and output


A strange little DNS server
Runs on port 7122

Run make test to see it in action.


A great deal of info about DNSs can be found at 
https://github.com/dorin131/dns-query.git

Also ref p47, z17c2a


To run the server:
```
sudo ./dns -p 53
```

## Examples

### dnsgq : dns generate question

dnsgq google.com

You can add an answer to the answer section using '-a', like so:

dnsgq beelink -a 192.168.0.66

### dns-dump : prints a packet to stdout

You can see what a DNS query looks like by, for example:

dnsgq google.com | dns-dump

### udpsr : UDP send and receiver

Reads a packet from stdin, sends it to a server, and writes response to stdout


## References

* [How to Create a Daemon on Linux](https://www.makeuseof.com/create-daemons-on-linux/)
* [Let's hand write DNS messages](https://routley.io/posts/hand-writing-dns-messages)
* [Perl module](https://metacpan.org/pod/Net::DNS::Nameserver)


## Status

2024-05-06  Started. Works
