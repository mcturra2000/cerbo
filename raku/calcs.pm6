use v6;
unit module calcs;

say "hello from calcs";

sub tc($num) is export { return -$num + 1; } # effectively creating two's compliments
sub bin($num) is export { return $num.fmt("%b"); }
sub hex($num) is export { return $num.fmt("%x"); }
