#pragma once

#define YYSTYPE double
#include "bison.tab.hh"

extern int yylineno;
//int yyparse();


#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>	
#include <stdlib.h>	




//extern int yylineno;
void yyerror(const char* s);
int yylex(void);	

//extern char* yytext;
extern int column; // not yet implemented
extern int yywrap();
void found(const char* s);
void xfound(const char* s);
int mktext();
void print_text(int n);
void starting(int date);
void mkntran(int date, int dr, int cr, double amnt, int desc);
void mketran(int date, int dr, int epic, int qty, double amnt, int desc);
void mkgaap1(int type, int desc);
void mkgaap2(int type, int desc, int acc);



#ifdef __cplusplus
}
#endif