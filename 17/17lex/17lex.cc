#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <fstream>      // std::filebuf

#include <regex.h>
//#include "common.h"

using namespace std;
using strings_t = vector<string>;

regex_t regex;

strings_t print_file(std::istream& in)
{
    strings_t urls;
    size_t maxGroups = 3;
    regmatch_t groupArray[maxGroups];
    for(std::string line; std::getline(in, line);) {
        int reti = regexec(&regex, line.c_str(), maxGroups, groupArray, 0);
        auto str = [&](int n) {
            std::string res;
            auto ga = groupArray[n];
            for(int i = ga.rm_so; i < ga.rm_eo; i++)
                res += line[ga.rm_so*0 + i];
            return res;
        };

        if(!reti) {
            // match on url
            string url = str(1);
            cout << "["  << urls.size() << "] " << url << " -> " << str(2) << endl;
            urls.push_back(url);
            //puts("match");

            //std::cout << "/X\n";
        } else if (reti == REG_NOMATCH) {
            // no match on url, so just a regular line
            cout << line << endl;

            //puts("No match");
        } else {
            //regerror(reti, &regex)
            fprintf(stderr,"FATAL: regexec error");
            exit(1);
        }

        //std::cout << line << '\n';
        
    }
    return urls;
}

int main(int argc, char** argv)
{
    int reti = regcomp(&regex, "^=>\\s+(\\S+)\\s+(.*)$", REG_EXTENDED); 
    if(reti) {
        fprintf(stderr, "FATAL: Could not compile regex\n");
        exit(1);
    }

    string root{"/home/pi/repos/cerbo/17/17docs/"};
    string zet = "17";
    strings_t history;
    while(true) {
        std::filebuf fb;
        if(fb.open(root+zet, std::ios::in)) {
            std::istream is(&fb);
            auto urls = print_file(is);
            fb.close();
            cout << "?";
            string user_input;
            getline(cin, user_input);
            if(user_input == "b") {
                zet = history[history.size() -1];
                history.pop_back();
            } else {
                history.push_back(zet);
                int url_num = stoi(user_input);
                cout << "You selected url " << url_num << endl;
                cout << "------------------------------\n";
                zet = urls[url_num];
            }

        } else {
            puts("File not found");
        }
    }

    regfree(&regex);
    return 0;
    //int status = yyparse();
}
