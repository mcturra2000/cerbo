%option yylineno

%{
    #include "common.h"
%}

blanks  [ \t]+
word    [^ \t\n]+

%%

^=>     { xfound("LINKIND"); return LINKIND; }
{blanks}  { printf("- BLANKS -"); return BLANKS; }
{word}    { printf("word: %s", yytext); return WORD; }
\n       { return NL; }


%%

