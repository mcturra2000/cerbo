#include <algorithm>
#include <cstring>
#include <iostream>
#include <filesystem>
#include <fstream>
#include <sstream>
#include <string>
#include <tuple>
#include <vector>

using namespace std;
const string root_dir = "/home/pi/repos/cerbo/17/17docs";

typedef vector<string> strings;

/*
//std::pair<string::iterator, bool> find1(string str, string what) 
//string::iterator find1(string str, string what) 
int find1(string str, string what) 
{
	string::iterator it = find(str.begin(), str.end(), what);
	return 2;
	//return {it, it != str.end()};
}
*/

int main(int argc, char** argv)
{
	strings paths; // the file names are not necessarily in alphabetical order
	for (auto const& dir_entry : std::filesystem::directory_iterator{root_dir}) {
		string line;
		ifstream istrm(dir_entry.path(), ios::in);
		getline(istrm, line);
		paths.push_back(line);
		//std::cout << dir_entry.path() << '\n';
	}

	std::sort(paths.begin(), paths.end());
	for(auto const& p : paths) {
		//auto [it, ok] = find1(p, "--");
		const char* loc = strstr(p.c_str(), "--");
		if(loc == 0) {
			cout << p ;
		} else {
			char* src = (char*) p.data();
			while(src < loc) putchar(*src++);
			//for(char* src = p.c_str(), src <loc, src++) cout << *str;
		}
		cout << '\n';
	}

	return 0;
}
