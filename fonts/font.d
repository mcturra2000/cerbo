import std.array;
import std.conv;
import std.stdio;

void main()
{
	auto f = File("banner.flf", "r");
	/*
	   string magic;
	   int height, baseline, max_len, old_laout, comment_lines, print_dirn, full_layout, codetag_count;
	   f.readf!"%s %d %d %d %d %d %d %d %d"(magic, height, baseline, max_len, old_laout, comment_lines, print_dirn, full_layout, codetag_count);
	   writeln("magic is ", magic);
	 */
	auto spec = f.readln();
	writeln("spec is ", spec);
	assert(spec[0..5] == "flf2a"); // magic file specifier required
	char smush = spec[5];
	writeln("smuch is ", smush);
	auto arr = spec.split(" ");
	int h = to!int(arr[1]); // height
	int b = to!int(arr[2]); // baseline
	//int ml = to!int(arr[3]); // max length

	int ncl = to!int(arr[5]); // number of comment lines
	foreach(i; 0 .. ncl) f.readln(); // skip the comments
	writeln(ncl);

	string [8][256] charmap;
	//charmap = new string[h][256];
	assert(h == 8);
	//charmap[9][0] = "foo";
	//auto ordinal = 32; // start with space
	foreach(ordinal; ' ' .. '~' + 1) {
		foreach(i; 0 .. h) {
			string raw = f.readln();
			string built = "";
			foreach(c; raw[1..raw.length-2]) {
				char c1 = c;
				if(c == smush) c1 = ' ';
				if(c == '@') continue;
				built ~= c1;
			}
			//writeln(ordinal, " ",  i, " '", built, "'");
			//writeln(charmap);
			charmap[ordinal][i] = built;

			//writeln("'", charmap[ordinal][i], "'");
		}
	}

	// print some text
	string text = "Hello 42";
	foreach(i; 0 .. h) {
		foreach(c; text) {
			write(charmap[c][i]);
		}
		writeln();
	}

}
