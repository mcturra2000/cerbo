#include <stdio.h>

#include <unistd.h>

#include <locale.h>

//#include <notcurses/direct.h>
#include <notcurses/notcurses.h>
//#include <notcurses/notcurses-core.h>
//#include <notcurses/ncport.h>
//#include <notcurses/ncseqs.h>


int main(){
	setlocale(LC_ALL, "");
	notcurses_options ncopt;
	memset(&ncopt, 0, sizeof(ncopt));
	struct notcurses* nc = notcurses_init(&ncopt, stdout);
	struct ncplane* stdplane = notcurses_stdplane(nc);

	ncplane_options body_opts = {0};
	body_opts.y = 1;
	body_opts.x = 0;
	body_opts.rows = 18;
	body_opts.cols = 80;
	struct ncplane* body = ncplane_create(stdplane, &body_opts);

	// we want a coloured background
	struct nccell blank = {0};
	blank.gcluster = ' ';
	nccell_set_bg_rgb8(&blank, 0, 0, 255);
	ncplane_set_base_cell(body, &blank);


	// black text on a white background
	ncplane_set_fg_rgb8(body, 0, 0, 0);
	ncplane_set_bg_rgb8(body, 255, 255, 255);

	//ncplane_erase(body);
	//ncplane_set_bg_default(body);

	for (int i = 0; i < 25; i++){
		for (int j = 0; j < 25; j++){
			//ncplane_putchar_yx(stdplane, i, j, '*');
			ncplane_putchar_yx(body, i, j, '*');
			//ncplane_render(body);
			notcurses_render(nc);
			usleep(5000);
		}
	}
	sleep(7);
	ncplane_destroy(body);
	notcurses_stop(nc);
	return 0;
}
