//#include <common.h>
#include <stdio.h>
#include <stdint.h>
#include <curses.h>
#include <unistd.h>
#include <time.h>
#include <locale.h>
#include <stddef.h>
#include <string.h>


#include "fonts.h"

int use_unicode = 1;


void write_bit(int on)
{
	if(use_unicode) {
		if(on) 
		       	addstr("🟦"); // blue square
		else
			addstr("  ");
		return;
	}

	// not using unicode
	if(on) {
		attron(COLOR_PAIR(1));
		addstr("XX");

	} else {
		attron(COLOR_PAIR(2));
		addstr("  ");
	}
}

void write_char(char chr, int row){
	for(int col = 0; col< 6; col++) {
		const uint8_t *carr = ssd1306_font6x8 + 4+(chr-' ')*6; // offset into char

		int bit = carr[col] & (1<<row);
		write_bit(bit);
	} 
}

void write_str(char *text) 
{
	for(int row = 0; row < 8; row++) {
		clrtoeol();
		char *str = text;
		//char chr;
		while(*str) 
			write_char(*str++, row);

		//printw("\r\n");
		int y, x;
		getyx(stdscr, y, x);
		move(y+1, 0);
		//addch('\r');
	}
}

int main()
{
	setlocale(LC_ALL, "");
	//setlocale(LC_CTYPE,"C-UTF-8");
	initscr();
	start_color();
	init_pair(1, COLOR_BLUE, COLOR_BLUE);
	init_pair(2, COLOR_BLACK, COLOR_BLACK);
	keypad(stdscr, TRUE); // enable keyboard 
	cbreak(); //  take input chars one at a time, no wait for \n
	noecho();
	nodelay(stdscr, TRUE);
	curs_set(0); // hide cursor

	int timing = 0;
	time_t start_time; // in seconds

	while(1) {
		move(0,0);
		//erase();

		char buffer[26];
		time_t now = time(NULL);
		struct tm* tm_info = localtime(&now);
		strftime(buffer, sizeof(buffer), "%a %d", tm_info);
		write_str(buffer);
		strftime(buffer, sizeof(buffer), "%H:%M:%S", tm_info);
		strftime(buffer, sizeof(buffer), "%H:%M", tm_info);
		write_str(buffer);

		refresh();
		int ch = getch();
		//if(kbhit()) {
		//	int c = getch();
		if(ch == 'q') break;
		if(ch == ' ') {
			timing = 1 - timing;
			if(timing) {
				start_time = now;
				//write_str("on");
			} else {
				//write_str("off");
			}
		}


		if(timing) {
			int secs = now - start_time;
			int mins = secs / 60;
			secs = secs % 60;
			sprintf(buffer, "%d:%02d", mins, secs);
			write_str(buffer);
		} else {
			write_str("0:00");
		}
		//}
		usleep(50* 1000);
	}
	endwin();
	return 0;
}
