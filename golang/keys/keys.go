package main

import (
	"bytes"
	"fmt"
	"github.com/pkg/term" // go get github.com/pkg/term@1a4a3b7 
)


// Returns either an ascii code, or (if input is an arrow) a Javascript key code.
func getChar(t *term.Term) (ascii int, keyCode int, err error) {
	bytes1 := make([]byte, 6)

	var numRead int
	numRead, err = t.Read(bytes1)
	if err != nil {
		return
	}
	fmt.Println("num read = ", numRead, "\r")
	fmt.Println(bytes1, "  ")

	for _, c := range bytes1 {
		var s string
		switch(c) {
		case 27:
			s = "\\033" // escape
		default:
			s = string(c)
		}
		fmt.Print(s)
	}
	fmt.Println("\r")


	//term.CBreakMode(t)
	//t.Restore()
	//t.Close()

	pg_up := []byte{27, 91, 53, 126, 0, 0}
	if bytes.Equal(bytes1, pg_up) {
		fmt.Println("You pressed Page Up\r")
	}
	ascii = int(bytes1[0])
	return
}

func main() {
	t, _ := term.Open("/dev/tty")
	term.RawMode(t)
	defer  func() {
		term.CBreakMode(t)
		t.Restore()
		t.Close()
	}()
	fmt.Println("\r")
	for {
		c , _, _ :=getChar(t);
		if c == 'q' { break; }
	}

}
