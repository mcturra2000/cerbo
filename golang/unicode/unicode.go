package main

import (
	"bytes"
	"fmt"
	"github.com/pkg/term" // go get github.com/pkg/term@1a4a3b7 
)


// Returns either an ascii code, or (if input is an arrow) a Javascript key code.
func getChar(t *term.Term) (ascii int, keyCode int, err error) {
	bytes1 := make([]byte, 6)

	var numRead int
	numRead, err = t.Read(bytes1)
	if err != nil { return }
	if numRead == 0 { return }
	//fmt.Println("num read = ", numRead, "\r")
	//fmt.Println(bytes1, "  ")

	/*
	for _, c := range bytes1 {
		var s string
		switch(c) {
		case 27:
			s = "\\033" // escape
		default:
			s = string(c)
		}
		//fmt.Print(s)
	}
	*/
	//fmt.Println("\r")


	pg_up := []byte{27, 91, 53, 126, 0, 0}
	if bytes.Equal(bytes1, pg_up) {
		fmt.Println("You pressed Page Up\r")
	}
	ascii = int(bytes1[0])
	return
}

var charmap =  map[byte]string {
	'a' : "➔" + string([]rune{0x2794}),
	'b' : "⊥", // u22a5
	'c' : "⟘", // u27d8
	'd' : "⊢", // u22a2
	'e' : "⟝", // u27dd
	'f' : "⊣", // u22a3
	'g' : "⊤", // u22a4
	//'h' : "⫠", // u2ae0
	'i' : string([]rune{0x1df5}), // u1df5

	// box drawings
	'j' : "└", // u2514 light arc up and right (box drawing)
	'k' : "╭", // u256d light arc down and right (box drawing)
	'7' : "┌", // u250c light down and right
	'8' : "┬", // u252c light down and vertical
	'9' : "┐", // u2510 light down and left
	'4' : "├", // u251c light vertical and right
	'5' : "┼", // u253c light vertical and horizontal
	'6' : "┤", // u2524 light vertical and left
	'1' : "└", // u2514 light up and right
	'2' : "┴", // u2534 light up and vertical
	'3' : "┘", // u2518 light up and right
	'h' : "─", // u2500 light  horizontal
	'v' : "│", // u2502 light  vertical


}

func closeTerm(t *term.Term) {
	term.CBreakMode(t)
	t.Restore()
	t.Close()
}

func main() {
	t, _ := term.Open("/dev/tty")
	term.RawMode(t)
	defer  closeTerm(t)
	fmt.Println("\r")
	fmt.Println(charmap)
	for {
		c , _, _ :=getChar(t);
		uni, exists := charmap[byte(c)]
		if(exists) {
			fmt.Print(uni)
		} else {
			fmt.Print(string(c))
			if c == 13  { fmt.Println("") }

		}

		if c == 'q' { break; }
	}

}
