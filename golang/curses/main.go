// goncurses - ncurses library for Go.
// Copyright 2011 Rob Thornton. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

/* Expanding on the basic menu example, the example demonstrates how you
* could possibly utilize the mouse to navigate a menu and select options
*/
package main

import (
	gc "github.com/gbin/goncurses"
	//"/gbin/goncurses"
	"fmt"
	"log"
)

const (
	HEIGHT = 10
	WIDTH  = 30
)

func main() {
	fmt.Println(gc.REQ_SCR_FLINE)
	fmt.Println(gc.REQ_SCR_HFLINE)
	fmt.Println(gc.REQ_NEXT_LINE)
	//return


	stdscr, err := gc.Init()
	if err != nil {
		log.Fatal("init:", err)
	}
	defer gc.End()

	gc.Raw(true)
	gc.Echo(false)
	gc.Cursor(0)
	stdscr.Keypad(true)

	if gc.MouseOk() {
		stdscr.MovePrint(3, 0, "WARN: Mouse support not detected.")
	}

	//return
	// Adjust the default mouse-click sensitivity to make it more responsive
	gc.MouseInterval(50)
	gc.MouseMask(gc.M_ALL, nil)
	for {
		ch:= stdscr.GetChar()
		stdscr.Println(ch)
		switch ch {
		case 'q' :
			return;
		case gc.KEY_MOUSE:
			/* pull the mouse event off the queue */
			md := gc.GetMouse()
			stdscr.Println(md)

		}
	}
}
