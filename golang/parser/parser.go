package main

import (
	//"C"
	"bufio"
	"fmt"
	"os"
	"strings"
	//"regexp"
)

var r = bufio.NewReader(os.Stdin)

/*
var ident = regexp.MustCompile("^[[:alpha:]][[:alnum:]]*")

func a(b bool) (bool) {
	fmt.Println("a called");
	return b;
}
*/


const (
	t_eof int = -1
	t_unk = 0
	t_ident = 1
	t_gpio = 2
	t_input = 3
	t_pullup = 4
)

type keyword struct {
	word string;
	id int
}

var keywords = []keyword{
	{"GPIO", t_gpio},
	{"INPUT", t_input},
	{"PULLUP", t_pullup},
}

var yytype = t_unk;
var yytext string;
var yyTEXT string; // in uppercase
var yyline = 1;
var ch int = -2; // next char
//var eof =  false;

func getCh() int {
	//if(ch == '\n') {yyline += 1 }
	c, err :=  r.ReadByte()
	if(err != nil) { return -1 }
	return int(c);
}

func inside(ch , lo, hi int) (bool) { return lo <= ch && ch <= hi }

func isDigit(ch int) (bool) { return inside(ch, '0', '9') }
func isAlpha(ch int) (bool) { return inside(ch, 'a', 'z') || inside(ch, 'A', 'Z') }
func isAlnum(ch int) (bool) { return isAlpha(ch) || isDigit(ch) }

func isSpace(ch int) (bool) {
	if ch == '\n' { yyline +=1 }
	return ch == '\f' || ch == '\n' || ch == '\r' || ch == '\t' || ch == '\v'  || ch == ' '
}

/*
func scoop(fn func(int) bool)  {
	for {
		getCh();
		if(eof) { return}
		if(fn(ch)) {
			yytext += string(ch);
		} else {
			break;
		}
	}
}
*/

func yylex() (int) {
	yytype = t_unk

	yytext = string(ch);
	if(isSpace(ch)) {
		//fmt.Println("lexing space")
		for {
			ch = getCh();
			if(!isSpace(ch)) {break}
		}
		return yylex()
	} else if (ch == t_eof) {
		return t_eof
	} else if( isAlpha(ch) ) {
		//fmt.Println("looks like an alpha:", ch)
		//scoop(isAlnum);
		for {
			ch = getCh();
			if(!isAlnum(ch)) { break }
			yytext +=  string(ch)
		}
		yytype = t_ident
		yyTEXT = strings.ToUpper(yytext)
		for _, kw := range keywords {
			if(kw.word == yyTEXT) {
				yytype = kw.id
				break
			}
		}
		ch = getCh()
		return yytype
	} else {
		ch = getCh()
		yytype = t_unk
		return t_unk
	}
	return t_unk

}


func main() {
	fmt.Println("number of keywords =", len(keywords));

	ch = getCh()
	for {
		tok := yylex()
		if(tok == t_eof) { break }
		//if(tok

		fmt.Printf("line %d, type %d, token ='%s'\n", yyline, yytype,  yytext)
	}

}

