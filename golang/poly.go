package main

import "fmt"

/* 
type Face interface {
	Eye | Mouth
	Purpose() string
}
*/

type Organ interface {
	Purpose() string
}

type Eye struct {
}

type Mouth struct {
}

func (_ Mouth) Purpose() string  { return "I eat" }
func (_ Eye) Purpose() string  { return "I see" }

func main() {
	var v any
	v = 3
	fmt.Println(v)
	v = "hello"
	fmt.Println(v)

	//var w interface{}
	//var x Organ
	var x Organ = Mouth{}
	fmt.Println(x.Purpose())

	var y  Organ = Eye{}
	fmt.Printf("y is of type %T\n", y)
}
