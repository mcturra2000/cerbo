#include <iostream>
#include <string>

using namespace std;
int main() 
{
	string name;
	while(1) {
		int c = getchar();
		if((c == EOF) || (c == '\r') || (c == '\n')) break;
		name += c;
	}

	cout << "Hi: " << name << "\r\nNice to meet you. Bye\r\n" << endl;

	return 0;
}
