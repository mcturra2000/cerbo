# xinetd demo

Creates a simple service on port 7120

You type in your name, prints a little greeting, and exits

## one-off setup
```
sudo apt install xinetd

sudo vim /etc/services , add:
hello	7120/tcp
hello	7120/udp
```

## Building

make install # makes binary and installs

## Test

Method 1:
```
make test
```


Method 2:
telnet localhost 7120
... enter something, and press return

## Status

2024-05-06  Started. Works
