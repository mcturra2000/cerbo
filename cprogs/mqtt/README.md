# mqtt

## Setup

```
sudo apt install mosquitto mosquitto-clients libpaho-mqtt-dev
```

**Remote Access**

`vim /etc/mosquitto/conf.d/mcarter.conf`:
```
listener 1883
allow_anonymous true
```
Then restart:
```
sudo systemctl restart mosquitto
```



**Simple test**
```
mosquitto_sub -h localhost -t test
mosquitto_pub -h localhost -t test -m "hello world"
```

`-h` for host, `-t` for topic, `-m` for message.


## Demo

Compile and run the demo. Then use mosquitto\_pub as indicated above.


## See also

* [Debian files for libpaho-mqtt-dev](https://packages.debian.org/bullseye/amd64/libpaho-mqtt-dev/filelist)


## Status

2021-12-21	Started. Works
