// diy libserialport-devel libserialport-doc
// gcc ser.c -lserialport
#include <libserialport.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int check(enum sp_return result)
{
	/* For this example we'll just exit on any error by calling abort(). */
	char *error_message;
	switch (result) {
		case SP_ERR_ARG:
			printf("Error: Invalid argument.\n");
			abort();
		case SP_ERR_FAIL:
			error_message = sp_last_error_message();
			printf("Error: Failed: %s\n", error_message);
			sp_free_error_message(error_message);
			abort();
		case SP_ERR_SUPP:
			printf("Error: Not supported.\n");
			abort();
		case SP_ERR_MEM:
			printf("Error: Couldn't allocate memory.\n");
			abort();
		case SP_OK:
		default:
			return result;
	}
}

int main()
{	
	struct sp_port *port;
	check(sp_get_port_by_name("/dev/ttyACM0", &port));
	check(sp_open(port, SP_MODE_READ_WRITE));
	check(sp_set_baudrate(port, 115200));
#if 0 // seems unnecesary
	check(sp_set_bits(port, 8));
	check(sp_set_parity(port, SP_PARITY_NONE));
	check(sp_set_stopbits(port, 1));
	check(sp_set_flowcontrol(port, SP_FLOWCONTROL_NONE));
#endif

	for(;;) {
		char buf[1000]; // big size doesn't seem to hurt
		enum sp_return n = sp_nonblocking_read(port, buf, sizeof(buf));
		check(n);
		if(n>0) {
			write(1, buf, n);
			fflush(stdout);
		}
		usleep(100);
	}


	check(sp_close(port));
	sp_free_port(port);
	return 0;
}
