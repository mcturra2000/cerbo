Communicate with a serial port

Needs
```
sudo dnf install libserial libserial-devel libserial-doc
```

## References

[libserialport](http://sigrok.org/api/libserialport/unstable/)
