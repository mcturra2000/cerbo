/* 2022-11-21 Created
 * Some key seuqnces:
 *
 * \n = 10, \r = 13
 *
 * Page Up: (decimal):
 * 27  91 53 126
 * ESC [  5  ~
 *
 * Page Down: (decimal):
 * 27  91 54 126
 * ESC [  6  ~
 */
#include <stdio.h>

int main()
{

	while(1) {
		int c = getchar();
		printf("%d ", c);
	}
	return 0;
}
