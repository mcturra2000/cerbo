#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stddef.h>


#define W 1920
#define H 1080
#define D 4	// depth is 3 bytes
int main()
{
	int fd = open("/dev/fb0", O_RDWR);
	char* fbmem = mmap(NULL, 1920 * 1080 * D, PROT_WRITE, MAP_SHARED, fd, 0);

	fbmem += 200 * W * D + 100 * D;
	for(int y=0; y<360; y++) {
		for(int x=0; x<480; x++) {
			fbmem[x*D] = 255;
			fbmem[x*D +1 ] = 0;
			fbmem[x*D +2] = 0;
		}
		fbmem += W * D;
	}
	//close(fd);
	return 0;
}
