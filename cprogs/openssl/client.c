/*
 * 2023-06-08	Created
 * https://vocal.com/resources/development/how-do-i-make-a-tls-client-connection-using-openssl/
 */
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <openssl/ssl.h>
#include <errno.h>
#include <netdb.h>


int main (int argc, char *argv[])
{
	int s, result, status;
	char buf[64];
	SSL *ssl;


	/* Create a TLS client context with a CA certificate */
	SSL_CTX *ctx = SSL_CTX_new(TLS_client_method());
	//status = SSL_CTX_use_certificate_file(ctx, "ca.crt", SSL_FILETYPE_PEM);
	status = SSL_CTX_use_certificate_file(ctx, "/home/pi/.gemini/cert.pem", SSL_FILETYPE_PEM);
	if(status != 1) {
		puts("SSL_CTX_use_certificate_file failure");
	}


	struct hostent *he = gethostbyname("blinkyshark.chickenkiller.com");
	if(he == 0) {
		puts("gethostbyname failed");
	}
	char *ip = inet_ntoa(*(struct in_addr*)he->h_addr_list[0]);
	printf("ip is %s\n", ip);



	/* Set the address and port of the server to connect to */
	struct sockaddr_in srv_addr;
	memset(&srv_addr, 0, sizeof(struct sockaddr_in));
	srv_addr.sin_family = AF_INET;
	//srv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	srv_addr.sin_addr.s_addr = inet_addr(ip);
	srv_addr.sin_port = htons(4433);
	/*
	status = inet_pton(AF_INET, "127.0.0.1", &srv_addr.sin_addr);
	//status = inet_pton(AF_INET, "192.168.0.13", &srv_addr.sin_addr);
	//status = inet_pton(AF_INET, "blinkyshark.chickenkiller.com", &srv_addr.sin_addr);
	if(status == 0) {
		puts("inet_pton source does not contain valid network address");
	} else if(status == -1) {
		puts("inet_pton: af does not contain a valid address family");
	}
	*/



	/* Create a socket and SSL session */
	s = socket(AF_INET, SOCK_STREAM, 0);
	if(s== -1) {
		printf("socket: error %d\n", errno);
	}
	ssl = SSL_new(ctx);
	status = SSL_set_fd(ssl, s);
	if(status== 0) {
		puts("SSL_set_fd failed");
	}


	/* Try to connect */
	result = connect(s, (struct sockaddr *)&srv_addr, sizeof(srv_addr));
	if (result == 0) {
		/* Run the OpenSSL handshake */
		result = SSL_connect(ssl);

		/* Exchange some data if the connection succeeded */
		if (result == 1) {
			sprintf(buf, "Hello from the client");
			SSL_write(ssl, buf, strlen(buf) + 0);
			size_t len;
			SSL_read_ex(ssl, buf, sizeof(buf)-1, &len);
			buf[len] = 0;
			printf("Received message from server: ‘%s’\n", buf);
		} else {
			puts("SSL_connect failed");
		}
	} else {
		printf("connect failed with error %d\n", errno);
	}



	/* Done */
	close(s);
	SSL_free(ssl);
	SSL_CTX_free(ctx);
	return 0;

}
