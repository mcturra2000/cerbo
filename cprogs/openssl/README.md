# Openssl server and client (gemini)

```
sudo apt install libssl-dev libssl-doc
```

## Example

Example session:
```
C: gemini://example.com/<CRLF>
S: 20 text/gemini<CRLF>
S: Hello from gemini<CRLF>
```


## Diagnostics

You can find errors using the errno command. E.g.
```
errno 111
ECONNREFUSED 111 Connection refused
```

```
netstat -tupan
``


## Status

2023-06-08	Started. Working
