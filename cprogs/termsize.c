/*
 * get the size of the terminal
 *
 * see also TIOCSWINSZ for setting terminal size
 *
 * 2022-12-04	Started
 */

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <termios.h>

typedef struct {
	unsigned short ws_row; // number of rows, in characters
	unsigned short ws_col; // number of cols, in (single-byte) chars
	unsigned short ws_xpixel; // horizontal size of the window in pixeds
	unsigned short ws_ypixel; // vertical size of the window in pixeds
} ttysize_t;

void say(char *src, unsigned short val)  {printf("%s\t%d\n", src, val); }

int main()
{
	//int fd = open("/dev/tty", O_RDONLY);
	int fd = STDIN_FILENO; // 25/5 used stdin instead

	ttysize_t ttys;
	int ret = ioctl(fd, TIOCGWINSZ, &ttys);
	if(ret==-1) { perror(strerror(errno)); }
	say("num rows:", ttys.ws_row);
	say("num cols:", ttys.ws_col);
	say("num xpixels:", ttys.ws_xpixel);
	say("num ypixels:", ttys.ws_ypixel);


	//close(fd);
	return 0;
}
