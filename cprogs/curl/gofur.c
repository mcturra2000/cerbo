#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <curl/curl.h>


struct memory {
	char *response;
	size_t size;
};

static size_t cb(void *data, size_t size, size_t nmemb, void *clientp)
{
	size_t realsize = size * nmemb;
	struct memory *mem = (struct memory *)clientp;
	puts("cb called");

	char *ptr = realloc(mem->response, mem->size + realsize + 1);
	if(ptr == NULL)
		return 0;  /* out of memory! */

	mem->response = ptr;
	memcpy(&(mem->response[mem->size]), data, realsize);
	mem->size += realsize;
	mem->response[mem->size] = 0;

	return realsize;
}


int main(void)
{
	struct memory chunk = {0};
	CURLcode res;
	CURL *curl_handle = curl_easy_init();

	if (!curl_handle)
	{
		puts("Couldn't get gurl handle");
		return 1;
	}
	curl_easy_setopt(curl_handle, CURLOPT_URL, "gopher://tozip.chickenkiller.com/1/chirps.gmi");
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, cb); // callback
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk); // data for callback

	res = curl_easy_perform(curl_handle); // send a request

	printf("Data received: %s", chunk.response);
	/* remember to free the buffer */
	free(chunk.response);

	curl_easy_cleanup(curl_handle);
	return 0;
}
