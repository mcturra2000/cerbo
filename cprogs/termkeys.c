/*
 * show raw key codes of terminal keys input
 *
 * 2022-12-05	Started. Works
 */

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <termios.h>
#include <stdlib.h>


// Ctrl-C will terminate the program


int main()
{
	struct termios term, term_ori;
	int fd = open("/dev/tty", O_RDONLY);
	if(tcgetattr(fd, &term) < 0) exit(1); // what are the current attributes?
	term_ori = term; // create a copy for later restoration
	term.c_lflag &= ~ICANON; // don't wait for newline
	if(tcsetattr(fd, TCSANOW, &term)) exit(1); // set the attributes how we wish


	unsigned char buf[8]; // use unsigned rather than signed. Important for e.g. ¬
	memset(buf, 0, sizeof(buf));
	int n;
	while(n = read(fd, &buf, sizeof(buf))) {
		// print out decimal form
		printf("\t");
		for(int i = 0; i< sizeof(buf); i++) {
			printf(" %3d", buf[i]);
		}
		puts("");
		if(buf[0] == 'q') break;
		memset(buf, 0, sizeof(buf));
	}

	close(fd);
	if(tcsetattr(STDIN_FILENO, TCSANOW, &term_ori)) exit(1);
	return 0;
}
