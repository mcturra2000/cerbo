#include <stdbool.h>
#include <stdio.h>

/*
typedef union 
{
	int num;
} 
*/

typedef int  yystype_t;
#define YYSTYPE yystype_t
extern YYSTYPE yylval;

extern int yylex();
extern void yyerror(const char *);
extern int yyparse();
extern char* yytext;

void set_settings(int d, int o, int b);
void add_note(int duration, int note, bool extend_duration, int octave);
