/*
 *
 * ao_example.c
 *
 *     Written by Stan Seibert - July 2001
 *
 * Legal Terms:
 *
 *     This source file is released into the public domain.  It is
 *     distributed without any warranty; without even the implied
 *     warranty * of merchantability or fitness for a particular
 *     purpose.
 *
 * Function:
 *
 *     This program opens the default driver and plays a 440 Hz tone for
 *     one second.
 *
 * Compilation command line (for Linux systems):
 *
 *     gcc -o ao_example ao_example.c -lao -ldl -lm
 *
 */

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <ao/ao.h>
#include <math.h>

#include "rtone.h"

int settings_d, settings_o, settings_b;

typedef struct {
	int freq;
	int ms; // duration in milliseconds
} note_t;

#define MAX_NOTES 300
int num_notes = 0;
note_t notes[MAX_NOTES];

//            	  A       B       C     D     E       F       G
int oct8[] = 	{3520*2, 3951*2, 4186, 4699, 2637*2, 2794*2, 3136*2}; // non-sharp frequencies
int oct8_s[] = 	{3729*2, 0,      4435, 4978, 0,      2960*2, 3322*2}; // the # frequencies

void add_note(int duration, int note, bool extend_duration, int octave)
{
	static int idx = 0;
	int basic_note = note >> 8;
	int sharp = note & 0xFF;
	if(octave == -1) octave = settings_o;
	const int debug = 1;

	if(debug) {
		printf("note %d: %d%c", ++idx, duration, basic_note);
		if(sharp == '#') printf("#");
		if(extend_duration) printf(".");
		printf("%d  ", octave);
	}

	assert(num_notes < MAX_NOTES);
	note_t* n = &notes[num_notes];

	if(basic_note == 'p') {
		n->freq = 0;
	} else {
		int* oct = sharp ? oct8_s : oct8;
		n->freq = oct[basic_note-'a'] >> (8- octave);
	}

	if(duration==-1) duration = settings_d;
	n->ms = 60 * 1000 / settings_b * 4 / duration ;
	if(extend_duration) n->ms = (3 *n->ms) / 2;

	if(debug) {
		printf("%d %d\n", n->freq, n->ms);
	}

	num_notes++;
}

void yyerror(const char* msg)
{
	printf("Syntac error: %s\n", msg);
}

#define BUF_SIZE 4096

/*
   char get() {
   return getchar();
   }

   char song_name[20];

   enum YYSTYPE { Title, Params };
   enum YYSTYPE yystype;

   int yyparse(void)
   {
   static void* label = &&title;
   goto *label;
   int name_pos = 0;
   char c;
title:
while((c = get()) != ':') song_name[name_pos++] = c;
song_name[name_pos] = 0;
yystype = Title;
label = &&params;
return 0;
params:
yystype = Params
expect('d');
expect('=');
param_d = get_num();
while(c = get()
c = get();
assert(c == 'd'
fin:
return 1;
}
*/
void set_settings(int d, int o, int b)
{
	settings_d = d;
	settings_o = o;
	settings_b = b;
}

ao_device *device;
ao_sample_format format;
int default_driver;
char* buffer;
int buf_size;

void tone_init(void)
{
	ao_initialize();
	default_driver = ao_default_driver_id();

	memset(&format, 0, sizeof(format));
	format.bits = 16;
	format.channels = 2;
	format.rate = 44100;
	format.byte_format = AO_FMT_LITTLE;

	/* -- Open driver -- */
	device = ao_open_live(default_driver, &format, NULL /* no options */);
	if (device == NULL) {
		fprintf(stderr, "Error opening device.\n");
		exit(1);
	}

	buf_size = format.bits/8 * format.channels * format.rate * 20; // for 20 seconds
	buffer = calloc(buf_size, sizeof(char));
	assert(buffer);
}

void tone_deinit(void)
{
	ao_close(device);
	ao_shutdown();
}

void tone (int freq, int ms)
{
	//char *buffer;
	int sample;
	//float freq = 440.0;
	int i;
	int nsamples = 44100 * ms / 1000;
	float fs = 44100;
	float dt = 1.0 / (float)fs;
	float t0 = 0;
	//float fw = 500;
	float fw_t0 = 1.0/(float)freq;
	for(int i=0; i< nsamples; i++) {
		sample = 0;

		//bool hi0= false;
		t0 = fmod(t0, fw_t0);
		if(t0>=0.5*fw_t0) sample = 5000;
		t0 += dt;

		buffer[4*i] = buffer[4*i+2] = sample & 0xff;
		buffer[4*i+1] = buffer[4*i+3] = (sample >> 8) & 0xff;
	}

	ao_play(device, buffer, nsamples * 4 );

}

int main(int argc, char **argv)
{


	fprintf(stderr, "libao example program\n");

	yyparse();
	//return 0;


	tone_init();

	for(int i = 0; i< num_notes; i++) {
		note_t* n = &notes[i];
		tone(n->freq, n->ms);
	}
	//tone(1000, 500);

	tone_deinit();

	return (0);
}
