%{
//#include <iostream>
#include "rtone.h"
#include "yacc.tab.h"

//using namespace std;

%}

%option noyywrap

delim		[ \t\n\r\v]
ws		{delim}+
text		[a-zA-Z\- ]{2,}
num		[[:digit:]]+


%%

{ws}		{ }
{text}		{ puts("title"); return TITLE; }
{num}		{ yylval = atoi(yytext); return NUM; }
.		{ yylval = yytext[0]; return (int) yytext[0]; }
