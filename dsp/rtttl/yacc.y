%{
#include "rtone.h"
%}

%token TITLE
%token NUM

%%

rtttl : title ':' settings ':' note_list 
      ;
       

title : TITLE {printf("title=%s\n", yytext); }
      | error {puts("title error"); }

settings	:  'd' '=' NUM ',' 'o' '=' NUM ',' 'b' '=' NUM { set_settings($3, $7, $11); }
	 	;

note_list 	: note { puts("found note"); }
	  	| note_list ',' note
		;


note 		: /* might be empty */
       		| key			{ add_note(-1, $1, false, -1); }
       		| key NUM		{ add_note(-1, $1, false, $2); }
       		| key '.' 		{ add_note(-1, $1, true, -1); }
       		| key '.' NUM		{ add_note(-1, $1, true, $3); }
       		| NUM key		{ add_note($1, $2, false, -1); }
       		| NUM key '.'		{ add_note($1, $2, true, -1); }
		| NUM key '.' NUM	{ add_note($1, $2, true, $4); }
       		| NUM key NUM		{ add_note($1, $2, false, $3); }
     		;

key 		: basic { $$ = $1 << 8; }
    		| basic '#' {$$ = $1 << 8 | '#'; }
		;

basic		: 'a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'p'
       		;

%%

