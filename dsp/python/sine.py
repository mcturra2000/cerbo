# Works perfectly
# 2024-08-03    Created. Works perfectly

#import fcntl
#from time import sleep
import pyaudio # dnf install python3-pyaudio
import math

import wave
import sys
import struct

import pyaudio

def sine():
    fs = 16000 # sampling frequency
    p = pyaudio.PyAudio()
    form = pyaudio.get_format_from_width(2, unsigned = False) # 16-bit signed little-endian
    stream = p.open(format= form, # sample size in bytes
    rate = 16000,
    channels =1, output = True)

    dt = 1.0/fs
    t = 0.0
    pi2 = 2.0 * math.pi
    fw = 400 # wave frequency
    w = pi2 * fw # angular velocity

    while True:
        b = math.sin(w * t) * (2**15)
        b = math.floor(b)
        if b == 2**15: b -= 1
        b = struct.pack("<h", b) # # 2-byte signed
        stream.write(b)
        t += dt
        if t > 1: t -= 1 # prevent sine calcs going daft
    fpin.close()
    stream.close()
    p.terminate()

sine()

def play_from_pipe():
    p = pyaudio.PyAudio()
    form = pyaudio.get_format_from_width(2, # number of bytes per sample
    unsigned = False)
    #form = pyaudio.paUInt16
    stream = p.open(format= form, # sample size in bytes
    rate = 16000,
    channels =1, output = True)

    fpin = open("music.0", "rb")
    sz = fcntl.fcntl(fpin, fcntl.F_SETPIPE_SZ, 1024) # restrict the size of the pipe, just because
    print("Size of pipe:", sz)

    while True:
        b = fpin.read(1024)
        if len(b) == 0: 
            sleep(0.01)
            continue
        stream.write(b)
    fpin.close()
    stream.close()
    p.terminate()

#play_from_pipe()