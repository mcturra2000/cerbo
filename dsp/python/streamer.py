import fcntl
from time import sleep
import pyaudio # dnf install python3-pyaudio


import wave
import sys

import pyaudio


CHUNK = 1024

def eg1():
    if len(sys.argv) < 2:
        print(f'Plays a wave file. Usage: {sys.argv[0]} filename.wav')
        sys.exit(-1)

    with wave.open(sys.argv[1], 'rb') as wf:
        # Instantiate PyAudio and initialize PortAudio system resources (1)
        p = pyaudio.PyAudio()

        # Open stream (2)
        stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                        channels=wf.getnchannels(),
                        rate=wf.getframerate(),
                        output=True)

        # Play samples from the wave file (3)
        while len(data := wf.readframes(CHUNK)):  # Requires Python 3.8+ for :=
            stream.write(data)

        # Close stream (4)
        stream.close()

        # Release PortAudio system resources (5)
        p.terminate()


def fyle():
	fd = open("music.0", "r")
	sz = fcntl.fcntl(fd, fcntl.F_GETPIPE_SZ)
	print(sz)
	sz = fcntl.fcntl(fd, fcntl.F_SETPIPE_SZ, 1024)
	print(sz)

def play_sample():
    p = pyaudio.PyAudio()
    form = pyaudio.get_format_from_width(1, unsigned = True)
    stream = p.open(format= form, # sample size in bytes
    rate = 16000,
    channels =1, output = True)

    fpin = open("/home/pi/Music/sheep-16k.raw", "rb")
    while True:
        b = fpin.read(512)
        if len(b) == 0: break
        stream.write(b)
    fpin.close()
    stream.close()
    p.terminate()

def play_from_pipe():
    p = pyaudio.PyAudio()
    form = pyaudio.get_format_from_width(2, # number of bytes per sample
    unsigned = False)
    #form = pyaudio.paUInt16
    stream = p.open(format= form, # sample size in bytes
    rate = 16000,
    channels =1, output = True)

    fpin = open("music.0", "rb")
    sz = fcntl.fcntl(fpin, fcntl.F_SETPIPE_SZ, 1024) # restrict the size of the pipe, just because
    print("Size of pipe:", sz)

    while True:
        b = fpin.read(1024)
        if len(b) == 0: 
            sleep(0.01)
            continue
        stream.write(b)
    fpin.close()
    stream.close()
    p.terminate()

play_from_pipe()