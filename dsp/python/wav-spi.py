import spidev, time, wave

# notes about transferring multiple values
# to_send = [0x01, 0x02]
# hi, lo = spi.readbytes(2)
# val = (hi << 8) + lo
# print(val)

tx = 0
#nbits = 16

def loop(spi):
	global tx
	#rx = spi.xfer([tx], bits_per_word = nbits)
	tx_lo = tx & 0xFF
	tx_hi = (tx >> 8) & 0xFF
	rx_hi, rx_lo = spi.xfer([tx_hi, tx_lo])
	rx = (rx_hi<<8) + rx_lo
	print(tx, rx)
	tx += 1
	tx = tx & 0xFFFF
	#if tx >= 1<<(nbits-1): tx = 0
	time.sleep(1.0)


def main1():
	#global spi, nbits
	spi = spidev.SpiDev()
	try:
		spi.open(0, 0)
		#spi.bits = nbits
		spi.max_speed_hz = 32_000_000 # 32MHz, at least, seems OK for STM32L432
		while True:
			loop(spi)
	finally:
		spi.close()

def main():
	try:
		wav = wave.open("/home/pi/Music/sheep.wav")
		print("Num channels:", wav.getnchannels())
		print("Framerate:", wav.getframerate())
		print("Num frames:", wav.getnframes())
		print("Sample width:", wav.getsampwidth())
	finally:
		wav.close()



if __name__ == "__main__":
	main()
