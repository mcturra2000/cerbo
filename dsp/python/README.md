
## `streamer.py`

Allows you to stream files to a named pipe.

One off:
```
mkfifo music.0
```

Start the streamer:
```
python streamer.py
```
It assumes that you are using signed 16-bit little-endian single-channel sound at 16kHz

To play a sound:
```
cat ~/Music/moonlight-s16le-16k.raw > music.0
```


2024-08-02  Started. Works
