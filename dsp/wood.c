/* Percusive woodedn sound
 * 2023-03-08	Created
 */
#include <stdlib.h>
#include <stdio.h>

int main()
{
	for(int i = 255; i >0; i--) {
		int j = rand() % 3 == 0 ? i : 0;
		putchar(j);		
	}
	return 0;
}
