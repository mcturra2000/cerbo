# playwav (blocking)

Assumes 2 channel input at 44.10kHz.

## Status

2021-12-25 	Confirmed working for: blocking. Non-blocking fails.

2021-06-23 	Started. Working.
