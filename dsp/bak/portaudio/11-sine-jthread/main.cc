#include <assert.h>
#include <iostream>
#include <sndfile.h>
#include <thread>
#include <semaphore>
#include <unistd.h>
#include <portaudio.h>
#include <atomic>
#include <string.h>
#include <math.h>
#include <chrono>
#include <semaphore.h>

using namespace std;
using namespace std::chrono;

#define FPB 512

static_assert(sizeof(float) == 4);


constexpr float sample_freq = 44000.0;
constexpr float dt = 1.0/sample_freq;
constexpr float sine_freq = 440.0; // Hz
constexpr float pi = 3.1412;
constexpr float w = 2.0 * pi * sine_freq; // angular frequency
//typedef paInt16 dtype;

float buff[2][FPB];

void _check(int line, PaError err)
{
	if(err == paNoError) return;
	const char* msg = Pa_GetErrorText(err);
	printf("Failed:%d:%s\n", line, msg);
	exit(1);
}


#define CHECK() _check(__LINE__, paerr);


PaError paerr;
PaStream* strm;

//int signal_fill = -1;
static sem_t sem;
int playing = 0;

void make_data()
{
	float t = 0;
	while(1) {
		sem_wait(&sem);
		//while(signal_fill == -1) usleep(1000); // we can actually sleep for a long time, 10'000 will be a little glitchy, though
		int loc = 1- playing;
		//signal_fill = -1;
		for(int i = 0; i< FPB; ++i) {
			buff[loc][i] = sin(w * t);
			t += dt;
		}
		t = fmod(t, 1.0/sine_freq);
	}
}


int main()
{
	// init sound stream
	paerr = Pa_Initialize();
	CHECK();
	paerr = Pa_OpenDefaultStream(&strm, 
			0, // number input channels
			1, // number output channels
			paFloat32,
			sample_freq, // sample rate
			FPB, // frames per buffer
			NULL, // callback 0 implies blocking
			NULL);
	CHECK();
	paerr = Pa_StartStream(strm);
	CHECK();


	sem_init(&sem, 0, 0);
	jthread thr{make_data};
	//int playing = 0;

	while(1) {
		sem_post(&sem);
		//signal_fill = 1 - playing;
		paerr = Pa_WriteStream(strm, buff[playing], FPB);
		CHECK();

		playing = 1 - playing;
	}

	Pa_StopStream(strm);
	Pa_CloseStream(strm);
	paerr = Pa_Terminate();
	CHECK();
	return 0;
}
