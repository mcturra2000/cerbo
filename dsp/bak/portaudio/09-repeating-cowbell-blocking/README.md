Play a cowbell 20 times, then stop,


WAV files are expected to be 2-channel 32-bit signed floats at 44.1kHz.
wav\_dump  will output 1 channel, 32-bit signed float. To play the file:
```
aplay -r 44100 -f FLOAT_LE out.raw
```


## Status

2021-12-16	Started. Works
