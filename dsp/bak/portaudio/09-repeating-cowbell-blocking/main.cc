#include <algorithm>
#include <assert.h>
#include <bitset>
#include <iostream>
#include <sndfile.h>
#include <thread>
#include <semaphore.h>
#include <unistd.h>
#include <portaudio.h>
#include <atomic>
#include <strings.h>
#include <math.h>
#include <chrono>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>


#define BLOCKING 1

static sem_t sem;

using namespace std;
using namespace std::chrono;

#define FPB 512
//#define FPB (512*4)

typedef float wav_t;
typedef vector<wav_t> wvec_t;

static_assert(sizeof(float) == 4);

constexpr float sample_freq = 44000.0;
constexpr float dt = 1.0/sample_freq;
constexpr float sine_freq = 440.0; // Hz
constexpr float pi = 3.1412;
constexpr float w = 2.0 * pi * sine_freq; // angular frequency
//typedef paInt16 dtype;


void _check(int line, PaError err)
{
	if(err == paNoError) return;
	const char* msg = Pa_GetErrorText(err);
	printf("Failed:%d:%s\n", line, msg);
	exit(1);
}


#define CHECK() _check(__LINE__, paerr);


PaError paerr;
PaStream* strm;

vector<wav_t>  wav_load(const string& filename)
{
	vector<wav_t> result;
	wav_t buff[512*2]; // two channels in
	SF_INFO sfinfo;
	sfinfo.format = 0;
	assert(sizeof(float) == 4);

	SNDFILE* sndfile = sf_open(filename.c_str(), SFM_READ, &sfinfo);
	assert(sndfile);
	while(sf_count_t nread = sf_readf_float(sndfile, buff, 512)) {
		// just take the first channel
		//printf(".");
		//cout <<".";
		for(int i = 0; i < min((sf_count_t) 512, nread); i++) {
			result.push_back(buff[i*2+1]);
		}
	}


	return result;
}

void wav_dump(const vector<wav_t>& sample)
{
#if 1
	FILE* fp = fopen("out.raw", "wb");
	assert(fp);
	fwrite(sample.data(), sizeof(float), sample.size(), fp);
	fclose(fp);
#else
	ofstream raw;
	raw.open("out.raw", ios::out | ios::binary);
	for(auto s : sample) 
		raw << (float)s;
	raw.close();
#endif
}


void play (const wvec_t& wav)
{
	float empty[512];
	bzero(empty, sizeof(empty));

	for(int i = 0; i < 20; i++) {
		int idx = 0;
		while(1) {

			unsigned long frames = min(wav.size() -idx, (long unsigned) 512);
			//printf("frames=%ld\n", frames);
			//float buff[512];
			paerr = Pa_WriteStream(strm, wav.data() + idx, frames);
			CHECK();
			idx += 512;
			if(frames<512) break;
		}

		// a bit of silence. 100*512/44100 = 1.16 secs
		for(int j = 0; j < 100; j++) {
                        paerr = Pa_WriteStream(strm, empty, 512);
			CHECK();
		}


	}
}

int main()
{
	//cout << sizeof(short) << "\n"; // 16 bits
	//vector<int16_t> cow1 = wav_load("/home/pi/Music/2021/cowbell-1.wav");

	string filename{"/home/pi/Music/sheep.wav"};
	filename = "/home/pi/Music/2021/Cowbell-2.wav";
	vector<wav_t> cow1 = wav_load(filename);
	wav_dump(cow1);
	//exit(0);

	// init sound stream
	paerr = Pa_Initialize();
	CHECK();
	paerr = Pa_OpenDefaultStream(&strm, 
			0, // number input channels
			1, // number output channels
			paFloat32,
			sample_freq, // sample rate
			FPB, // frames per buffer
			NULL, // callback 0 implies blocking
			NULL);
	CHECK();
	paerr = Pa_StartStream(strm);
	CHECK();

	play(cow1);

#if 0
	int nblocks = 1000;
	nblocks = 1;
	float  buff[FPB*nblocks];
	auto start = high_resolution_clock::now();
	float t = 0;
	for(int i = 0; i< FPB*nblocks; ++i) {
		buff[i] = (sin(w*t) + sin(w*1.15*t))/2.0;
		t+= dt;
	}
	auto stop = high_resolution_clock::now();
	auto duration = duration_cast<microseconds>(stop - start);
	cout << "Time taken by function: " << duration.count() << " microseconds" << endl;

	while(1) {
		for(int i = 0; i< FPB; ++i) {
			//buff[i] = (sin(w * t) + sin(w*1.01*t+1.40001) + 0*sin(w*1.1*t))/3.0 ;
			buff[i] = sin(w*t) < 0.0  ? 1.0 : 0.0;
			buff[i] += sin(w*t*1.02) < 0.0  ? 1.0 : 0.0;
			buff[i] += sin(w*t*1.05) < 0.0  ? 1.0 : 0.0;
			buff[i] += sin(w*t*1.10) < 0.0  ? 1.0 : 0.0;
			buff[i] /= 4.0;
			//buff[i] =  i *2 < FPB   ? 1.0 : -1;
			t += dt;
		}
		paerr = Pa_WriteStream(strm, buff, FPB);
		CHECK();

		//t = fmod(t, 1.0/sine_freq); 
	}
#endif

	Pa_StopStream(strm);
	Pa_CloseStream(strm);
	paerr = Pa_Terminate();
	CHECK();
	return 0;
}
