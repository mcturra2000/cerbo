#include <array>
//#include <experimental/array>
#include <assert.h>
#include <iostream>
#include <sndfile.h>
#include <semaphore.h>
#include <unistd.h>
#include <portaudio.h>
#include <atomic>
#include <string.h>
#include <math.h>
#include <chrono>
#include <cstdlib>
#include <ctime>
#include <signal.h>
#include <stdio.h>
#include <thread>
#include <vector>

#include "gui.h"


using namespace std;
using namespace std::chrono;

typedef float wav_t;
typedef vector<wav_t> wvec_t;


atomic<bool> program_is_running{true};


#define FPB 512 // frames per buffer
PaStream* strm;
array<Fl_Box*, 16> leds;
array<Fl_Light_Button*, 16> trigs;
const auto sample_freq = 44100;

class clip {
	public:
		clip();
		void load(const string& filename);
		wav_t* data();
		size_t size();

	private:
		wvec_t buffer;
} cow1;

clip::clip() {}

wav_t* clip::data() { return buffer.data(); }

size_t clip::size() { return buffer.size(); }

void clip::load(const string& filename)
{
	buffer.clear();
	wav_t buff[512*2]; // two channels in
	SF_INFO sfinfo;
	sfinfo.format = 0;
	assert(sizeof(float) == 4);

	SNDFILE* sndfile = sf_open(filename.c_str(), SFM_READ, &sfinfo);
	assert(sndfile);
	while(sf_count_t nread = sf_readf_float(sndfile, buff, 512)) {
		for(int i = 0; i < min((sf_count_t) 512, nread); i++) {
			buffer.push_back(buff[i*2+1]);
		}
	}
}


bool gui_ready = false;
void gui_thread()
{
	Fl_Double_Window* root = make_window();
	leds = array<Fl_Box*, 16>{box0, box1, box2, box3, box4, box5, box6, box7, 
		box8, box9, boxA, boxB, boxC, boxD, boxE, boxF};
	trigs = array<Fl_Light_Button*, 16>{trig0, trig1, trig2, trig3, trig4, trig5, trig6, trig7, 
		trig8, trig9, trigA, trigB, trigC, trigD, trigE, trigF};
	root->show();
	gui_ready = true;
	auto res = Fl::run();
	program_is_running = false;
}

void pa_check(int line, PaError err)
{
	if(err == paNoError) return;
	const char* msg = Pa_GetErrorText(err);
	fprintf(stderr, "Failed:%d:%s\n", line, msg);
	exit(1);
}


#define PACHECK(paerr) pa_check(__LINE__, paerr);



void sound_thread(void)
{
	// TODO
}

void play(clip& a_clip)
{
	int idx = 0;
	PACHECK(Pa_StartStream(strm));
	while(1) {
		unsigned long frames = min(a_clip.size() -idx, (long unsigned) 512);
		//printf("frames=%ld\n", frames);
		//float buff[512];
		PACHECK(Pa_WriteStream(strm, a_clip.data() + idx, frames));
		idx += 512;
		if(frames<512) break;
	}
	PACHECK(Pa_StopStream(strm));

}
void sound_init(void)
{
	PaError paerr;
	PACHECK(Pa_Initialize());
	paerr = Pa_OpenDefaultStream(&strm,
			0, // number input channels
			1, // number output channels
			paFloat32,
			sample_freq, // sample rate
			FPB, // frames per buffer
			NULL, // callback 0 implies blocking
			NULL);
	PACHECK(paerr);
	//PACHECK(Pa_StartStream(strm));
}

void sound_deinit(void)
{
	//PACHECK(Pa_StopStream(strm));
	Pa_CloseStream(strm);
	PACHECK(Pa_Terminate());
}

void exit_function()
{
	puts("exit_function:called");
	sound_deinit();
}

void intHandler(int dummy) { exit(0); }


int main()
{
	int status;
	signal(SIGINT, intHandler);
	atexit(exit_function);

	cow1.load("/home/pi/Music/2021/Cowbell-1.wav");
	sound_init();
	jthread jgui(gui_thread);
	jthread jsound(sound_thread);


	while(!gui_ready);
	//sleep(1); // needed to stop segfault
	int led_active = -1;
	while(program_is_running) {
		led_active++;
		if(led_active>= leds.size()) led_active = 0;
		cout << "active led: " << led_active << endl;

		leds[led_active]->color(FL_GREEN);
		leds[led_active]->redraw(); // seems necessary
		Fl::check();

		if(trigs[led_active]->value())
			play(cow1);

		usleep(500'000);

		//if(led_active>=0) {
		leds[led_active]->color(FL_BACKGROUND_COLOR);
		leds[led_active]->redraw(); // seems necessary
		cout << "deactive led: " << led_active << endl;
		//}
		//for(int i = 0; i < 1000000; i++)
		//	volatile auto j = i;
	}

	return 0;
}

