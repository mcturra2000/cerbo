#include <assert.h>
#include <iostream>
#include <sndfile.h>
#include <thread>
#include <semaphore>
#include <unistd.h>
#include <portaudio.h>
#include <atomic>
#include <string.h>
#include <math.h>
#include <chrono>
#include <semaphore.h>


#include "gui.h"


using namespace std;
using namespace std::chrono;

#define FPB 512

static_assert(sizeof(float) == 4);

constexpr float sample_freq = 44000.0;
constexpr float dt = 1.0/sample_freq;
constexpr float sine_freq = 440.0; // Hz
constexpr float pi = 3.1412;
constexpr float w = 2.0 * pi * sine_freq; // angular frequency
//typedef paInt16 dtype;

float fc = 8000; // cut-off frequency
float coeff[2]; // coeffs as per db07.24
void coeff_update(void)
{
	float alpha = 2.0 * 3.1412 * fc / sample_freq;
	coeff[1] = 1.0 / (1.0+ alpha);
	coeff[0] = alpha * coeff[1];
	 cout << "coeff_update: " << coeff[0] << "," << coeff[1] << endl;
}

bool keep_generating = true;
float buff[2][FPB];

void _check(int line, PaError err)
{
	if(err == paNoError) return;
	const char* msg = Pa_GetErrorText(err);
	printf("Failed:%d:%s\n", line, msg);
	exit(1);
}


#define CHECK() _check(__LINE__, paerr);


PaError paerr;
PaStream* strm;

//std::binary_semaphore sem{0};
//std::counting_semaphore<1> sem(0);
int playing = 0;
int vol_is_on = 1;

static sem_t sem;

void make_data()
{
	float vout = 0;
	while(keep_generating) {
		sem_wait(&sem);
		//while(signal_fill == -1) usleep(100); // we can actually sleep for a long time, 10'000 will be a little glitchy, though
		int loc = 1-playing;
		//signal_fill = -1;
		for(int i = 0; i< FPB; ++i) {
			//buff[loc][i] = sin(w * t);
			float vin = (float(rand())/float((RAND_MAX))) * 2.0 - 1.0;
			vout = coeff[0] * vin + coeff[1] * vout;
			buff[loc][i] = vout;
			if(!vol_is_on) buff[loc][i] = 0.0;
			//t += dt;
		}
		//t = fmod(t, 1.0/sine_freq);
	}
}

void slider_callback(Fl_Value_Slider* slider, void* data)
{
	fc = slider->value();
	coeff_update();
}

void vol_changed_callback(Fl_Slider* slider, void* data)
{
}

void vol_on_changed(Fl_Check_Button* btn, void* data)
{
	vol_is_on = btn->value();
}


void gui_thread()
{
	Fl_Double_Window* root = make_window();
	root->show();
	auto res = Fl::run();
	keep_generating = false;
	cout << "gui_thread:exiting" << endl;

}

int main()
{
	// init sound stream
	paerr = Pa_Initialize();
	CHECK();
	paerr = Pa_OpenDefaultStream(&strm, 
			0, // number input channels
			1, // number output channels
			paFloat32,
			sample_freq, // sample rate
			FPB, // frames per buffer
			NULL, // callback 0 implies blocking
			NULL);
	CHECK();
	paerr = Pa_StartStream(strm);
	CHECK();

	sem_init(&sem, 0, 0);
	jthread thr{make_data};
	jthread thr_gui(gui_thread);

	coeff_update();

	//int playing = 0;


	while(keep_generating) {
		//signal_fill = 1 - playing;
		paerr = Pa_WriteStream(strm, buff[playing], FPB);
		CHECK();
		sem_post(&sem);
		playing = 1 - playing;
	}
	cout << "main: cleaning up" << endl;

	Pa_StopStream(strm);
	Pa_CloseStream(strm);
	paerr = Pa_Terminate();
	CHECK();
	thr_gui.join();
	cout << "thr_gui:joined" << endl;
	//signal_fill = 0; // trick make_data() into looking for keep_generating
	sem_post(&sem);
	thr.join();

	return 0;
}
