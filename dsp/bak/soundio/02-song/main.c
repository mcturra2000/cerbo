#include <soundio/soundio.h>

#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int more = 1;
uint8_t *buffer;
long s; // buffer size
long idx = 0;
#if 1
void slurp_file(void)
{
	//char *buffer;
	FILE *fh = fopen("/home/pi/Music/dubstep-16k.raw", "rb");
	if ( fh != NULL )
	{
		fseek(fh, 0L, SEEK_END);
		s = ftell(fh);
		rewind(fh);
		buffer = malloc(s);
		if ( buffer != NULL )
		{
			fread(buffer, s, 1, fh);
			// we can now close the file
			fclose(fh); fh = NULL;

			// do something, e.g.
			//fwrite(buffer, s, 1, stdout);

			//free(buffer);
		}
		if (fh != NULL) fclose(fh);
	}
}
#endif

uint8_t get_sample(void)
{
#if 1
	uint8_t value = 0;
	if(idx>=s) {
		puts("get_sample: finished");
		more = 0;
	} else
		value = buffer[idx++];
	return value;
#else
	static FILE* fp;
	static int inited = 0;
	static uint8_t data[512];
	static size_t chunk_size, chunk_idx = 0;
	if(inited == 0) {
		inited = 1;
		fp = fopen("/home/pi/Music/dubstep-16k.raw", "rb");
		assert(fp);
		chunk_size = fread(data, 1, 512, fp);
	}


	uint8_t value = 0;
	if(chunk_idx < chunk_size) {
		value = data[chunk_idx];

	} else {
		chunk_size = fread(data, 1, 512, fp);
		chunk_idx = 0;
	}

	if(chunk_size == 0) {
		more = 0;
		fclose(fp);
	}

	chunk_idx++;
	return value;
#endif
}

//static const float PI = 3.1415926535f;
//static float seconds_offset = 0.0f;
static void write_callback(struct SoundIoOutStream *outstream,
		int frame_count_min, int frame_count_max)
{
	const struct SoundIoChannelLayout *layout = &outstream->layout;
	float float_sample_rate = outstream->sample_rate;
	float seconds_per_frame = 1.0f / float_sample_rate;
	struct SoundIoChannelArea *areas;
	int frames_left = frame_count_max;
	int err;

	while (frames_left > 0) {
		int frame_count = frames_left;

		if ((err = soundio_outstream_begin_write(outstream, &areas, &frame_count))) {
			fprintf(stderr, "%s\n", soundio_strerror(err));
			exit(1);
		}

		if (!frame_count)
			break;

		//float pitch = 440.0f;
		//float radians_per_second = pitch * 2.0f * PI;
		for (int frame = 0; frame < frame_count; frame += 1) {
			//float sample = sinf((seconds_offset + frame * seconds_per_frame) * radians_per_second);
			uint8_t	sample = get_sample();
			for (int channel = 0; channel < layout->channel_count; channel += 1) {
				uint8_t *ptr = (uint8_t*)(areas[channel].ptr + areas[channel].step * frame);
				*ptr = sample;
			}
		}
		//seconds_offset = fmodf(seconds_offset + seconds_per_frame * frame_count, 1.0f);

		if ((err = soundio_outstream_end_write(outstream))) {
			fprintf(stderr, "%s\n", soundio_strerror(err));
			exit(1);
		}

		frames_left -= frame_count;

		if(more == 0) {
			soundio_outstream_pause(outstream, 1);
			return;
		}
	}
}

int main(int argc, char **argv) {
	slurp_file();
	int err;
	struct SoundIo *soundio = soundio_create();
	if (!soundio) {
		fprintf(stderr, "out of memory\n");
		return 1;
	}

	if ((err = soundio_connect(soundio))) {
		fprintf(stderr, "error connecting: %s", soundio_strerror(err));
		return 1;
	}

	soundio_flush_events(soundio);

	int default_out_device_index = soundio_default_output_device_index(soundio);
	if (default_out_device_index < 0) {
		fprintf(stderr, "no output device found");
		return 1;
	}

	struct SoundIoDevice *device = soundio_get_output_device(soundio, default_out_device_index);
	if (!device) {
		fprintf(stderr, "out of memory");
		return 1;
	}

	fprintf(stderr, "Output device: %s\n", device->name);

	struct SoundIoOutStream *outstream = soundio_outstream_create(device);
	//outstream->format = SoundIoFormatFloat32NE;
	outstream->format = SoundIoFormatU8;
	outstream->sample_rate = 16000;
	outstream->write_callback = write_callback;

	if ((err = soundio_outstream_open(outstream))) {
		fprintf(stderr, "unable to open device: %s", soundio_strerror(err));
		return 1;
	}

	if (outstream->layout_error)
		fprintf(stderr, "unable to set channel layout: %s\n", soundio_strerror(outstream->layout_error));

	if ((err = soundio_outstream_start(outstream))) {
		fprintf(stderr, "unable to start device: %s", soundio_strerror(err));
		return 1;
	}

	while(more) 
		soundio_wait_events(soundio);

	soundio_outstream_destroy(outstream);
	soundio_device_unref(device);
	soundio_destroy(soundio);
	return 0;
}
