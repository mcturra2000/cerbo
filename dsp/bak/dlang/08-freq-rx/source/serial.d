import std.conv;
import std.stdio;
import serialport;
//import thread.osthread;
import core.thread;


shared int shared_freq = 440;




void serial_callback(string line)
{
	//writeln(".");
	shared_freq = to!int(line);
	//writeln(shared_freq);

}

void serial_thread()
{
	auto com1 = new SerialPortBlk("/dev/ttyACM0", 115200);
	void[1] serial_rx = void;
	string line;
	writeln("serial thread started");
	while(1) {
		try {
			auto res2 = com1.read(serial_rx);
			if(res2.length > 0) {
				auto str = cast(string)res2;
				if(str == "\n") continue;
				if(str == "\r") {
					serial_callback(line);
					line = "";
				} else {
					line ~= str;
				}
			}
		} catch (TimeoutException e) { }
	}
}
void serial_start()
{
	auto th_serial = new Thread(&serial_thread).start();
}
