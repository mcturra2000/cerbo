import std.conv;


extern (C) int  bcm2835_init();
extern (C) int  bcm2835_close();
extern (C) int  bcm2835_gpio_lev(ubyte pin);
extern (C) void bcm2835_gpio_set(ubyte pin);
extern (C) void bcm2835_gpio_clr(ubyte pin);
extern (C) void bcm2835_gpio_fsel(ubyte pin, ubyte mode);
extern (C) void	bcm2835_spi_transfernb (ubyte *tbuf, ubyte *rbuf, uint len);
extern (C) int 	bcm2835_spi_begin();
extern (C) void	bcm2835_spi_end();
extern (C) void bcm2835_spi_setClockDivider(uint divider);

enum BCM2835_GPIO_FSEL_INPT = 0; // input select


class Synth
{
        enum BCM2835_GPIO_FSEL_INPT = 0; // input select
        enum RDY = 22; // DAC ready

        this() // constructor
        {
                bcm2835_init();
                bcm2835_spi_begin();
                bcm2835_spi_setClockDivider(64/2); // On Pi3, 32=> 12.5MHz, 2=>200<Hz
                bcm2835_gpio_fsel(RDY, BCM2835_GPIO_FSEL_INPT);
        }

        ~this() // destructor
        {
                bcm2835_spi_end();
                bcm2835_close();
        }

        void send_float(float f) const
        {
                int vol = to!int((f +1.0) * 4095.0 / 2.0);
                while(bcm2835_gpio_lev(RDY)==0) {};  // wait until pin goes high
                // NB 16-bit data is transmitted in MSB first
                ubyte[2] tx, rx;
                tx[1] = vol & 0xFF;
                tx[0] = (vol>>8) & 0xFF;
                bcm2835_spi_transfernb(cast(ubyte*)tx, cast(ubyte*) rx, 2);
        }
}

