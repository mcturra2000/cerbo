/* loop through the ADCs printing their values in a round-robin fashion
 * User ADC inputs are on 0-3 (GPIO 26-29), the temperature sensor is on ADC 4
 */

#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
//#include "hardware/adc.h"
#include <math.h>

#include "debounce.h"
#include "rotary.h"


int main() 
{
	stdio_init_all();

	int count = 57;
	puts("440");

	debounce_t deb;
	debounce_init(&deb, 19, 4);

	rotary_t rot1;
	rotary_init(&rot1, 21, 20);

	int prev = -1;
	while(1) {
		int chg;
		if(chg = rotary_change(&rot1)) {
			count += chg;
			if(count<0) count = 0;
			if(count>107) count = 107;
		}

		if(count != prev) {
			prev = count;
			float freq = 440.0 * pow(2, ((float)count - 57.0)/12.0);
			printf("%d\n",  (int)freq);
		}

	}

	return 0;
}

