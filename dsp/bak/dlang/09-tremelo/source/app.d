//import inid;
import onyx.bundle;
import waved;
import std.stdio;
import std.math;
import std.random;
import std.conv;


void main()
{
	writeln("tremelo example");
	auto bundle = new immutable Bundle("app.conf");

	auto dir = bundle.value("config", "dir");
	auto fname = bundle.value("config", "file");
	auto depth = to!float(bundle.value("config", "depth"));	
	auto f_lfo = to!float(bundle.value("config", "lfo_freq"));


	auto full_name = dir ~ "/" ~ fname;
	//fname ~= "dubstep.wav";
	//fname ~= "brown.wav";
	//fname ~= "handel.wav";
	Sound wav_in = decodeWAV(full_name);
	wav_in = wav_in.makeMono();
	auto fs = wav_in.sampleRate;
	//assert(fs == 44100);
	float[] samps1 = wav_in.channel(0);

	auto dt = 1.0/fs;
	//auto depth = 0.3;
	auto nsamps = samps1.length;
	//auto f_lfo = 20; // frequency of LFO
	auto pi2 = 6.283185307179586;
	float g_t = 0.0;
	foreach(i; 0..nsamps) {
		g_t += pi2 * f_lfo / fs;
		if(g_t > pi2) g_t -= pi2;
		auto gn = sin( g_t);
		samps1[i] = samps1[i] * ( (1-depth) + depth * gn);
	}

	//Sound(fs, wav1).encodeWAV("out.wav");
	Sound snd = Sound(cast(int)fs, 1, samps1);
	encodeWAV(snd, "out.wav");


}
