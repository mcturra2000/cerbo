import scone;
import std.stdio;



void main() {
	window.title("example");
	window.resize(33, 20);

	bool run = true;
	while(run) {
		foreach(input; window.getInputs()) {
			// if CTRL+C is pressed
			if(input.key == SK.c && input.hasControlKey(SCK.ctrl)) {
				run = false;
			}
		}

		window.clear();
		window.write(
				12, 9,
				Color.yellow.foreground, "Hello ",
				Color.red.foreground, Color.white.background, "World"
			    );
		window.print();
	}
}
