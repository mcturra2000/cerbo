import std.stdio;
import serialport;

void main()
{
	writeln("Serial reader");
	//auto com1 = new SerialPortNonBlk("/dev/ttyACM0", 115200);
	auto com1 = new SerialPortBlk("/dev/ttyACM0", 115200);
	void[1] serial_rx = void;
	while(1) {
		try {
			auto res2 = com1.read(serial_rx);
			if(res2.length > 0) {
				auto str = cast(string)res2;
				write(str);
				auto c = cast(char)str[0];
				if(c == '\n') writeln("\t\\n 10 LF\n");
				//if(c == '\r') writeln("\t\\r 13 CR\n");
			}
		} catch (TimeoutException e) { }
	}
}
