import onyx.bundle; // dub add onyx-config
import waved; // dub add wave-d
import std.stdio;
import bcm2835 : Synth;
//import asdf;
import std.file;
import std.algorithm;
import std.array;
import std.format;


class Drum {
	float[] samples;
	ulong index;
	ulong length;

	this(float[] samps) {
		samples = samps;
		index = length; // begin in a halted state
		length = samples.length;
	}

	void reset() {
		index = 0;
	}

	void inc() {
		if(index <= length) index++;
	}

	float vol() {
		if(index<length)
			return samples[index];
		else
			return 0;
	}
}

void main()
{
	writeln("drum machine");
	//float[][string] drums;
	Drum[string] drums;
	char[][string] scores;


	// read configuration
	auto cfg = new immutable Bundle("app.conf");

	// load drum wavs
	auto all_drum_names = cfg.values("config", "drums");
	foreach(d; all_drum_names) {
		string file = cfg.value("config", "dir") ~ "/" ~ cfg.value("config", d);
		Sound wav = decodeWAV(file);
		wav = wav.makeMono();
		float[] samps = wav.channel(0);
		//writeln(d, ":", samps.length, ",", samps.length <= spq);
		drums[d] = new Drum(samps);
	}

	// load seq
	string seq_name = cfg.value("config", "seq");
	write(seq_name);
	try {
		auto desc = to!(string[])(cfg.values(seq_name, "desc"));
		auto desc1 = join(desc, " ");
		write(format(":%s", desc1));
	} catch(KeyNotFoundException ex) {}
	writeln("");

	int score_len = -1;
	string[] drum_names;
	foreach(d; all_drum_names) {
		string[] score;
		try {
			score = to!(string[])(cfg.values(seq_name, d));
		} catch(KeyNotFoundException ex) {
                        continue;
                }
                drum_names ~= d;

		auto score1 = join(score);
		auto score2 = to!(char[])(score1);
		scores[d] = score2;
		if(score_len == -1) {
			score_len = to!int(score2.length);
		} else {
			assert(score_len == score_len);
		}
	}

	int spq = 44100 / 4 ; // samples per quaver, assuming 120bpm
	try { 
		if(to!bool(cfg.value(seq_name, "semi")))
			spq = spq/2;
	} catch (KeyNotFoundException ex) {}

	//writeln(drums["snare"].length);

	auto synth = new Synth;

	uint tick = 0;
	uint prev_score_idx = -1;
	while(true) {
		//auto sample_idx = tick % spq; // sample index 
		uint score_idx = (tick / spq) % score_len; 

		float vol = 0.0;
		foreach(d; drum_names) {
			if(scores[d][score_idx] == '!' && prev_score_idx != score_idx) 
				drums[d].reset();
			else
				drums[d].inc();
				
			//&& drums[d].length > sample_idx) 
			vol += drums[d].vol();						
		}
		prev_score_idx = score_idx;
		vol /= drum_names.length;
		synth.send_float(vol);

		tick++;

	}

}
