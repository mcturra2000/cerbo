import waved;
import std.array;
import std.ascii;
import std.stdio;
//import std.container.dlist;
import std.math;
import std.random;
import std.conv;
//import pegged.grammar;

extern (C) int getchar();

const float bpm = 120; // beats per minute
immutable float cr = 60.0/bpm; // duration of crotchet in secs
immutable float qua = cr/2; // duration of quaver
immutable float minim = cr*2; // duration of minim

const float fs = 44100; // sampling frequency


shared immutable float[string] freqs;

static this()
{
	freqs = [ "g4" : 392.00,
	      "c5" : 523.25,
	      "d5" : 587.33,
	      "e5" : 659.25,
	      "f5" : 698.46,
	      "g5" : 783.99,
	      "a5" : 880.00
	];
}

immutable auto g4 = 392.00;
immutable auto c5 = 523.25;
immutable auto d5 = 587.33;
immutable auto e5 = 659.25;
immutable auto f5 = 698.46;
immutable auto g5 = 783.99;
immutable auto a5 = 880.00;

/*
   mixin(grammar(`
Score:
Notes <- "g5"
`));
 */

shared int ch;
int yyget()
{
	ch =  getchar();
	return ch;

}

float get_dur()
{
	if(ch == '8') return qua;
	if(ch == '2') return minim;
	writeln("Error:get_dur:didn't understand:", ch);
	//throw Exception("get_dur error");
	return 0;
}

struct N{ float freq; float dur;}; // a note

N[] parse()
{
	//int ch;
	N[] notes; // = new [];
top:
	yyget();
	while(isWhite(ch)) yyget();
	if(ch == EOF) return notes;
	float dur = cr;
	if(isDigit(ch)) {dur = get_dur(); yyget(); }
	//string note = cast(char)ch;
	auto note = to!char(ch);
	writeln("note is ", note);
	auto octave = to!char(yyget()); // octave
	//string freq = cast(string)(note) ~ cast(char)(ch);
	string freq = [note, octave];
	writeln("freq is:", freqs[freq]);
	notes ~= N(freqs[freq], dur);
	goto top;
}


void apodise(float[] arr, float ms)
{
	float over = ms * 44100/1000; // apodise over  ms
	ulong len = arr.length;
	for(int i = 0; i < over; i++) {
		arr[i] *= (cast(float)(i)/ over);
		arr[len - i -1] *= (cast(float)(i)/ over);
	}
}

void main()
{

	int which = 1;
	string fname;
	float base_freq;
	switch(which) {
		case 1:
			fname ~= "guitar-2-A2.wav";
			base_freq = 110.0;
			break;
		case 2:
			fname ~= "husky-1.wav";
			base_freq = 350;
			break;
		default:
			throw new Exception("unknown 'which'");
			//writeln("I'm confused");
	}


	string dir = "/home/pi/Music/2022/";
	Sound wav = decodeWAV(dir ~ fname);
	wav = wav.makeMono();
	float[] gsamps = wav.channel(0);




	float[] samples;
	auto notes = parse();

	foreach(note; notes) {
		int n = cast(int)(note.dur * fs); 
		auto arr = new float[n];
		for(int n1 = 0; n1< n; n1++) {
			auto idx = cast(int)(note.freq * float(n1)/2.0/base_freq);
			if(idx < gsamps.length)
				arr[n1] = gsamps[idx];
			else
				arr[n1] = 0;
		}
		apodise(arr, 4);
		samples ~= arr;
	}


	// low-pass filter to eliminate clicks - doesn't help
	if(0) {
		float vout = 0;
		float fc = 10000.0; // cutoff frequency
		float alpha = 2.0 * 3.141593 * fc/ fs;
		float c2 = 1.0 / (1.0 + alpha);
		float c1 = alpha * c2;
		foreach(i; 0..samples.length) {
			vout = c1 * samples[i] + c2 * vout;
			samples[i] = vout;
		}
	}

	Sound snd= Sound(cast(int)fs, 1, samples);
	encodeWAV(snd, "out.wav");
	writeln("Done");
}
