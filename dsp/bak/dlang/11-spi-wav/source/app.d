//import inid;
//import onyx.bundle;
import waved;
import std.stdio;
import std.math;
//import std.random;
//import std.conv;
import bcm2835 : Synth;
import core.runtime;


void main()
{
	writeln("play wav over l432");

	//writeln(Runtime.args[1]);
	auto full_name = Runtime.args[1];
	Sound wav_in = decodeWAV(full_name);
	wav_in = wav_in.makeMono();
	auto fs = wav_in.sampleRate;
	//assert(fs == 44100);
	float[] samps = wav_in.channel(0);
	auto nsamps = samps.length;

	writeln("Loaded. Playing...");

	auto synth = new Synth;
	foreach(i; 0..nsamps) {
		synth.send_float(samps[i]);
	}


}
