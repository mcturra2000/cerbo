import waved;
//import std.array;
import std.stdio;
//import std.container.dlist;
import std.math;
import std.random;

const float bpm = 120; // beats per minute
immutable float cr = 60.0/bpm; // duration of crotchet in secs
immutable float qua = cr/2; // duration of quaver
immutable float minim = cr*2; // duration of minim

const float fs = 44100; // sampling frequency


immutable auto g4 = 392.00;
immutable auto c5 = 523.25;
immutable auto d5 = 587.33;
immutable auto e5 = 659.25;
immutable auto f5 = 698.46;
immutable auto g5 = 783.99;
immutable auto a5 = 880.00;


struct N{ float freq; float dur;}; // a note
auto notes = [
	N(c5, cr),
	N(d5, cr),
	N(e5, cr),
	N(c5, cr),

	N(c5, cr),
	N(d5, cr),
	N(e5, cr),
	N(c5, cr),

	N(e5, cr),
	N(f5, cr),
	N(g5, minim),

	N(e5, cr),
	N(f5, cr),
	N(g5, minim),

	N(g5, qua),
	N(a5, qua),
	N(g5, qua),
	N(f5, qua),
	N(e5, cr),
	N(c5, cr),

	N(g5, qua),
	N(a5, qua),
	N(g5, qua),
	N(f5, qua),
	N(e5, cr),
	N(c5, cr),

	N(c5, cr),
	N(g4, cr),
	N(c5, minim),

	N(c5, cr),
	N(g4, cr),
	N(c5, minim)
	
];

float[] fill_note(float freq, int n)
{
	float[] arr;
	arr.length = n;

	// with ring oscillation
	const float pi = 3.1415926535897;
	const float pi2 = 6.283185307179586;
	float sep = 3.0; // Hz // freq separating the real note. Set to 0 for no ring oscillation
	//sep = 0;
	float w0 = pi2 * (freq);
	float w1 = pi2 * (freq - sep);
	float w2 = pi2 * (freq + sep);
	//float t = 0;
	for(int i=0; i< n; i++) {
		float i1 = cast(float) i;
		float t = i1 / fs;
		float amp = sin(w1 * t)/2f + sin(freq *t)  + sin(w2 * t)/2f;

		// or as a ring oscillator
		amp = sin(w1 * t) + sin(w2 * t*t );

		// wave shaping
		amp = amp / 2.0;
		//amp = sin(w0*t);
		//
		//amp = sqrt(abs(amp)); //  * amp;
		amp = amp * amp;
		//if(amp < 0.0) amp = - amp;

		//amp = sin(pi2* freq * t) + 0f*sin(3.0f*pi2* freq* t)/1.5f;

		// add some white noise - actually pretty rubbish
		//auto u1 = (uniform01() -1f) /2f *0.15;
		//amp += u1;

		// turn it into a square wave
		//if(amp>=0) {amp = 1f;}  else {amp = -1f;}

		// exponential decay - about best so far
		amp = sin(w0 * t) * exp(1.0-20.0 * t); // best as at 2022-02-11
		//amp = (5f*sin(w0 * t) +  1f*sin((1.5*w0)*t) + 1f*sin((w0/1.5)*t))/7.0f * exp(1.0-8.0 * t);


		// FM on db07.31
		float wm = pi2 * 5f;
		float B = 40.0;
		//B= 0f;
		amp = sin( w1*t  + B/wm * (sin(wm*t-pi/2.0) +1.0f) );

		arr[i] = amp * 0.3;
	}

	// apodise
	int n1 = cast(int)(0.015 * fs);
	//n1 = n / 50;
	//goto skip;
	for(int i = 0; i < n1; i++) {
		float scale = cast(float)i / cast(float)n1;
		arr[i] *= scale; // required to not sound aweful
		arr[n-i-1] *= scale; // likewise
	}
skip:

	return arr;
}

void main()
{
	float[] samples;

	foreach(note; notes) {
		int n = cast(int)(note.dur * fs); 
		samples ~= fill_note(note.freq, n);
	}

	//samples ~= fill_note(20, 20);

	Sound snd= Sound(cast(int)fs, 1, samples);
	encodeWAV(snd, "out.wav");
	writeln("Done");
}
