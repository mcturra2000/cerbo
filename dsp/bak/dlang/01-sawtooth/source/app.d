import waved;
import std.stdio;

const float fw = 500; // frequency of wave
const float fs = 44100; // sample frequence
const int nsamples = 60 * cast(int)fs; // 60s

void main()
{
	float[] samples = new float[nsamples];

	const float L = -0.1, H = 0.1; // low and high value volume
	const float dy = fw / fs * (H-L);
	float y0 = L, y1 = L;
	for(int i=0; i< nsamples; i++) {
		//writeln(".");
		samples[i] = y1;
		y1 = y1 + dy;
		//if(y1>=H) { y1 = y1 - (H-L); }
		if(y1>H) { y1 = L; }
	}
	writeln("sample array filled");
	Sound snd= Sound(cast(int)fs, 1, samples);
	encodeWAV(snd, "out.wav");
	writeln("Done");
}
