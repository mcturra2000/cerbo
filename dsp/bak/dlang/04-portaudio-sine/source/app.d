import std.math;
import std.stdio;
import deimos.portaudio;
import serialport;



struct Phase
{
	float left=0, right=0, dx = 0, x = 0;
}

enum pi2 = 6.283185307179586;
bool more = true;

extern(C) int sinewave(const(void)* inputBuffer, void* outputBuffer,
		size_t framesPerBuffer,
		const(PaStreamCallbackTimeInfo)* timeInfo,
		PaStreamCallbackFlags statusFlags,
		void *userData)
{
	auto phase = cast(Phase*)userData;

	auto pout = cast(float*)outputBuffer;

	enum vol = 0.2f;

	foreach(i; 0 .. framesPerBuffer)
	{
		*pout++ = vol * phase.left;
		*pout++ = vol * phase.right;

		phase.left = sin(phase.x);
		phase.right = phase.left;

		phase.x += phase.dx;
		if(phase.x > pi2) phase.x -= pi2;
	}
	return 0;
}


int main()
{
	enum SAMPLE_RATE = 44100;
	enum NUM_SECONDS = 5;

	PaStream* stream;
	PaError err;
	Phase phase_data;
	phase_data.dx = pi2 * 440.0f / SAMPLE_RATE;

	if ((err = Pa_Initialize()) != paNoError) goto Lerror;

	if ((err = Pa_OpenDefaultStream(&stream,
					0,
					2,
					paFloat32,
					SAMPLE_RATE,
					paFramesPerBufferUnspecified,
					&sinewave,
					&phase_data))
			!= paNoError) goto Lerror;

	if ((err = Pa_StartStream(stream)) != paNoError) goto Lerror;

	while(more) Pa_Sleep(10);

	if ((err = Pa_StopStream(stream)) != paNoError) goto Lerror;
	if ((err = Pa_CloseStream(stream)) != paNoError) goto Lerror;
	if ((err = Pa_Terminate()) != paNoError) goto Lerror;

	return 0;
Lerror:
	stderr.writefln("error %s", to!string(Pa_GetErrorText(err)));
	return 1;
}

