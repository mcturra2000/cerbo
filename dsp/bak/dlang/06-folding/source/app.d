import std.math;
import std.stdio;
import deimos.portaudio;
import scone;
import serialport;



struct Phase
{
	float left=0, right=0, dx = 0, x = 0, fold = 1.0;
}

enum pi2 = 6.283185307179586;
bool more = true;
//float fold = 16.0f/16.0f;

extern(C) int sinewave(const(void)* inputBuffer, void* outputBuffer,
		size_t framesPerBuffer,
		const(PaStreamCallbackTimeInfo)* timeInfo,
		PaStreamCallbackFlags statusFlags,
		void *userData)
{
	auto phase = cast(Phase*)userData;

	auto pout = cast(float*)outputBuffer;

	enum vol = 0.2f;

	//fold = 10f/16f;
	foreach(i; 0 .. framesPerBuffer)
	{
		*pout++ = vol * phase.left;
		*pout++ = vol * phase.right;

		float val = sin(phase.x);
		if(val>phase.fold) val = 2.0 * phase.fold - val;
		//if(val>1) val = 2.0f - val;
		//if(val<-1)  val = -val -2.0f;
		if(val<-phase.fold)  val = -val -2.0f*phase.fold;


		phase.left = val;

		phase.right = val;

		phase.x += phase.dx;
		if(phase.x > pi2) phase.x -= pi2;

	}
	return 0;
}


void process_console_events(Phase* phase)
{
	//phase.fold = 12f/16f;
	foreach(input; window.getInputs()) {
		// if CTRL+C is pressed
		if(input.key == SK.c && input.hasControlKey(SCK.ctrl)) {
			more = false;
		}

		const auto fold_val = [
			SK.key_0 : 1f, 
			SK.key_1 : 2f, 
			SK.key_2 : 3f, 
			SK.key_3 : 4f, 
			SK.key_4 : 5f, 
			SK.key_5 : 6f, 
			SK.key_6 : 7f, 
			SK.key_7 : 8f, 
			SK.key_8 : 9f, 
			SK.key_9 : 10f, 
			SK.a : 11f, 
			SK.b : 12f, 
			SK.c : 13f, 
			SK.d : 14f, 
			SK.e : 15f, 
			SK.f : 16.0f ];
		if(input.key in fold_val)
			phase.fold = fold_val[input.key] / 16f;
		//if(input.key == SK.a) phase.fold = 10f/16f;
		window.clear();
		window.write(1, 1, input.key);
		window.print();
		//phase.fold = 12f/16f;
	}
}
int main()
{
	enum SAMPLE_RATE = 44100;
	enum NUM_SECONDS = 5;

	PaStream* stream;
	PaError err;
	Phase phase_data;
	phase_data.dx = pi2 * 440.0f / SAMPLE_RATE;

	if ((err = Pa_Initialize()) != paNoError) goto Lerror;

	if ((err = Pa_OpenDefaultStream(&stream,
					0,
					2,
					paFloat32,
					SAMPLE_RATE,
					paFramesPerBufferUnspecified,
					&sinewave,
					&phase_data))
			!= paNoError) goto Lerror;

	if ((err = Pa_StartStream(stream)) != paNoError) goto Lerror;

	while(more) process_console_events(&phase_data);
	//Pa_Sleep(10);

	if ((err = Pa_StopStream(stream)) != paNoError) goto Lerror;
	if ((err = Pa_CloseStream(stream)) != paNoError) goto Lerror;
	if ((err = Pa_Terminate()) != paNoError) goto Lerror;

	return 0;
Lerror:
	stderr.writefln("error %s", to!string(Pa_GetErrorText(err)));
	return 1;
}

