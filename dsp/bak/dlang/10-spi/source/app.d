import std.stdio;
import core.thread;
import bcm2835;

/*
extern (C) int  bcm2835_init();
extern (C) int  bcm2835_close();
extern (C) int  bcm2835_gpio_lev(ubyte pin);
extern (C) void bcm2835_gpio_set(ubyte pin);
extern (C) void bcm2835_gpio_clr(ubyte pin);
extern (C) void bcm2835_gpio_fsel(ubyte pin, ubyte mode);
extern (C) void	bcm2835_spi_transfernb (ubyte *tbuf, ubyte *rbuf, uint len);
extern (C) int 	bcm2835_spi_begin();
extern (C) void	bcm2835_spi_end();
extern (C) void bcm2835_spi_setClockDivider(uint divider);

enum BCM2835_GPIO_FSEL_INPT = 0; // input select
*/

enum RDY = 22; // DAC ready

void main()
{
	writeln("example");
	bcm2835_init();
	bcm2835_spi_begin();
	bcm2835_spi_setClockDivider(64/2); // On Pi3, 32=> 12.5MHz, 2=>200<Hz
	bcm2835_gpio_fsel(RDY, BCM2835_GPIO_FSEL_INPT);

	ushort count = 0;
	//ubyte rx, tx =0;
	while(1) {

		while(bcm2835_gpio_lev(RDY)==0) {};  // wait until pin goes high
		//bcm2835_spi_gpio_write();
		// NB 16-bit data is transmitted in MSB first
		ubyte[2] tx, rx;
		tx[1] = count & 0xFF;
		tx[0] = (count>>8) & 0xFF;

		bcm2835_spi_transfernb(cast(ubyte*)tx, cast(ubyte*) rx, 2);
		if( 0) {
			int rx16 = (rx[0] << 8) + rx[1];
			printf("%d, %d", count, rx16);
			if(count !=rx16) printf(" X");
			puts("");
		}
		count += 1;
		if(count>4095) count = 0;
		//Thread.sleep( dur!("msecs")( 100 ) );  // sleep for 
		//Thread.sleep( dur!("usecs")( 23 ) );  // sleep for 
	}

	//bcm2835_spi_end();
	//bcm2835_close();
}
