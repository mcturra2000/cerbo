import waved;
import std.math;
import std.stdio;

const float fw = 500; // frequency of wave
const float fs = 44100; // sample frequence
const int nsamples = 60 * cast(int)fs; // 60s

void main()
{
	float[] samples = new float[nsamples];
	float dt = 1 / fs;
	float t0 = 0, t1 = 0;
	const float fw_t0 = 1.0/fw;
	const float fw_t1 = 1.0/(fw + 1.0); // add a little binaural
	for(int i=0; i< nsamples; i++) {
		samples[i] = 0;

		bool hi0= false;
		t0 = fmod(t0, fw_t0);
		if(t0>=0.5*fw_t0) hi0 = true;
		t0 += dt;

		bool hi1 = false;
		t1 = fmod(t1, fw_t1);
		if(t1>=0.5*fw_t1) hi1 = true;
		t1 += dt;
		if(hi0 || hi1) samples[i] = 0.1;
	}
	writeln("sample array filled");
	Sound snd= Sound(cast(int)fs, 1, samples);
	encodeWAV(snd, "out.wav");
	writeln("Done");
}
