# sawtooth wave

```
dub add wave-d
# need to do something else, too
```

## See also

* db07.102
* [blog post](https://mcturra2000.wordpress.com/2022/04/17/square-wave-and-boolean-modulated-output-using-sampling/)


# Status

2022-04-17	Works

2022-04-16	Started.
