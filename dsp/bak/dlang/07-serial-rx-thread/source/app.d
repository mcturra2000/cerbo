import std.stdio;
import serialport;
//import thread.osthread;
import core.thread;

// https://tour.dlang.org/tour/en/multithreading/synchronization-sharing
void safePrint(T...)(T args)
{
    // Just executed by one concurrently
    synchronized {
        import std.stdio : writeln;
        writeln(args);
    }
}

shared string g_string;
shared bool fresh = false;

void print_thread()
{
	while(1) {
		//writeln(g_string);
		if(!fresh) continue;
		fresh = false;
		string str = g_string;
		writeln(str);
	}
}

void serial_thread()
{
	auto com1 = new SerialPortBlk("/dev/ttyACM0", 115200);
	void[1] serial_rx = void;
	string line;
	while(1) {
		try {
			auto res2 = com1.read(serial_rx);
			if(res2.length > 0) {
				auto str = cast(string)res2;
				if(str == "\n") continue;
				if(str == "\r") {
					g_string = "Line is <" ~ line ~ ">";
					fresh = true;
					safePrint("Line is <", line, ">");
					line = "";
				} else {
					line ~= str;
				}
				//write(str);
				//auto c = cast(char)str[0];
				//if(c == '\n') writeln("\t\\n 10 LF\n");
				//if(c == '\r') writeln("\t\\r 13 CR\n");
			}
		} catch (TimeoutException e) { }
	}
}
void main()
{
	writeln("Serial reader");
	auto th_serial = new Thread(&serial_thread).start();
	//auto th_print = new Thread(&print_thread).start();

	g_string = "hello";
	fresh = true;
	//while(1) {};
}
