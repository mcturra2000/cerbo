//#include <fstream>
//#include <sstream>
//import oss;
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/ioctl.h>

#include <linux/soundcard.h>


#include <oss.h>

#define info(x) printf("\t%s\t%lu\n", #x, x)


int main() {

	puts("Header info:");
	info(SNDCTL_DSP_GETBLKSIZE);
	info(SNDCTL_DSP_SETFRAGMENT);
	info(SNDCTL_DSP_GETOSPACE);
	info(SNDCTL_DSP_SPEED);
	info(SNDCTL_DSP_GETFMTS);
	info(SNDCTL_DSP_SETFMT);
	info(AFMT_MU_LAW);
	info(AFMT_A_LAW);
	info(AFMT_U8);
	info(AFMT_S16_LE);
	info(AFMT_S16_BE);
	info(AFMT_S16_NE);
	info(AFMT_U16_LE);



	try {
		oss aoss;
		constexpr auto fs = 8000; // sampling frequency
		aoss.set_sampling_freq(fs);
		aoss.set_fmt(AFMT_S16_LE);
		aoss.sigint_throws();

//#define MUSIC "/data/data/Music/"
#define MUSIC "/home/pi/Music/"
		char fname1[] = MUSIC "sheep-08k.raw";
		char fname2[] = MUSIC "dubstep-8k.raw";
		char fname3[] = MUSIC "moonlight-08k.raw";
		slab aslab(fname1);

		puts("ready to play");
		for(int i=0; i < aslab.size; i++) { 
			aoss.push(aslab.data[i]* 100);
		}

	} catch (int e) {
		puts("Exception caught");
	}

	return 0;
}
