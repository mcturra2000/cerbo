#include <assert.h>
#include <cmath>
#include <iostream>
#include <stdio.h>
#include <cstdint>
#include <string>

#include <oss.h>

#include "wav.h"

using namespace std;


FILE* main_wav (const string& infile, wave_fmt_t& wave_fmt, hdr_t& hdr)
{
	//assert(argc == 2);
	//string infile{argv[1]};
	FILE* fin = fopen(infile.c_str(), "r");
	assert(fin);

	//hdr_t hdr;
	read_hdr(fin, &hdr);
	assert(hdr.id == riff);

	read_hdr(fin, &hdr);
	assert(hdr.id == wave);
	//wave_fmt_t wave_fmt;
	dump_wave_fmt(fin, wave_fmt);
	//cout << "sample rate:" << wave_fmt.sample_rate << endl;
	//ahead(fin, -4); // size isn't here, so roll back 4 bytes

	while(1) {
		read_hdr(fin, &hdr);
		cout << "Found chunk "<< unchid(hdr.id) << "...";
		if(hdr.id == wav_data) break;
		ahead(fin, hdr.size);
		cout << "ignoring\n";
	}

	return fin;

#if 0
	//cout << "3\n";
	cout << "extracting as 10-bit in 2 bytes\n";

	size_t lastindex = infile.find_last_of(".");
	string outname = infile.substr(0, lastindex);
	outname += ".raw";
	cout << "Output name: " << outname << "\n";

	FILE* fout = fopen(outname.c_str(), "w");
	assert(fout);
	cout << "Bytes being processed: " << hdr.size << "\n";
	while(hdr.size > 0) {
		int16_t val1; // input file uses signed
		fread(&val1, 2, 1, fin);

		// convert signed to unsigned
		uint16_t val2 = 0x8000 +val1; // convert it to a positive int
		if(val1<0) val2 += 0x10000;
		val2 >>= 5; // convert from 15 bits to 10

		//val1 >>= 1; // convert it to 10 bits
		cout <<  val1 << "     " << val2 << "\n";
		fwrite(&val2, 2, 1, fout);
		/*
		int32_t val2 = val1; // expand it
		if(val2 >0
		val2 += 0xFF; // 
		*/
		hdr.size -= 2;
	}

	cout << "wav10 finished\n";
	fclose(fout);
	fclose(fin);
	return 0;
#endif


#if 0
	int option;
	char *append_filename = nullptr;
	while((option = getopt(argc, argv, "a:h")) != -1) {
		switch(option) {
			case 'a': append_filename = optarg; break;
			case 'h': print_help();
			case '?': printf("unknown option: %c\n", optopt); exit(1);
		}
	}

	/*
	   for(; optind < argc; optind++){ //when some extra arguments are passed
	   printf("Given extra arguments: %s\n", argv[optind]);
	   }
	   */

	cout << "optind=" << optind << ", argc = " << argc << "\n";
	if(argc <= optind) {
		puts("Filename unspecified. Aborting");
		exit(1);
	}

	char *filename = argv[optind];

	if(append_filename) {
		append_file(filename, append_filename);
	} else {
		dump_file(filename);
	}

	fclose(fp);

#endif
}


void main1()
{
	hdr_t data_hdr;
	wave_fmt_t wave_fmt;
	string fname{"/home/pi/Music/dubstep.wav"};
	fname = "/home/pi/Music/handel.wav";
	FILE* fin = main_wav(fname, wave_fmt, data_hdr);

	cout << "data_hdr size:" << data_hdr.size << endl;
	int nchans = wave_fmt.num_channels;
	int dsz = data_hdr.size; // size of data
	int nspc = dsz / nchans / sizeof(int16_t); // number of samples per channels 
	//constexpr auto fs = 22000; // sampling frequency
	//constexpr auto fc = 200; // cutoff frequency
	//constexpr double K = 2.0 * 3.14159 * fc / fs;

	//int16_t data[nchans][nspc];
	int16_t *data = (int16_t*) malloc(dsz); // these sizes aren't particularly right, and it isn't freed
	assert(data);
	fread(data, nspc*nchans, sizeof(int16_t), fin);
	oss aoss(wave_fmt.sample_rate, AFMT_S16_LE, true);
	puts("Press Ctl-C to exit");
	for(int i = 0; i<nspc; i++) {
		aoss.push(data[i*2]);
	}

	fclose(fin);
}

int main() {
	try {
		main1();
	} catch (int e) {
		cout << "Exception caught:" << e << endl;
	}
	return 0;
}
