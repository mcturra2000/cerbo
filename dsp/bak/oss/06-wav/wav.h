#pragma once

#include <string>

typedef struct {
	uint32_t len_fmt; // length of the format data (s/b 16)
	uint16_t type;
	uint16_t num_channels;
	uint32_t sample_rate; // samples per second
	uint32_t byte_rate; // SampleRate * BitsPerSample * Channels / 8
	uint16_t block_align; // BitsPerSample * Channels / 8 ; 1= 8bit mono, 2 = 8bit stereo or 16bit mono; 4 = 16bit stereo
	uint16_t bits_per_sample;
} wave_fmt_t;

typedef struct __attribute__((__packed__)) {
	uint32_t id; // RIFF, WAVE, etc.
	uint32_t size;
	uint32_t begin; // file position where the chunk data begins
	uint32_t end; // file position where the chunk data ends
} hdr_t;

constexpr uint32_t chid(char c1, char c2, char c3, char c4) { return (c4 <<24) | (c3 << 16) | (c2 << 8) | c1; }
constexpr auto riff = chid('R', 'I', 'F', 'F');
constexpr auto wave = chid('W', 'A', 'V', 'E');
constexpr auto fmt_ = chid('f', 'm', 't', ' ');
constexpr auto wav_data = chid('d', 'a', 't', 'a');
constexpr auto list = chid('L', 'I', 'S', 'T');
constexpr auto id3_ = chid('i', 'd', '3', ' ');

std::string unchid(uint32_t id);
void ahead(FILE* fp, int offset);
void read_hdr(FILE* fp, hdr_t* hdr);
void dump_wave_fmt(FILE* fp, wave_fmt_t& wave_fmt);
