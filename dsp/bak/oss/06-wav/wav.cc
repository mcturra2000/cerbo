#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
//#include <fstream>
#include <unistd.h>

#include "wav.h"

using namespace std;

//FILE *fp;

//constexpr uint32_t chid(const char* id) { return *(uint32_t*)id; }

//#define CHID(x) *(uint32_t*) x




std::string unchid(uint32_t id)
{
	string res = "";
	for(int i = 0; i<4; i++) {
		res += (char) (id & 0xFF);
		id >>= 8;
	}
	return res;
}



bool is_even(int n) { return (n % 2 == 0); }
bool is_odd(int n) { return ! is_even(n); }

/* align file to an even offset */
bool align (FILE* fp)
{
	if(is_even(ftell(fp))) return false;
	cout << "aligning fileptr\n";
	fseek(fp, 1, SEEK_CUR);
	return true;
}

void ahead (FILE* fp, int offset)
{ 
	fseek(fp, offset, SEEK_CUR); 
	align(fp);
}

size_t file_pos = 0;

//uint32_t file_size;

void read_hdr (FILE* fp, hdr_t* hdr)
{
	uint32_t n = fread(hdr, 1, 8, fp);
	assert(n==8);
	hdr->begin = ftell(fp);
	hdr->end = hdr->begin + hdr->size;
	//file_pos += 8;
}

#if 0
void dump_list (hdr_t* hdr)
{
	cout << "LIST chunk...\n" ;
	char list_id[4];
	fread(list_id, 4, 1, fp);
	if(strncmp(list_id, "INFO", 4)) {
		cout << "Not an INFO, So skipping\n";
		fseek(fp, hdr->size -4, SEEK_CUR);
		return;
	}
	assert(is_even(hdr->size));
	size_t remaining = hdr->size-4;
	while(remaining > 0) {
		//cout << "Remaining: " << remaining << "\n";
		read_hdr(hdr);
		cout <<  unchid(hdr->id) << ": " ;
		for(int i =0; i<hdr->size; i++) {
			char c;
			fread(&c, 1, 1, fp);
			if(c !=0) putchar(c);
		}
		cout << "\n";
		remaining = remaining - hdr->size - 8;
		if(align(fp)) remaining--;
	}
	cout << "\n";
	return;
}
#endif


void dump_wave_fmt (FILE* fp, wave_fmt_t& wave_fmt)
{

	cout << "FMT chunk...\n";
	//wave_fmt_t fmt;
	//fseek(fp, -4, SEEK_CUR);
	fread(&wave_fmt, 1, sizeof(wave_fmt_t), fp);
	//uint32_t chunk_size += sizeof(struct fmt_info);
	cout << "len = " << wave_fmt.len_fmt << "\n";
	assert(wave_fmt.len_fmt == 16);
	cout << "type: " << wave_fmt.type << "\n";
	cout << "#channs: " << wave_fmt.num_channels << "\n";
	cout << "sample rate: " << wave_fmt.sample_rate << "\n\n";
}

#if 0
void dump_wave (hdr_t* hdr, FILE* fp, bool* has_id3)
{
	cout << "\nWAVE chunk...\n";

	*has_id3 = true;

	// wind back the file a little, because "size" is actually a chunk name
	fseek(fp, -4, SEEK_CUR);

	while(1) {
		hdr_t hdr1;
		read_hdr(&hdr1);
		switch(hdr1.id) {
			case list:
				dump_list(&hdr1);
				break;
			case fmt_:
				dump_wave_fmt(fp);
				//ahead(fp, hdr1.size);
				break;
			case data:
				cout << "DATA chunk size " << hdr1.size << "\n";
				ahead(fp, hdr1.size);
				break;
			case id3_:
				cout << "id3 chunk found. Assume end of file\n";
				*has_id3 = true;
				return;
			default:
				cout << "Unknown wave type;" << unchid(hdr1.id) << ". Aborting.\n";
				exit(1);
		}
	}
				
}

void dump_file(const char* filename)
{
	cout << "RIFF process\n";
	fp = fopen(filename, "r");
	assert(fp);
	cout << "intial ftell = " << ftell(fp) << "\n";
	hdr_t hdr;
	read_hdr(&hdr);
	assert(hdr.id == riff);
	cout << "header ends " << hdr.end << "\n";
	//uint32_t end = hdr.size+8;

	bool has_id3 = false;
	while(ftell(fp) < hdr.end) {
		hdr_t hdr1;
		read_hdr(&hdr1);
		//cout << "chunk size = " << hdr1.size << "\n";
		//cout << "chunk ends = " << hdr1.end << "\n";
		//uint32_t end = hdr.size;
		switch(hdr1.id) {
			case wave: 
				dump_wave(&hdr1, fp, &has_id3);
				if(has_id3) { 
					cout << "id3 encountered. Assumed end of file\n";
					goto finis;
				}
				//ahead(fp, hdr1.size);
				//cout << "ftell now at " << ftell(fp) << "\n";
				break;
			default:
				cout << "RIFF doesn't understand " << unchid(hdr1.id) << ", skipping\n";
				ahead(fp, hdr1.size);
		}
	}
finis:
	cout << "Bye\n";
}

void print_help(void)
{
	const char* text = R"EOI(
riff - edit RIFF files
a [FILE] append file to end 
h	print this help
)EOI";
	puts(text);
	exit(0);
}

void append_file (const char* filename, const char* append_filename)
{
	fp = fopen(filename, "r+"); // for reading and writing
	assert(fp);
	hdr_t hdr;
	read_hdr(&hdr);
	assert(hdr.id == riff);
	uint32_t riff_data_size = hdr.size;
	
	FILE* fapp = fopen(append_filename, "r");
	assert(fapp);
	fread(&hdr, 8, 1, fapp);
	assert(hdr.id == riff);

	// check that we have a WAVE chunk
        fread(&hdr, 8, 1, fapp);
	assert(hdr.id == wave);

	fseek(fapp, -8, SEEK_CUR); // now wind back to start of WAVE chunk

	//append WAVE in fapp to fp
	fseek(fp, 0, SEEK_END);
	char block[512];
	size_t n, cum = 0;
	while(1) {
		n = fread(block, 1, sizeof(block), fapp);
		if(n<=0) break;
		fwrite(block, 1, n,  fp);
		cum += n;
	}

	// update the length of RIFF
	riff_data_size += cum;
	fseek(fp, 4, SEEK_SET); // size of file is at offset 
	fwrite(&riff_data_size, 4, 1, fp);	

	fclose(fapp);
}
#endif



