# 2023-09-11	snapshot.
#		monitor reset init is possibly wrong. See blink sketch in HAL
#		for a likely fix to this
# 2023-07-19	snapshot
# 2023-07-01 	snapshot

# quit without confirmation
define hook-quit
    set confirm off 
end

set pagination off
file app
#load
#b main
#b main.c:32
#monitor reset init
echo RUNNING...\n
r
#c

