/*
 * 2023-03-31	Started
 */
#include <ctype.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/poll.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdexcept>
#include <termios.h>


#include <oss.h>

//import oss;

void delay_ms(int n) { usleep(n*1000); }


static struct termios term_ori; // original term that we want to restore when we've finished

void term_init(void)
{
	// https://linux.die.net/man/3/termios
	struct termios term;
	if(tcgetattr(STDIN_FILENO, &term) < 0) exit(1); // what are the current attributes?
	term_ori = term; // creat a copy for later restoration
	cfmakeraw(&term); // raw mode; char-by-char, echo disabled, special chars disabled
	// cfmakeraw() does too much, so we have to set things back again
	term.c_iflag |= ICRNL; // translate CR to NL on input
	term.c_oflag |= OPOST; // get newlines working properly again on output
	term.c_lflag |= ISIG; // re-enable INTR, QUIT, SUSP, DSUPS. Ctl-C will work again.
	if(tcsetattr(STDIN_FILENO, TCSANOW, &term)) exit(1); // set the attributes how we wish
}

void term_deinit(void)
{
	if(tcsetattr(STDIN_FILENO, TCSANOW, &term_ori)) exit(1);
}

/*
 * NB stdout will be supressed unless there is a \n for an fflush(stdout)
 */
char inbuf[8];
int get_key_non_blocking(void)
{
	int fd;
	//char c;
	const nfds_t nfds = 1;
	struct pollfd pfds[nfds];
	int timeout = 1; // ms
	const int stdin_fd = STDIN_FILENO;
	pfds[0].fd = stdin_fd; // stdin
	pfds[0].events = POLLIN | POLLHUP;
	//while(1) {
		int n = poll(pfds, nfds, timeout);
		if(n==0) return 0;
		if(pfds[0].revents & POLLHUP) {
			return 0;
		}
		if(pfds[0].revents & POLLIN) {
			int i = read(stdin_fd, inbuf, sizeof(inbuf));
			return i;
			//putchar(toupper(c));
			//printf(" %d, ", c);
			//fflush(stdout);
		}
	//}
		return 0;
}

void goto_yx(int y, int x)
{
	printf("\033[%d;%df", y, x);
}

void at_yx(int y, int x, char c)
{
	goto_yx(y,x);
	putchar(c);
	fflush(stdout);
}

// clear screen
void clr()
{
	printf("\033[2J");
}

bool beats[16];
slab snare("/home/pi/Music/snare-8k.raw");
//oss oss1(8000, AFMT_U8, true);
//oss oss1(8000, AFMT_U16_LE, true);
oss oss1;

int main()
{
	oss1.set_fmt(AFMT_S16_LE);
	term_init();
	int beat_num = 0;
	printf("\033[?25l"); // cursor off
	clr();
	goto_yx(1, 0);
	puts("0123456789abcdef");
	while(1) {
		at_yx(2, 1+beat_num, '^');

		if(beats[beat_num]) {
			for(int i = 0; i < snare.size; i++) {
				while(oss1.full()) usleep(200);
				oss1.push(snare.data[i]*1000);
			}
		}

		delay_ms(250);
		at_yx(2, 1+beat_num, ' ');
		beat_num++;
		beat_num = beat_num % 16;

		int n = get_key_non_blocking();
		if(n == 0) continue;
		int c = inbuf[0];
		int beat = -1;
		if('0' <= c && c <= '9') {
			beat = c -'0';
			//at_yx(3, 1+c-'0', '!');
		}
		if('a' <= c && c <= 'f') {
			beat = c - 'a' + 10;
			//at_yx(3, 1+c-'a' + 10, '!');
		}
		if(beat > -1) {
			beats[beat] = ! beats[beat];
			at_yx(3, beat +1, beats[beat] ? '!' : ' ');
		}


		//for(int i = 0 ; i < n; i++) 
		//	printf("%d ", inbuf[i]);
		//puts("");
		if(inbuf[0]== 'x') break;


		//printf("%d ", c);
		//fflush(stdout);
	}
	term_deinit();
	printf("\033[?25h"); // cursor on
	return 0;
}
