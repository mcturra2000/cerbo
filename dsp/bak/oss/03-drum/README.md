# oss play snare drum when you hit a key

Uses termio, too.

* app1 - plays a drum, and messes around with termios to skip non-key presses
* app2 - like app1, but blocks for key presses. Use it in conjunction with controller.py
* controller.py - controls app2


## Status

2022-10-28	Works.

2022-10-21	Started.
