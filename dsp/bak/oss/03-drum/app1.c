#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
//#include <math.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>


#include <linux/soundcard.h>

#define DEV "/dev/dsp"
#define LEN 1024
uint8_t buf[LEN];

int fd = -1;
FILE* fp = 0;


struct termios termin_ori, termout_ori;

void cleanup()
{
	if(tcsetattr(STDIN_FILENO, TCSANOW, &termin_ori)) exit(1);
	//if(tcsetattr(STDOUT_FILENO, TCSANOW, &termout_ori)) exit(1); // doesn't seem to work
	//system("reset");
	if(fp) fclose(fp);
	if(fd!= -1) close(fd);
}

void irq_handler(int ret) 
{
	puts("irq_handler called");
	cleanup();
	exit(ret);
}

/* typical failure:
 * app: main.c:138: main: Assertion `false' failed.
 * Aborted
 */

void _assert1(bool x, int lineno)
{
	if(x) return;
	printf("assert fail line %d\n", lineno);
	irq_handler(1);
}

#define assert1(x) _assert1(x, __LINE__)

void _ioctlx(unsigned long cmd, const char* cmd_str, void* parg) 
{
	if(ioctl(fd, cmd, parg) == -1) { 
		printf("errno = %d, in cmd:\n", errno); 
		perror(cmd_str);
		cleanup();
		exit(1);
	}
}
#define ioctlx(cmd, arg) _ioctlx(cmd, #cmd, arg)


// possibly use tty mechanism used by termkeys.c

void kbd_init(void)
{
	// https://linux.die.net/man/3/termios
	struct termios term;
	if(tcgetattr(STDIN_FILENO, &term) < 0) exit(1); // waht are the current attributes?
	termin_ori = term;
	if(tcgetattr(STDOUT_FILENO, &termout_ori) < 0) exit(1); // waht are the current attributes?
	cfmakeraw(&term); // raw mode; char-by-char, echo disabled, special chars disabled
	// cfmakeraw() does too much, so we have to set things back again
	term.c_iflag |= ICRNL; // translate CR to NL on input
	term.c_oflag |= OPOST; // get newlines working properly again on output
	term.c_lflag |= ISIG; // re-enable INTR, QUIT, SUSP, DSUPS. Ctl-C will work again.
	if(tcsetattr(STDIN_FILENO, TCSANOW, &term)) exit(1); // set the attributes how we wish
}

int kbd_read(void)
{
	const int conn = STDIN_FILENO;
	const nfds_t nfds = 1;
	struct pollfd pfds[nfds];
	const int timeout_ms = 0; // ms
	pfds[0].fd = conn;
	pfds[0].events = POLLIN | POLLHUP;
	int n = poll(pfds, nfds, timeout_ms);
	if(n==0) return -1;
	//cout << "getkey: revents" << to_string(pfds[0].revents) << endl;
	if(pfds[0].revents == POLLIN) {
		char c;
		int i = read(conn, &c, 1);
		return c;
		//if (1){ printf("%02Xh ", (unsigned char) c); fflush(stdout);}
		//cout << "getkey: key: " << c <<endl;
		//return c;
	}


}

void play(audio_buf_info* pinfo)
{
	size_t n;
	fseek(fp, 0, SEEK_SET);
	while(n = fread(buf, LEN, 1, fp)) {

		// introduce a bit of blocking. 
		// Not necessary, but likely useful for those that want blocking
		while(1) {
			ioctlx(SNDCTL_DSP_GETOSPACE , pinfo);
			if(pinfo->fragments > 0) break; // we could write without blocking
			usleep(200);
		}

		ssize_t n = write(fd, buf, LEN);
	}
}

int main() {
	signal(SIGINT, irq_handler);

	kbd_init();
	int c = kbd_read();

	int flags = O_WRONLY;
	//open(DEV, flags, 0);
	//flags |= O_SYNC; // doesn't seem to help
	//flags |= O_NONBLOCK; // may supress "unwanted side effects" as suggested by man ioctl
	fd = open(DEV, flags, 0);
	assert1(fd != -1);
	/*
	if(fd == -1) {
		perror(DEV);
		//printf("open error: %d\n", errno);
		//printf("EACCES=%d,EBUSY=%d\n", EACCES, EBUSY);
		return 1;
	}
	*/


	int arg;
	ioctlx(SNDCTL_DSP_GETBLKSIZE, &arg); // should be called after selecting sampling paramenters
	printf("SNDCTL_DSP_GETBLKSIZE: %d\n", arg);

	// set up "double-buffering"
	arg = 10;
	ioctlx(SNDCTL_DSP_SETFRAGMENT , &arg);

	audio_buf_info info; // see README.md for meaning
	ioctlx(SNDCTL_DSP_GETOSPACE , &info);
	printf("SNDCTL_DSP_SETFRAGMENT arg = %d, fragments:%d, fragstotal:%d, fragsize:%d, bytes:%d\n", 
			arg, info.fragments, info.fragstotal, info.fragsize, info.bytes);
	assert1((arg == 10) && (info.fragstotal == 2) && (info.fragsize == 1024));
	//assert(false);



	fp = fopen("/home/pi/Music/snare-8k.raw", "rb");
	assert1(fp);

	puts("Press any key to play drum. Ctrl-C to exit");
	while(1) {
		int c = kbd_read();
		if(c>0) play(&info);
		usleep(10 );
	}

	irq_handler(0);
	return 0;
}
