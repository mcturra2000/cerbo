#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
//#include <math.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>


#include <linux/soundcard.h>

#define DEV "/dev/dsp"
#define LEN 1024
uint8_t buf[LEN];

int fd = -1;
FILE* fp = 0;


struct termios termin_ori, termout_ori;

void cleanup()
{
	if(fp) fclose(fp);
	if(fd!= -1) close(fd);
}

void irq_handler(int ret) 
{
	puts("irq_handler called");
	cleanup();
	exit(ret);
}

/* typical failure:
 * app: main.c:138: main: Assertion `false' failed.
 * Aborted
 */

void _assert1(bool x, int lineno)
{
	if(x) return;
	printf("assert fail line %d\n", lineno);
	irq_handler(1);
}

#define assert1(x) _assert1(x, __LINE__)

void _ioctlx(unsigned long cmd, const char* cmd_str, void* parg) 
{
	if(ioctl(fd, cmd, parg) == -1) { 
		printf("errno = %d, in cmd:\n", errno); 
		perror(cmd_str);
		cleanup();
		exit(1);
	}
}
#define ioctlx(cmd, arg) _ioctlx(cmd, #cmd, arg)


void play(audio_buf_info* pinfo)
{
	size_t n;
	fseek(fp, 0, SEEK_SET);
	while(n = fread(buf, LEN, 1, fp)) {

		// introduce a bit of blocking. 
		// Not necessary, but likely useful for those that want blocking
		while(1) {
			ioctlx(SNDCTL_DSP_GETOSPACE , pinfo);
			if(pinfo->fragments > 0) break; // we could write without blocking
			usleep(200);
		}

		ssize_t n = write(fd, buf, LEN);
	}
}

int main() {
	signal(SIGINT, irq_handler);

	int flags = O_WRONLY;
	//open(DEV, flags, 0);
	//flags |= O_SYNC; // doesn't seem to help
	//flags |= O_NONBLOCK; // may supress "unwanted side effects" as suggested by man ioctl
	fd = open(DEV, flags, 0);
	assert1(fd != -1);


	int arg;
	ioctlx(SNDCTL_DSP_GETBLKSIZE, &arg); // should be called after selecting sampling paramenters
	printf("SNDCTL_DSP_GETBLKSIZE: %d\n", arg);

	// set up "double-buffering"
	arg = 10;
	ioctlx(SNDCTL_DSP_SETFRAGMENT , &arg);

	audio_buf_info info; // see README.md for meaning
	ioctlx(SNDCTL_DSP_GETOSPACE , &info);
	printf("SNDCTL_DSP_SETFRAGMENT arg = %d, fragments:%d, fragstotal:%d, fragsize:%d, bytes:%d\n", 
			arg, info.fragments, info.fragstotal, info.fragsize, info.bytes);
	assert1((arg == 10) && (info.fragstotal == 2) && (info.fragsize == 1024));
	//assert(false);



	fp = fopen("/home/pi/Music/snare-8k.raw", "rb");
	assert1(fp);

	while(1) {
		getchar();
		play(&info);
	}

	return 0;
}
