#include <cmath>
#include <stdio.h>
#include <cstdint>

#include <oss.h>

void main1()
{
	constexpr auto fs = 22000; // sampling frequency
	constexpr auto fc = 200; // cutoff frequency
	constexpr double K = 2.0 * 3.14159 * fc / fs;

	oss aoss(fs, AFMT_S16_LE, true);
	puts("Press Ctl-C to exit");

	double vc = 0;
	while(1) {
		double va = (random()% 2) ==1 ? 1.0 : 0.0;
		vc = vc + K *(va - vc);
		//vc = va;
		aoss.push(vc * 32000.0);
	}
}

int main() {
	try {
		main1();
	} catch (int e) {
		puts("Exception caught");
	}
	return 0;
}
