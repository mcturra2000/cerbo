//module;

#include <stdio.h>
//#include <stdexcept>
#include <system_error>
#include <errno.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
//#include <math.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
//#include <termios.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
//#include <string>

#include <oss.h>

#include <sys/soundcard.h>

//export module oss;

//extern constexpr double pi = 3.14159;
//extern constexpr double two_pi = 2.0 * 3.14159;

static void bad (std::string str)
{
	//int err = errno;
	//throw std::system_error(10, str);
	std::cerr << str << std::endl;
	throw 67;
}



slab::slab(const char *fname)
{
	std::ifstream in;
	in.open(fname, std::ifstream::in | std::ifstream::binary);
	std::stringstream sstr;
	sstr << in.rdbuf();
	in.close();
	std::string str1{sstr.str()};
	const char *data1 = str1.c_str(); 
	size = str1.size();
	data = (uint8_t*) malloc(size); // rubbish way of doing this
	if(data ==0) bad("slab could not malloc file");
	memcpy(data, data1, size);
	//return sstr.str();

#if 0
	fp = fopen(fname, "rb");
	if(fp == 0) bad("slab could not open file for reading: " + std::string(fname));

	fseek(fp, 0L, SEEK_END);
	size = ftell(fp);
	//if (bufsize == -1) { /* Error */ }

	data = (uint8_t*) malloc(size);
	if(data ==0) bad("slab could not malloc file");

	if (fseek(fp, 0L, SEEK_SET) != 0) bad("slab could not seek beginning of file");

	size_t size = fread(data, sizeof(char), size, fp);
	if (size == 0) bad("slab error trying fread");

	fclose(fp);
#endif
}


slab::~slab()
{
}




static int sigint_throw_value = 67;

void handle_sigint(int sig)
{
	puts("signal handler called");
	throw sigint_throw_value;
}

void oss::sigint_throws(int v)
{
	sigint_throw_value = v;
	signal(SIGINT, handle_sigint);
}

// It's not really compulsory to call this, but it saves the producer getting too far ahead of itself
bool oss::full()
{
	audio_buf_info abi;
	ioctl(fd, SNDCTL_DSP_GETOSPACE , &abi);
	return abi.fragments == 0;
}

void oss::push(double v)
{
	buf[idx++] = v;
	if(idx==sizeof(buf)/sizeof(buf_t)) {
		write(fd, (void*)buf, sizeof(buf));
		idx = 0;
	}
}


oss::oss()
{
	fd = open("/dev/dsp", O_WRONLY , 0);
	if(fd == -1) bad("Coudln't open /dev/dsp");
	printf("size of buf = %d\n", sizeof(buf));

	// set up double-buffering. Maybe a depraceted function?? But seems necessary.
	int arg = 10;
	ioctl(fd, SNDCTL_DSP_SETFRAGMENT , &arg);

}

oss::oss(int freq, int fmt, bool throws)
{
	// this stuff is the same as oss()
	// although it doesn't work properly as at 2023-03-29. Maybe module bug?
	fd = open("/dev/dsp", O_WRONLY , 0);
	if(fd == -1) bad("Coudln't open /dev/dsp");
	printf("size of buf = %d\n", sizeof(buf));

	// set up double-buffering. Maybe a depraceted function?? But seems necessary.
	int arg = 10;
	ioctl(fd, SNDCTL_DSP_SETFRAGMENT , &arg);

	//oss();
	//usleep(1000);
	set_sampling_freq(freq);
	set_fmt(fmt);
	if(throws) sigint_throws();
}

oss::~oss()
{
	close(fd);
	puts("bye from oss:~oss()");
}

void oss::set_sampling_freq(int freq)
{
	int freq1 = freq;
	ioctl(fd, SNDCTL_DSP_SPEED, &freq1);
	if(freq1 != freq) bad("Couldn't set required sampling frequency");
}


void oss::set_fmt(int fmt)
{
	int fmts = 0;
	ioctl(fd, SNDCTL_DSP_GETFMTS, &fmts);
	if((fmts & fmt) ==0) bad("Couldn't set oss format");
	ioctl(fd, SNDCTL_DSP_SETFMT, &fmt);
}

