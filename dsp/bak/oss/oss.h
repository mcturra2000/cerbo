#pragma once

/* contains paramters for format, such as
 * AFMT_U8 , AFMT_S16_LE
 * See file:
 * less /usr/include/linux/soundcard.h
 */
#include <sys/soundcard.h>

constexpr double pi = 3.14159;
constexpr double two_pi = 2.0 * 3.14159;

class slab {
	public:
		slab(const char *fname);
		~slab();

		//FILE *fp = nullptr;
		long size = 0;
		uint8_t *data = nullptr;
};



class oss {
	public:
		oss();
		oss(int freq, int fmt, bool throws);
		~oss();
		void set_sampling_freq(int freq);
		void set_fmt(int fmt);
		void push(double v);
		bool full();
		void sigint_throws(int val = 67);

		int fd;
		using buf_t = int16_t;
		buf_t buf[512];
		int idx = 0;
};
