#include <cmath>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <cstdlib>
#include <cstdint>

#include <oss.h>

#include <sys/soundcard.h>

//constexpr auto two_pi = 2.0 * pi;


void main1()
{
	constexpr auto fs = 22000; // sampling frequency
#if 1
	oss aoss(fs, AFMT_S16_LE, true);
#else
	oss aoss;
	aoss.set_sampling_freq(fs);
	aoss.set_fmt(AFMT_S16_LE);
	aoss.sigint_throws();
#endif

	puts("Press Ctl-C to exit");

	constexpr auto fw = 440.0;
	constexpr auto dt = two_pi * fw / fs;
	double t = 0;

	// envelope at a simple 1Hz
	constexpr auto fwe = 1.0;
	constexpr auto dte = two_pi * fwe / fs;
	double te = 0; // envelope

	while(1) {
		double v = sin(t);

		// wave folding
		double v1 = fabs(v);
		constexpr double fold = 0.9; // wavefolding
		if(v1 > fold) {
			v1 = fold - (v1 - fold);
			//v1 = fold;
		}
		if(v<0) v1 = - v1;
		v1 = v; // undo the wavefoling


		// envelope
		double ve = sin(te);
		te += dte;
		if(ve<0) {
			v1 = 0;
		} else {
			v1 = v1 * ve;
		}


		aoss.push(v1* 32000);
		t += dt;
		if (t>two_pi) t -= two_pi;
		//while(aoss.full()) usleep(200); // spin our wheel if OSS pipeline full. Appears to be unnecessary
	}
}

int main() {
	try {
		main1();
	} catch (int e) {
		puts("Exception caught");
	}
}
