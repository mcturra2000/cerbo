#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <math.h>

#include <linux/soundcard.h>

#define DEV "/dev/dsp"
#define LEN 1024
uint8_t buf[LEN];

int fd = -1;
FILE* fp = 0;

void cleanup()
{
	if(fp) fclose(fp);
	if(fd!= -1) close(fd);
}

void irq_handler(int dummy) 
{
	puts("irq_handler called");
	cleanup();
	exit(0);
}

void _ioctlx(unsigned long cmd, const char* cmd_str, void* parg) 
{
	if(ioctl(fd, cmd, parg) == -1) { 
		printf("errno = %d, in cmd:\n", errno); 
		perror(cmd_str);
		cleanup();
		exit(1);
	}
}
#define ioctlx(cmd, arg) _ioctlx(cmd, #cmd, arg)


int main() {
	signal(SIGINT, irq_handler);

	int flags = O_WRONLY;
	//open(DEV, flags, 0);
	//flags |= O_SYNC; // doesn't seem to help
	//flags |= O_NONBLOCK; // may supress "unwanted side effects" as suggested by man ioctl
	fd = open(DEV, flags, 0);
	if(fd == -1) {
		perror(DEV);
		//printf("open error: %d\n", errno);
		//printf("EACCES=%d,EBUSY=%d\n", EACCES, EBUSY);
		return 1;
	}


	int arg;
	ioctlx(SNDCTL_DSP_GETBLKSIZE, &arg); // should be called after selecting sampling paramenters
	printf("SNDCTL_DSP_GETBLKSIZE: %d\n", arg);

	// set up "double-buffering"
	arg = 10;
	ioctlx(SNDCTL_DSP_SETFRAGMENT , &arg);

	audio_buf_info info; // see README.md for meaning
	ioctlx(SNDCTL_DSP_GETOSPACE , &info);
	printf("SNDCTL_DSP_SETFRAGMENT arg = %d, fragments:%d, fragstotal:%d, fragsize:%d, bytes:%d\n", 
			arg, info.fragments, info.fragstotal, info.fragsize, info.bytes);
	assert((arg == 10) && (info.fragstotal == 2) && (info.fragsize == 1024));



	fp = fopen("/home/pi/Music/sheep-08k.raw", "rb");
	assert(fp);

	size_t n;
	while(n = fread(buf, LEN, 1, fp)) {

		// introduce a bit of blocking. 
		// Not necessary, but likely useful for those that want blocking
		while(1) {
			ioctlx(SNDCTL_DSP_GETOSPACE , &info);
			if(info.fragments > 0) break; // we could write without blocking
			usleep(200);
		}

		ssize_t n = write(fd, buf, LEN);
	}

	irq_handler(0);
	return 0;
}
