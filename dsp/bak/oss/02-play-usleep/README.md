# alsa playing but with some blocking

Calls `usleep()`, even though it isn't necessary.


```
SNDCTL_DSP_GETOSPACE 

        audio_buf_info info;
        ioctlx(SNDCTL_DSP_GETOSPACE , &info);

        // fragments: number of full fragments that can be read/written without blocking (only reliable when app reads/writes full fragments at a time
        // fragstotal: total num of fragments allocated for buffering
        // fragsize: byte size of fragment. Same as ioctl(SNDCTL_DSP_GETBLKSIZE)
        // bytes: num bytes that can be read/written without blocking
        // Debian returned 32, 32, 682, 21284 (= 32 * 682)

SNDCTL_DSP_SETFRAGMENT
	ioctlx(SNDCTL_DSP_SETFRAGMENT , &arg);

	arg	fragstotal	fragsize	bytes
	4	2		16		32
	8	2		256		512
	9	2		512		1024
	10	2		1024		2048

```


## Status

2022-10-20	Started. Works
