// dump midi

#include <cstdio>
#include <cstring>
#include <cassert>
#include <byteswap.h>
#include <cstdlib>
#include <cstdint>
#include <unistd.h>
#include <ctype.h>



#define PACKED __attribute__((__packed__))

// header chunk
// https://www.recordingblogs.com/wiki/header-chunk-of-a-midi-file
// 14 bytes
typedef struct PACKED {
	char id[4]; // expect MThd
	uint32_t size; // expect 6
	uint16_t fmt; // expect 0, 1, 2
	uint16_t num; // number of tracks
	uint16_t div; // time division
} hdr;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;


u16 read16be()
{
	u16 b;
	read(0, &b, 2);
	return bswap_16(b);
}

u32 read32be()
{
	u32 b;
	read(0, &b, 4);
	return bswap_32(b);
}

u8 readu8()
{
	u8 b;
	read(0, &b, 1);
	return b;
}

void prhex(u8 b)
{
	printf(" 0x%02x ", b);
}

void bye() { } 

void rest(int size)
{
	while(size > 0) {
		unsigned char blk[16];
		int n = read(0, blk, 16);
		size -= n;
		for(int i = 0; i < n; i++) {
			printf("%02x ", blk[i]);
		}
		printf("| ");
		for(int i = 0; i < n; i++) {
			int c = blk[i];
			if(!isprint(c)) c = '.';
			printf("%c", c);
		}
		puts("");


	}
}


void prstr(int len, int &size)
{
	for(int i = 0; i < len; i++) {
		u8 c = readu8(); size--;
		putchar(c);
	}
}


void prhexes(int len, int &size)
{
	for(int i = 0; i < len; i++) {
		u8 c = readu8(); size--;
		prhex(c);
	}
}

bool handle_meta_event(int &size)
{
	u8 type, len;
	printf(" meta type ");
	type = readu8(); size--;
	prhex(type);
	len = readu8(); size--;
	printf(" (len = %d) ", len);
	switch(type) {
		case 0x01:
			printf("text ");
			prstr(len, size);
			break;
		case 0x03:
			printf("Seq/track name: ");
			prstr(len, size);
			break;
		case 0x04:
			printf("Instrument: ");
			prstr(len, size);
			break;
		case 0x20:
			printf(" channel prefix assign ");
			prhexes(len, size);
			break;
		case 0x2F:
			printf(" end of track ");
			break;
		case 0x51:
			printf("tempo ");
			prhexes(len, size);
			break;
		case 0x54:
			printf("smpte offset ");
			prhexes(len, size);
			break;
		case 0x58:
			printf("time signiture ");
			prhexes(len, size);
			break;
		case 0x59:
			printf("key signiture ");
			prhexes(len, size);
			break;
		default:
			printf("unk meta type\n");
			return false;
	}
	puts("");
	return true;
}

int main ()
{
	ssize_t n;
	hdr ahdr;
	n = read(0, &ahdr, sizeof(hdr));
	assert(strncmp(ahdr.id, "MThd", 4) == 0);
	ahdr.size = bswap_32(ahdr.size);
	assert(ahdr.size == 6);
	ahdr.fmt = bswap_16(ahdr.fmt);
	assert(ahdr.fmt == 0); 
	ahdr.num = bswap_16(ahdr.num);
	assert(ahdr.num == 1);
	ahdr.div = bswap_16(ahdr.div);

	// should be getting a track
	char chid[4];
	read(0, chid, 4);
	assert(strncmp(chid, "MTrk", 4) == 0);
	int size; // this should be the number of unread bytes (assuming 1 track)
	size = read32be();
	printf("Track size = %d\n", size);
	//size = bswap_16(size);

	while(size>0) {
		printf("begin: size = %d\n", size);
		u8 delta = readu8(); size--;
		printf("delta = %d, ", delta);
		u8 ev = readu8(); size--;
		printf("ev = 0x%02x", ev);
		//u8 type, len;
		switch(ev) {
			case 0xFF: 
				if(!handle_meta_event(size)) goto finis;
				break;

			case 0xF0 :
				printf(" sys (unhandled)\n");
				goto finis;
			default: //midi event
				printf(" midi ev");
				//prhex(ev);
				u8 note = readu8();
				u8 vel  = readu8();
				printf(" note %d, velocity %d\n", note, vel);
				size -= 2;
				//goto finis;
		}

	}
finis:
	rest(size);



	bye();
}
