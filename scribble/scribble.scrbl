#lang scribble/base
@(require scribble/manual) @; for  codeblocks
 
@title{Introduction to DrRacket Scribble}
 


@table-of-contents{}



@section{The basics}

@subsection{Comments}@index{comments}
@verbatim|{@; this won't be output}|

Yadda yadda yadda @index{screeb}. If you give a mouse a cookie, he's going to ask for a
glass of milk. If you give a mouse a cookie, he's going to ask for a
glass of milk.  @"@"
@section{Alphabetical list}

@subsection{Bold}

Input:
@verbatim[#:indent 4]|{
@bold{This is in bold}
}|
Output:
@bold{This is in bold}

@subsection{Code block}
Input:
@verbatim[#:indent 4]|{
@codeblock|{
           This is a code block
           }|
          }|
Output:
@codeblock|{
           This is a code block
           }|

@subsection{Lists}

@bold{Unordered list}, input:
@verbatim[#:indent 4]|{
@itemlist[
@item{One}
@item{Two}
@item{Three}
]}|
Output:
@itemlist[
@item{One}
@item{Two}
@item{Three}
]

@hspace[1]
@para[#:style 'hspace]{@bold{Ordered list}, input:}
@verbatim[#:indent 4]|{
@itemlist[#:style 'ordered
@item{One}
@item{Two}
@item{Three}
]
}|

@para{Output:}
@itemlist[#:style 'ordered
@item{One}
@item{Two}
@item{Three}
]

Scribble and itemlists:
https://users.racket-lang.narkive.com/N7s8SiU1/racket-scribble-and-itemlists

@subsection{This is a subsection}

@verbatim|{

    Use @bold{---} like this...
       does this indent?
    

  }|


@margin-note|{This is a margin note done with @margin-note}|


@index-section{}
