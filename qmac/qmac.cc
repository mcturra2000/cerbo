// created 2022-06-19
#include <stdio.h>
#include <ctype.h>
#include <iostream>
#include <string>

using namespace std;

enum yytypes{ eof = -1, white = 128, comment, alpha};

//enum yytypes yytype;
int yytype;

string yytext;

int yylex()
{
	int c = getchar();
	yytype = c;
	yytext = c;
	if(isspace(c)) {
		yytype = white;
		while(1) {		
			c = getchar();
			if(!isspace(c)) break;
			yytext += c;
		}
		ungetc(c, stdin);
		return yytype;
	}

	if(c == '#') {
		yytype = comment;
		while(1) {
			c = getchar();
			if(c == EOF || c == '\n') break;
			yytext += c;
		}
		ungetc(c, stdin);
		return yytype;
	}

	if(isalpha(c)) {
		yytype = alpha;
		while(1) {
			c = getchar();
			if(!isalpha(c)) break;
			yytext += c;
		}
		ungetc(c, stdin);
		return yytype;
	}


	return c;
}

int main()
{
	while(yylex() != eof) cout << yytype << ":<" << yytext << ">" << endl;

	return 0;
}
