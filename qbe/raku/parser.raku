my $loop_num = 0;
my $gen_branch_id = 0;

sub do_loop()
{
	$loop_num++;
	say "@loop_start_$loop_num";
}

sub do_xloop()
{
	say "jmp @loop_start_$loop_num";
	say "@loop_end_$loop_num";
}

sub do_jlt($arg1, $arg2, $arg3) 
{
}

grammar G {
	token TOP { <stmt>* }
	proto 	rule stmt	{*}
		rule stmt:sym<_brk>	{ '.brk' { say "@loop_end_$loop_num"; } }
		rule stmt:sym<_jlt>	{ '.jlt' <arg1=.arg> ',' <arg2=.arg> ',' <arg3=.arg> 
						{ do_jlt($/<arg1>, $/<arg2>, $/<arg3>) ; } }
		rule stmt:sym<_loop>	{ '.loop' { do_loop; } }
		rule stmt:sym<_xloop>	{ '.xloop' { do_xloop; } }
		token stmt:sym<any>	{ . { print $/; } }

	token arg { _brk  | <-[,]>+ }
}

my $str = slurp("catty.il");

G.parse($str);
