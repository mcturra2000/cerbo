# c2c - C-to-C compiler

Proof of concept of translating C to C.

NB A cursory test seems to suggest that it works as of 2022-08-21, but it won't like preprocessing directives.


## See also

* [my write-up](gemini://tozip.chickenkiller.com/2022-08-21-c-parser.gmi)


## Status

2022-08-21	Works

2022-08-20	Started
