/*
 * On FreeBSD:
pkg install fortune
pkg install fortune-mod-freebsd-classic
*/

#include <filesystem>
#include <chrono>
#include "utils.h"

using namespace std::filesystem ;


static const char* hdr = 
"┏━╸┏━┓┏━┓╺┳╸╻ ╻┏┓╻┏━╸\r\n"
"┣╸ ┃ ┃┣┳┛ ┃ ┃ ┃┃┗┫┣╸ \r\n"
"╹  ┗━┛╹┗╸ ╹ ┗━┛╹ ╹┗━╸\r\n\r\n";

void handle_fortune_scr(Io& io)
{

	string cmd = "";
	auto set = [&cmd](string exe) { if(exists(exe)) cmd = exe; } ;
	set("/usr/games/fortune");	// For Debian
	set("/usr/bin/fortune");	// For FreeBSD
	assert(cmd.size()>0);

	while(1) {
		io.hdr(hdr);
		io << run_command(cmd);

		switch(io.getchar("\r\n[Paused. a: again. q: quit]")) {
			case 'a' : break;
			case 'q' : return;
		}
	}
}
