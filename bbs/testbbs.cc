#include <iostream>
#include <stdio.h>
#include <poll.h>
#include <cassert>

#include "utils.h"
#include "assets.h"

using namespace std;


#define code(x) cout << #x << " " << x << endl


void colour(int col, const char* name)
{
	//printf("\033[38;5,%dm %s\n", col, name);
	printf("\033[%dm %s\n", col, name);
}
void colours(void)
{
	/* ansi escape sequences:
https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797
*/
	puts("\nColour test on 3-4 bit colours:");	
	//puts("\033[38;5;31m Red");
	//puts("\033[38;5;37m White");
	colour(31, "red");
	colour(32, "green");
	colour(33, "yellow");
	colour(34, "blue");
	colour(35, "magenta");
	colour(36, "cyan");
	colour(37, "white");
}

// simulate LEDs, which is actually braille in range 0x2800-0x28FF
// This requires 3-char encoding . So it will have the form:
// 1110nnnn, 10nnnnnn, 10nnnnnn
// I have simplified the code a little, which might add a little
// to the confusion.

void leds (void)
{
	//puts("\nleds (really used braille unicode");
	for(auto  i = 0; i< 256; i++) {
		//bytes[2] &= ~(0x3F); // clear lower 6 bits
		//bytes[2] |= (i&3F); // set the lower 6 bits
		//bytes[2] = 0b1000'0000 | (i& 0x3F);
		//bytes[1] = 0b1010'0000 | (i>>6);
		string str{utf8_unicode(0x2800 + i)};
		printf("%s", str.c_str());
	}
	puts("");
}

void superscripts(void)
{
	// actually, the output is pretty unreadable anyway
	// weird ordering: 0    1     2     3       4       5       6       7       8       9
	auto unis = {0x2070, 0xB9, 0xB2, 0xB3, 0x2074, 0x2075, 0x2076, 0x2077, 0x2078, 0x2079};
	for(auto uni : unis) {
		string str{utf8_unicode(uni)};
		printf("%s", str.c_str());
	}
}



int main(int argc, char **argv)
{
#if 0
	code(EBADF);
	code(ECONNRESET);
	code(ENOTCONN);
	code(ENOTSOCK);
	code(EMSGSIZE);
	code(ENOTSOCK);
	code(EMSGSIZE);
	code(EAGAIN);
	code(EINTR);
	code(EFAULT);
#endif


#if 0
	cout << "poll stuff\n";
	code(POLLIN);
	code(POLLHUP);
	code(POLLERR);
	code(POLLNVAL);
#endif

	assetdir_init(argv[0]);
	if constexpr(true) colours();
	leds();
	superscripts();

	// following e items work
	printf("\nBunny:%s\n", utf8_unicode(0x1F407).c_str()); 
	printf("Moon:%s\n", utf8_unicode(0x1F311).c_str());
	printf("Alien:%s\n", utf8_unicode(0x1F47E).c_str());

	assert(false); // test of core dump

	return 0;

}
