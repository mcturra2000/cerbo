//#include <format>
#include <iostream>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <cstdio>
#include <unistd.h>
#include <fcntl.h>
//#include <inttypes.h>
#include <boost/algorithm/string/trim.hpp>

#include "utils.h"
#include "gofer.h"

//using namespace std;

//#include <filesystem>
//namespace fs = std::filesystem;

using std::string;
using std::vector;
using std::to_string;
using std::cout;

typedef std::vector<std::string> strings_t;



class Console : public Io {
	public:
		Console() {};
		~Console() {};
		//friend void  operator<<(Console& con, const char* str);
		//friend void operator<<(Console& con, const std::string& str);
                void operator<<(const std::string& str) { std::cout << str; };
                void operator<<(const char* str) { std::cout << str; };
		Console& ret(Console& con) {	std::cout << std::endl; return con; };
                void send(const char* str) { std::cout << str ; };
		std::string getline(void) { std::string input; std::getline(std::cin, input); return input; }
		int getchar(void) { return getchar(); }
		void putchar(int c) { cout << c; };

};

void operator<<(Console& con, const std::string& str) 
{
	std::cout << str;
};

void operator<<(Console& con, const char* str) 
{
	std::cout << str;
};

Console& endl(Console& con)
{
	std::cout << "\n";
	return con;
}

gopher_line_t decode_line (string line, int& link_count)
{
	gopher_line_t gl;
	if(line.size() == 0 ) return gl;

	if(line[line.size() -1] == '\r') line.erase(line.end()-1, line.end());
	auto fields = split(line, '\t');
	if(fields.size() < 4) return gl;

	auto field0 = fields.at(0);
	if(field0.size() == 0) return gl;
	auto type = line[0];
	gl.type = type;
	if(field0.size() > 1) gl.text = field0.substr(1);

	auto selector = fields[1];
	auto host = fields[2];
	auto port = fields[3];

	if(type == '0' || type == '1') {
		//gp.num_links++;
		//gl.url_id = ++gp.num_links;
		gl.url_id = ++link_count;

		string slash;
		if(selector.size() > 0 && selector.at(0) != '/') slash = '/';
		//if(fields.at(1).at(0) != '/') slash = '/';

		auto url = "gopher://" + host +  ":" + port + "/" +  type + slash + selector ;
		gl.url = url;
	}
	return gl;
}


gopher_page_t decode_page (string& raw_contents)
{
	gopher_page_t gp;
	auto lines = split(raw_contents, '\n');
	for(auto& line : lines) gp.gopher_lines.push_back(decode_line(line, gp.num_links));
	return gp;
}

enum Response { resp_continue, resp_quit, resp_link, resp_back };


int get_response (Io& io, int num_links, bool complete, int& link_num, bool allow_back)
{
	while(1) { 
		io << cyan();
		io << "[Paused (q: quit, <num> for link";
		if(!complete)  io << ", c: continue";
		if(allow_back) io << ", b: back";
		io << ")]";
		io << white();

		auto input = io.getline();
		boost::algorithm::trim(input);
		io << "\r\n";
		if(input.size()>0 && input[0] == 0) input = input.substr(1); // peculiar bug
		if((input == "c" || input == "") && !complete) return resp_continue;
		if(input == "b" && allow_back) return resp_back;
		if(input == "q") return resp_quit;
		int input_link_num = atoi(input.c_str());
		if((0 < input_link_num) && (input_link_num <=num_links)) {
			link_num = input_link_num;
			return resp_link;
		}
	} 
}

string change_url(const gopher_page_t& gp, int link_num)
{
	for(auto& gl: gp.gopher_lines) {
		if(gl.url_id == link_num)
			return gl.url;
	}
	return "YIKES";
}


std::string fetch(std::string url)
{
       	return run_command("/usr/local/bin/bombadillo -p " + url);
}

void loop (Io& io, strings_t& url_trail)
//void loop (auto& io, strings_t& url_trail)
{
	while(1) {
		if constexpr(false) {
			std::cout << "url trail\n";
			for(auto& url: url_trail) std::cout << "   " <<url << "\n";
			std::cout << ".\n";
		}

		auto raw_contents = fetch(url_trail.back());

		io << cls();
		auto gp = decode_page(raw_contents);
		for(auto& gl : gp.gopher_lines) {
			if(gl.url_id>=0) gl.text += fg(32) + " [" + std::to_string(gl.url_id) + "]\033[22m" + fg(37);
		}


		int line_num = 0;
		int link_num = 0;
		bool complete = false;
		while(!complete) {
			for(int i = 0; i<22; i++) {
				if(line_num < gp.gopher_lines.size()) {
					io << gp.gopher_lines[line_num++].text;
					io << "\r\n";
				} else 
					complete = true;		
			}
			switch(get_response(io, gp.num_links, complete, link_num, url_trail.size()>1))
			{
				case resp_continue:
					break;
				case resp_link:
					url_trail.push_back(change_url(gp, link_num));
					complete = true;
					break;
				case resp_back:
					if(url_trail.size() > 1) url_trail.pop_back();
					complete = true;
					break;
				case resp_quit: return;
			}

		}


	}

}

void browse_gopher(Io& io, std::string initial_url)
{
	strings_t url_trail{{initial_url}};
	loop(io, url_trail);
}

void browse_gopher(Io& io) {
	browse_gopher(io, "gopher://tozip");
}

/*
   void browse_gopher(Sock& io)
   {
   strings_t url_trail{{"gopher://tozip"}};
   loop(io, url_trail);
   }
   */

#ifdef gofer
int main()
{
	Console con;
	browse_gopher(con);
	return 0;
}
#endif
