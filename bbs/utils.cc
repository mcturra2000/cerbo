#include <sstream>
#include <fstream>
#include <iostream>
#include <fcntl.h>
#include <thread>
#include <mutex>
#include <unistd.h>
#include <time.h>
#include <filesystem>
#include <sys/types.h>
#include <sys/socket.h>
#include <poll.h>


#include "utils.h"

//export module utils;

using namespace std;
using namespace std::filesystem;


std::string run_command (std::string cmd)
{
	static int id = 0;
	id++;
	string fname{"/tmp/bbs." + to_string(getpid()) + "." + to_string(id)};
	cmd +=  " >" + fname;
	system(cmd.c_str());
	auto raw_contents = slurp(fname);
	remove(fname.c_str());
	return raw_contents;
}


// See:
// https://en.wikipedia.org/wiki/UTF-8
//
string utf8_unicode(int32_t uni)
{
	//uint8_t bytes[5] = { 0b1110'0000, 0b1000'0000, 0b1, 0, 0};
	//uint8_t bytes[5] = { 0, 0, 0, 0, 0};
	//assert(uni >= 0x800 && uni < 0x10000);

	constexpr auto k6 = 0b11'1111;
	constexpr auto ext = 1<<7;
	if(uni < 0x80) {
		string str{"N"};
		str[0] = uni;
		return str;
	}
	if(uni < 0x0800) {
		string str{"NN"};
		str[0] = 0b1100'0000 + (uni>> 6);
		str[1] = ext + (uni & k6);
		return str;
	}
	if(uni < 0x10000) {
		string str{"NNN"};
		str[0] =  0b1110'0000 + (uni>>12);
		str[1] = ext + ((uni>>6) & k6);
		str[2] = ext + (uni & k6);
		//return string(bytes[0] + bytes[1] + bytes[2]);
		return str;

	}
	//puts("deep into the woods");
	assert(uni<= 0x10FFFF);
	string str{"NNNN"};
	str[0] =  0b1111'0000 + (uni>>18);
	str[1] = ext + ((uni>>12) & k6);
	str[2] = ext + ((uni>>6) & k6);
	str[3] = ext + (uni & k6);
	return str;
}

void Sock::send(const char* str)
{ 
	while(*str) 
		this->putchar(*str++);

	//send_str(m_conn, str); 
}

void Sock::putchar(int c)
{
	if(c=='\n') this->putchar('\r');
	sendc(m_conn, c);
}

void Io::newscr()
{
	*this << cls();
	*this << home();
}

void Io::hdr (const char* hdr)
{
	this->newscr();
	*this << cyan();
	*this << hdr;
	*this << white();
}

int Io::getchar(const string& prompt)
{ 
	*this << prompt; 
	return this->getchar(); 
}

std::vector<std::string> split(const std::string& str, char sep, int &n)
{
	vector<string> res;
	stringstream sin{str};
	string field;
	while(std::getline(sin, field, sep)) {
		res.push_back(field);
	}
	n = res.size();
	return res;
}

std::vector<std::string> split(const std::string& str, char sep)
{
	int n;
	return split(str, sep, n);
}

/* non-blocking key check, Return -1 if no key found
*/
int getkey (int conn, int timeout_ms)
{
	const nfds_t nfds = 1;
	struct pollfd pfds[nfds];
	//int timeout = 1; // ms
	pfds[0].fd = conn;
	pfds[0].events = POLLIN | POLLHUP;
	int n = poll(pfds, nfds, timeout_ms);
	if(n==0) return -1;
	//cout << "getkey: revents" << to_string(pfds[0].revents) << endl;
	if(pfds[0].revents == POLLIN) {
		char c;
		int i = read(conn, &c, 1);
		if constexpr(false) { printf("%02Xh ", (unsigned char) c); fflush(stdout);}
		//cout << "getkey: key: " << c <<endl;
		return c;
	}
	return -1;
}

void write_time (Io& io)
{
	io << formatted_time("%Y/%m/%d, %H:%M:%S\r\n");
}
void sleep_ms (int ms)
{
	this_thread::sleep_for(chrono::milliseconds(ms) );
}

void sendc (int conn, char c)
{
	if(send(conn, &c, 1, MSG_NOSIGNAL) < 0)
		throw send_err;
}	


char recv_char (int conn)
{
	char c = 0;
	auto flags  = 0;
	flags = MSG_WAITALL;
	flags = MSG_DONTWAIT;
	flags = MSG_PEEK | MSG_DONTWAIT;
	flags = 0;
	auto n = recv(conn, &c, 1, flags);
	if constexpr(false) { int h = (unsigned char) c; printf("%02Xh ", h); fflush(stdout);}
	if(n==0) throw recv_err; // likely that client quit
	if(c==3) throw recv_ctl_c; // user hits Ctrl-C for immediate quit
	return c;
}

string recv_str (Io& io, int max_len, bool echo)
{
	string str;
	int c = 0;
	while(str.size() < max_len) {
		c = io.getchar();
		if(echo) io.putchar(c);
		str += c;
		if( c == '\r' || c == '\n') break;
	}
	if(c != '\n') str += '\n';
	return str;
}
void send_str (int conn, const std::string& msg)
{
	auto flags = MSG_NOSIGNAL; // will check for a busted connection
	int status = send(conn, msg.c_str(), msg.size(), flags); 
	if(status <0) throw send_err;
}

void send_str (int conn, const char* msg)
{
	send_str(conn, string(msg));
}

void send_line (int conn, const std::string& msg)
{
	send_str(conn, msg);
	send_str(conn, "\r\n");
}

char request_key (int conn, const string& msg)
{
	send_str(conn, msg);
	return recv_char(conn);
}

char request_key (int conn, const char* msg)
{
	return request_key(conn, string(msg));
}

std::string home(void)
{
	return "\033[H";
}

/*
   [[deprecated("Use layout of gopher_scr.cc instead")]]
   void show_header (int conn, const char* header)
   {
   send_str(conn, "\033[2J"s); // clear screen 
   send_str(conn, "\033[7~"s); //home
   send_str(conn, "\033[H"s); //home // works on xterm
   send_scr(conn, header);
   }
   */

std::string expand_path(const std::string_view& dir, const std::string_view& tail)	
{
	string p{dir};
	//if(dir.size() == 0) return "";
	if(dir[0] == '~') {
		p = string(getenv("HOME")); 
		p += dir.substr(1);

	}

	if(tail.size() > 0) {
		p += '/';
		p +=  tail;
	}

	return p;
}

void make_dir(const std::string_view& dir, const std::string_view& tail)
{
	auto p{expand_path(dir, tail)};
	cout << "mkdir:" << p << endl;
	create_directories(p);
}


using sv = std::string_view;



std::string slurp(const std::string& filename)
{
	std::ifstream t(filename);
	std::stringstream buffer;
	buffer << t.rdbuf();
	return buffer.str();
}


std::string formatted_time(const char* fmt)
{
	time_t rawtime;
	struct tm * timeinfo;
	time (&rawtime);
	timeinfo = localtime (&rawtime);
	char buffer [80];
	strftime(buffer,80, fmt,timeinfo);
	return buffer;
}




Timer::Timer(int ms, bool trigger_instantly)
{
	m_ms = ms;
	first = trigger_instantly;
}

std::chrono::steady_clock::time_point Timer:: now()
{
	return std::chrono::steady_clock::now();
}

bool Timer::expired(void)
{
	using ms_t = std::chrono::milliseconds;
	auto dur = now() - last;
	auto elapsed_ms = std::chrono::duration_cast<ms_t>(dur).count();
	bool trigger = first || (elapsed_ms >= m_ms);
	first = false;
	if(trigger) {
		last = now();
		return true;
	} else 
		return false;

}

std::string esc (int val, char c)
{
	char seq[10];
	snprintf(seq, 10, "\033[%d%c", val, c);
	return seq;
}

void esc (int conn, int val, char c)
{
	char seq[10];
	snprintf(seq, 10, "\033[%d%c", val, c);
	send_str(conn, seq);
}

std::string fg (int colour) // set foreground colour
{
	return esc(colour, 'm');
}

void fg (int conn, int colour) // set foreground colour
{
	esc(conn, colour, 'm');
}

void gotox (int conn, int x) 
{ 
	esc(conn, x, 'G'); 
} // untested

void gotoxy (Io& io, int x, int y) 
{ 
	char seq[10];
	snprintf(seq, 10, "\033[%d;%df", y, x);
	io << seq;
}

std::string cyan (void) { return esc(36, 'm'); }
std::string white (void) { return esc(37, 'm'); }
std::string green (void) { return esc(32, 'm'); }
std::string cls (void) { return "\033[2J"; }


std::string Sock::getline(void)
{ 
	return recv_str(*this, INT_MAX); 
}

//std::string getline(void) { std::string input; std::getline(std::cin, input); return input; }; // TODO

char recv_one_of (Io& io, const char* opts)
{
	while(1) {
		int c = io.getchar();
		for(auto i = 0 ; opts[i] ; i++) {
			if(opts[i] == c) return c;
		}
	}
}
