#include <assert.h>
#include <iostream>

#include "assets.h"
#include "utils.h"

using namespace std;

void assetdir_init(char *argv0)
{
	auto fs1 = fs::path(argv0);
	auto fs2 = canonical(fs1);
	auto fs3 = fs2.remove_filename(); // /abs/path/to/exe/ (with trailing /)
	fs3 += "/assets";
	assetdir = canonical(fs3);
	cout << "assetdir_init():" << assetdir << endl;
}


string Asset::text()
{
	assert(assetdir != ""); // You need to call assertdir_init(argv[0]) from main()
	if(!data.has_value()) {
		data = slurp(assetdir / m_filename);
	}
	return data.value();
}

