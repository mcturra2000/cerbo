#include "utils.h"

using std::cout;
using std::endl;
using std::ofstream;
				   /*
				   void handle_fortune_scr(Io&);
-                       case 'v' :
-                                  send_str(conn, get_wall());
-                                  request_key(conn, "Press any key to exit this screen");
-                                  return;
-                       case 'm' :
-                                  sendc(conn, '>');
-                                  write_wall(recv_str(conn, 140));
-                                  send_str(conn, "\nDone. Choose something else\r\n");
-                                  request_key(conn, "Press any key to exit this screen");
-                                  return;
*/

std::mutex wall_mtx; // used to lock the wall for reading/writing
static const std::string wall_file{expand_path(bbs_data, "wall.txt")};
//std::string get_wall(void);
void write_wall(const std::string& msg);

std::string get_wall (void)
{
	const std::lock_guard<std::mutex> lock(wall_mtx);
	cout << "get_wall: " << wall_file << endl;
	string contents =  slurp(wall_file);
	//cout << contents;
	return contents;
}
void write_wall (const std::string& msg)
{
	const std::lock_guard<std::mutex> lock(wall_mtx);
	cout << "write_wall: " << wall_file << endl;
	//string contents = get_wall() + msg;
	ofstream myfile;
	myfile.open (wall_file, std::ofstream::out | std::ofstream::app);
	myfile << msg;

	myfile << formatted_time("-- %F %I:%M%p\r\n");

	myfile.close();
}

static const char* hdr =
"╻ ╻┏━┓┏━╸┏━┓   ╻ ╻┏━┓╻  ╻   \n"
"┃ ┃┗━┓┣╸ ┣┳┛   ┃╻┃┣━┫┃  ┃   \n"
"┗━┛┗━┛┗━╸╹┗╸   ┗┻┛╹ ╹┗━╸┗━╸ \n"
;


static const char *kilroy[] = 
{      "          '''  ",
       "         (o o)  ",
       "------ooO-(_)-Ooo------",
       0, };




void show_art(Io &io, int x, int y, const char** art) // TODO utils
{
	while(*art) {
		gotoxy(io, x, y++);
		io << *art++;
	}
}

void handle_wall_scr(Io& io)
{
	while(1) {
		io.hdr(hdr);
		//io << cyan();
		io << green();
		show_art(io, 40, 1, kilroy);
		io << white();
		io << "\nChoose: r (read ), w (write, 140 chars max) wall. q to quit this screen\n";
		int c = recv_one_of(io, "qrw");
		string msg;
		switch(c) {
			case 'q' :
				return;
			case 'r': 
				io << get_wall(); 
				io.getchar("\nPress any key to continue");
				break;
			case 'w': 
				io.putchar('>');
				msg = recv_str(io, 140);
				write_wall(msg); 
				break;

		}
	}
}

