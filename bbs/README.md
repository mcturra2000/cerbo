# bbs - bulletin board system

## Setup

### Debian

```
sudo apt install libcurlpp-dev libboost-dev libboost-doc
```

## FreeBSD
```
pkg install curlpp

g++12 -c gofer.cc works
g++12 gofer.o -o gofer -lcurlpp -lcurl
gofer causes segmentation fault, though:q
```

The C++ version seems to segfault. Use C instead.


## Connecting to the server

```
telnet localhost 2039
```

It is best to enable character at a time mode:

stty -icanon -echo && nc localhost 9999

-echo stops echoing to screen:wq

Using putty:
Session:
	Connection type: raw
	Host  Name: localhost
	Port 9999
Terminal: Line disciplin options:
	Local echoL Force off
	Local line eiditing: Force off
	

## Fonts

To generate fonts on Debian:
```
sudo apt install figlet toilet toilet-fonts
```

Then
```
figlet -f future "My text"
```
The font is called future.tlf

## See also

* [core](core.md) - dumps

## Links to other sites

* [ansi escape sequences](https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797)
* [telnet commands and options](https://www.ibm.com/docs/en/zos/2.1.0?topic=problems-telnet-commands-options)


## Status

2022-07-18	Created
