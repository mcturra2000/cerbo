#include <filesystem>

#include "assets.h"
#include "utils.h"


using std::cout;
using std::endl;


static const char* hdr = 
"┏━┓┏━┓╺┳╸╻ ╻╻┏ ┏━┓\r\n"
"┣┳┛┃ ┃ ┃ ┣━┫┣┻┓┃ ┃\r\n"
"╹┗╸┗━┛ ╹ ╹ ╹╹ ╹┗━┛\r\n";



Asset rothko{"rothko.txt"};
Asset cadubi{"cadubi.txt"};


				
void cadubi_help(Io& io)
{
	io.newscr();
	io << cadubi.text();
	io.getchar();
}

void handle_rothko_scr (Io& io)
{
	while(1) {
		io.hdr(hdr);
		io << rothko.text();
		io << "c: cadubi help file (any key exits)\r\n";
		io << "q: quit\r\n";

		switch(io.getchar()) {
			
			case 'c':
				cadubi_help(io);
				break;
			case 'q': return;
		}
	}

}

