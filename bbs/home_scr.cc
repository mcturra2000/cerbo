#include "utils.h"


extern const char* home_scr_txt; // actually defined below

//////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////
void blinkenlights (Io& io)
{
	io << green();
	const int x0 = 31;
	uint8_t bytes[4] = { 0b1110'0010, 0, 0, 0};
	for(auto y = 1; y < 4; y++) {
		gotoxy(io, x0, y);
		for(auto  x = x0; x< 81; x++) {
			auto r = rand() % 256;
			bytes[2] = 0b1000'0000 | (r& 0x3F);
			bytes[1] = 0b1010'0000 | (r>>6);
			io <<  (char*) bytes;
		}
	}
	io << white();

}


static const char* hdr =
"╻  ╻ ╻   ╻ ╻┏━┓┏━┓   ┏┓ ┏┓ ┏━┓\n"
"┃  ┃┏┛╺━╸┗━┫┏━┛┣━┓   ┣┻┓┣┻┓┗━┓\n"
"┗━╸┗┛      ╹┗━╸┗━┛   ┗━┛┗━┛┗━┛\n"
"MU-TH-UR 6000 boot sequence complete. Enter command.\r\n";

static const char* menu = 
"f: fortune\n"
"g: gopher pages\n"
"r: rothko drawing\n"
"s: server status\n"
"t: tests\n"
"w: wall (user messages)\n"
"q: quit\n\n"
"Typing Ctl-C anywhere causes the sesson to quit\n\n"

"Created : 2022-07-17\n"
"Email   : oblate (at) zoho (dot) com"
;

void handle_home_scr (Io& io, int conn) // use of conn should be considered deprecated
{
	//show_header(conn, home_scr_txt);
	io.hdr(hdr);
	io << menu;
	Timer tim(1000);

	while(1) {
		if(tim.expired()) 
			blinkenlights(io);

		int c = getkey(conn);

		extern void handle_fortune_scr(Io&);
		extern void handle_gopher_scr(Io&);
		extern void handle_rothko_scr(Io&);
		extern void handle_status_scr(Io&);
		extern void handle_test_scr(Io&);
		extern void handle_wall_scr(Io&);

		switch(c) {
			case 'f':  handle_fortune_scr(io); return;
			case 'g':  handle_gopher_scr(io); return;
			case 'r':  handle_rothko_scr(io); return;
			case 'w' : handle_wall_scr(io); return;
			case 'q' : throw sess_abort;
			case 's' : handle_status_scr(io); return;
			case 't' : handle_test_scr(io); return;
		}
	}

}


