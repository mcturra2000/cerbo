#include <chrono>

#include "utils.h"

using namespace std;


void colour_test(Io&);
void interactivity_test(Io&);

		
void found_secret(Io& io)
{
	static const char* hdr = 
		"┏━┓┏━╸┏━╸┏━┓┏━╸╺┳╸\r\n"
		"┗━┓┣╸ ┃  ┣┳┛┣╸  ┃ \r\n"
		"┗━┛┗━╸┗━╸╹┗╸┗━╸ ╹ \r\n\n"
		;

	io.hdr(hdr);
	io << "Congratulations: You have found a secret screen\r\n\n";
	io << utf8_unicode(0x1F95A); // egg unicode
	io << " You found an alien ... I mean easter ... egg in this BBS. It's so beautiful.\r\nTake a real close look.\r\n";
	io.getchar("Press any key to exit this screen.");		
}

static const char* hdr = 
"╺┳╸┏━╸┏━┓╺┳╸   ┏━┓┏━╸┏━┓┏━╸┏━╸┏┓╻\r\n"
" ┃ ┣╸ ┗━┓ ┃    ┗━┓┃  ┣┳┛┣╸ ┣╸ ┃┗┫\r\n"
" ╹ ┗━╸┗━┛ ╹    ┗━┛┗━╸╹┗╸┗━╸┗━╸╹ ╹\r\n"
;

static const char* menu =
"1: colour test\r\n"
"2: interactivity test\r\n"
"q: quit screen\r\n"
;

void handle_test_scr(Io& io)
{
	while(1) {
		io.hdr(hdr);
		io << menu;

		switch(io.getchar()) {
			case '1' : colour_test(io); break;
			case '2' : interactivity_test(io); break;
			case 's' : found_secret(io); break;
			case 'q' : return;
		}
	}
}

void show_colour(Io& io, int col, const char* name)
{
	//printf("\033[38;5,%dm %s\n", col, name);
	char msg[100];
	sprintf(msg, "\033[%dm %s\r\n", col, name);
	io << msg;
}

void colour_test (Io& io)
{
	/* ansi escape sequences:
https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797
*/
	static const char* hdr = "Colour test on 3-4 bit colours:\n"
		"https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797\n";	
	//show_header(conn, hdr);
	io << hdr;
	auto colour = [&io](int col, const char* name) { show_colour(io, col, name); } ;
	//puts("\033[38;5;31m Red");
	//puts("\033[38;5;37m White");
	colour(31, "red");
	colour(32, "green");
	colour(33, "yellow");
	colour(34, "blue");
	colour(35, "magenta");
	colour(36, "cyan");
	colour(37, "white");

	io << "256 colour test:\r\n";
	for(auto i = 0; i<256; i++) {
		char msg[20];
		sprintf(msg, "\033[38;5;%dm %3d ", i, i);
		io << msg;
	}
	//send_line(conn, "\033[38;5;125m	colour 125");
	colour(37, "white");
	int c = io.getchar("Press any key to exit");
	cout << "colour_test:getchar: " << c << endl;

}


void interactivity_test (Io& io)
{
	using namespace std::chrono;

	steady_clock::time_point t1 = steady_clock::now();
	const  std::chrono::seconds sec1(1);
	while(1) {
		int c = io.getchar(); // TODO prolly wrong. Need to use getkey
		if(c == 'q') return;

		steady_clock::time_point t2 = steady_clock::now();
		auto dur = t2 - t1;
		typedef std::chrono::seconds secs_t;
		auto secs = std::chrono::duration_cast<secs_t>(dur);
		if(secs.count() >= 1) {
			write_time(io);
			t1 += sec1;
		}
		sleep_ms(10);
	}
}
