#pragma once
#include <string>
#include <vector>


typedef struct {
	char type = 'i';
	std::string url;
	std::string text;
	int url_id = -1; 
} gopher_line_t;


typedef struct {
	std::vector<gopher_line_t> gopher_lines;
	int num_links = 0;
} gopher_page_t;

std::string fetch(std::string url);
gopher_page_t decode_page (string& raw_contents);
void browse_gopher(Io& io, std::string initial_url);
void browse_gopher(Io& io);
