/* test polling of input of stdin and writes the input in upper case.
 *
 * Only need to do this prior to 2022-10-21:
 * --- Begin ---
 * Run:
 * 	stty -echo -icanon time 0 min 0 && ./poll
 * Afterwards, return tty to sane state:
 * 	stty sane
 * --- End ---
 *
 * 2022-12-02	Added some modularity
 * 2022-10-21	Controlling how input and output works via termios
 * 2022-07-21	created. Works
 */

#include <ctype.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/poll.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdexcept>
#include <termios.h>

static struct termios term_ori; // original term that we want to restore when we've finished

void term_init(void)
{
	// https://linux.die.net/man/3/termios
	struct termios term;
	if(tcgetattr(STDIN_FILENO, &term) < 0) exit(1); // what are the current attributes?
	term_ori = term; // creat a copy for later restoration
	cfmakeraw(&term); // raw mode; char-by-char, echo disabled, special chars disabled
	// cfmakeraw() does too much, so we have to set things back again
	term.c_iflag |= ICRNL; // translate CR to NL on input
	term.c_oflag |= OPOST; // get newlines working properly again on output
	term.c_lflag |= ISIG; // re-enable INTR, QUIT, SUSP, DSUPS. Ctl-C will work again.
	if(tcsetattr(STDIN_FILENO, TCSANOW, &term)) exit(1); // set the attributes how we wish
}

void term_deinit(void)
{
	if(tcsetattr(STDIN_FILENO, TCSANOW, &term_ori)) exit(1);
}

/*
 * NB stdout will be supressed unless there is a \n for an fflush(stdout)
 */
int get_key_blocking(void)
{
	int fd;
	char c;
	const nfds_t nfds = 1;
	struct pollfd pfds[nfds];
	int timeout = 1; // ms
	const int stdin_fd = STDIN_FILENO;
	pfds[0].fd = stdin_fd; // stdin
	pfds[0].events = POLLIN | POLLHUP;
	while(1) {
		int n = poll(pfds, nfds, timeout);
		if(n==0) continue;
		if(pfds[0].revents & POLLHUP) {
			return EOF;
		}
		if(pfds[0].revents & POLLIN) {
			int i = read(stdin_fd, &c, 1);
			return c;
			//putchar(toupper(c));
			//printf(" %d, ", c);
			//fflush(stdout);
		}
	}
}

int main() {

	term_init();
	puts("type 'q' to quit. I will echo output in uppercase");


	while(1) {
		//putchar('.');
		int c= get_key_blocking();
		if(c == EOF) {
			printf("\nEOF detected\n");
			break;
		}
		if(c == 'q') break;
		putchar(toupper(c));
		fflush(stdout); // important!
	}


	term_deinit();
	puts("Bye now");
	return 0;
}
