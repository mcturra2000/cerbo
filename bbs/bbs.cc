#include <netinet/in.h> // For sockaddr_in
#include <cstdlib> // For exit() and EXIT_FAILURE
#include <iostream> // For cout
#include <unistd.h> // For read
#include <thread>
#include <signal.h>
#include <string>
#include <cassert>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <poll.h>


#include "assets.h"
#include "utils.h"

//import utils;

using namespace std::string_literals;
using namespace std;

const int port = 2039; // port to bind to

//using sv = std::string_view;
using sv = std::basic_string<char>;
constexpr sv test_tmp(const sv& a, const sv& b) { return a + b; } // works

//////////////////////////////////////////////////////////////////////////////////////
// defer
template <typename F>
struct privDefer {
	F f;
	privDefer(F f) : f(f) {}
	~privDefer() { f(); }
};

template <typename F>
privDefer<F> defer_func(F f) {
	return privDefer<F>(f);
}

#define DEFER_1(x, y) x##y
#define DEFER_2(x, y) DEFER_1(x, y)
#define DEFER_3(x)    DEFER_2(x, __COUNTER__)
#define defer(code)   auto DEFER_3(_defer_) = defer_func([&](){code;})

//////////////////////////////////////////////////////////////////////////////////////

constexpr auto maxNumConnections = 3;

void report(const string& msg)
{
	cout << msg << endl;
}

void err_msg(const string& msg)
{
	cout << "Error: " << msg << ", errno: " << errno << endl;
}

bool make_nonblocking(int fd)
{
	// make socket non-blocking
	int flags;
	if (-1 == (flags = fcntl(fd, F_GETFL, 0))) flags = 0;
	int res = fcntl(fd, F_SETFL, flags | O_NONBLOCK);
	if(res != -1) {
		report("fcntl set to non-blocking");
	} else {
		err_msg("fcntl couldn't be set to non-blocking");
		throw runtime_error("make_nonblocking failed");
	}
	return true;
}



//////////////////////////////////////////////////////////////////////////////////////
using namespace std::chrono;

typedef struct {
	std::thread::id thread_id;
	//steady_clock::time_point start_time;
	system_clock::time_point start_time;
} session_t;

std::list<session_t> sessions;

std::mutex sessions_mtx;

void add_session(std::thread::id tid)
{

        const std::lock_guard<std::mutex> lock(sessions_mtx);
	session_t s{tid, system_clock::now()};
	sessions.push_back(s);
}

void remove_session(std::thread::id tid)
{
        const std::lock_guard<std::mutex> lock(sessions_mtx);

	auto me = [&tid](const session_t& s) { return tid == s.thread_id;};
	//string msg{std::to_string(tid)};
	sessions.remove_if(me);
}

std::string string_sessions(void)
{
        const std::lock_guard<std::mutex> lock(sessions_mtx);
	std::stringstream ss;

	//string str;
	for(auto& s: sessions) {
		std::time_t started = std::chrono::system_clock::to_time_t(s.start_time);
		//std::tm started_tm = *std::localtime(&started);

/// now you can format the string as you like with `strftime`
		ss << s.thread_id << '\t' << ctime(&started); //  << "\r\n";
	}
	return ss.str();
}

//////////////////////////////////////////////////////////////////////////////////////

void iac_do(int conn, uint8_t cmd)
{
	unsigned char msg[] = "\xFF\xFD\x00";
	msg[2] = cmd;
	send_str(conn, (char*) msg);
}

std::string noecho(void) // TODO
{
	//return "";
	return "\033[8m"; // ansi esc seq - but also seems to suppress all output
	return "\xFF\xFE\x01"; // telnet version
}

void discard_telnet_preamble(int conn)
{
#if 1
	return;
	auto recv = [&conn]() { 
		try { 
			return recv_char(conn);
		} catch(int e) {
			cout << "discard_telnet_preamble" << endl;
			if(e == recv_ctl_c)
				return (char) recv_ctl_c;
		}
		return '\0';
	};
	int c;
	iac_do(conn, 0xF1); // tell telnet to send a NOP, which it should refuse
			    // now eat telnet responses until we see a response to the nop
	while(1) {
		while(recv() != 0xff); // get an IAC
		recv(); // it wither will or wont do nop
		if(recv() == 0xF1) break; // OK, it's responded, so we can proceed
	}



#else
	send_line(conn, "Welcome to the LV-426 BBS. Press ESC twice within 15 seconds to continue.");
	Timer tmr(15000, false);
	auto num_escs = 0;
	//iac_do(conn, 0xF1); // tell telnet to send a NOP
	//iac_do(conn, 5); // tell telnet to send a NOP
	while(1) {
		if(getkey(conn) == '\033') num_escs++; // user presed ESC
		if(num_escs ==2) break;
		if(tmr.expired()) {
			send_line(conn, "Timed out. Exiting. Maybe try connecting again.");
			throw sess_abort;
		}
	}

	//while(recv_char(conn) != CR);
#endif
}


void start_session (int conn, sockaddr_in sockaddr, int addrlen)
{
	defer( {cout << "Ending session" << endl;  numConns--; close(conn);} );

	Sock sock;
	sock.m_conn = conn;
	numConns++;
	sessionsServed++;
	//make_nonblocking(conn);
	add_session(std::this_thread::get_id());

	extern void handle_home_scr (Io&, int);
	try {
		//discard_telnet_preamble(conn);
		sock << "\xFF\xFD\x22\xFF\xFD\x01"s; // put the calling telnet into character mode instead of linemode
		sock << "\033[?25l"; // make cursor invisible. Works
		sock << "\033[0m";
		//sock << noecho();
		while(1) {
			handle_home_scr(sock, conn);
		}
	} catch(...) {
		try { // try cleanup. NB session may have already ended
			sock << esc(2, 'J'); // erase screen. Works
			sock <<  "\033[?25h"; // make cursor visible. Works
		} catch(...) {}
	}
	remove_session(std::this_thread::get_id());
	// close(conn) ??
}

static volatile int keepRunning = 1;
static volatile int sockfd =-1;
void intHandler(int sig) {
	keepRunning = 0;
	if(sockfd != -1) 
		close(sockfd);
	printf("Bye from bbs, process %d\n", getpid());
	exit(0);
}

//using sv =  std::string_view ;


int main (int argc, char *argv[])
{
	make_dir(bbs_data);
	assetdir_init(argv[0]);

	// Create a socket (IPv4, TCP)
	sockfd = socket(AF_INET, SOCK_STREAM, 0); // closed by intHandler
	if (sockfd == -1) {
		std::cout << "Failed to create socket. errno: " << errno << std::endl;
		exit(EXIT_FAILURE);
	}
	// allow reuse of address immediately
	const int on = 1;
	int status = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));
	if(status == -1) {
		std::cout << "Failed to set socket opt. errno: " << errno << std::endl;
		exit(EXIT_FAILURE);
	}


	// Listen to port 2039 on any address - 2039 was when LV-426 was first detected
	sockaddr_in sockaddr;
	sockaddr.sin_family = AF_INET;
	sockaddr.sin_addr.s_addr = INADDR_ANY;
	sockaddr.sin_port = htons(port); // htons is necessary to convert a number to
					 // network byte order
	if (bind(sockfd, (struct sockaddr*)&sockaddr, sizeof(sockaddr)) < 0) {
		std::cout << "Failed to bind to port " << port << ". errno: " << errno << std::endl;
		exit(EXIT_FAILURE);
	}
	std::cout << "Bind to port " << port << std::endl;

	//make_nonblocking(sockfd);


	if (listen(sockfd, maxNumConnections) < 0) {
		std::cout << "Failed to listen on socket. errno: " << errno << std::endl;
		exit(EXIT_FAILURE);
	}
	signal(SIGINT, intHandler);
	signal(SIGPIPE, SIG_IGN); // ignore sigpip, so that we can handle broken sockets ourselves (didn't help)
				  //if (signal (SIGKILL, intHandler) == SIG_IGN) signal(SIGKILL, SIG_IGN);
				  //if (signal (SIGQUIT, intHandler) == SIG_IGN) signal(SIGQUIT, SIG_IGN);
				  //if (signal (SIGHUP, intHandler) == SIG_IGN) signal(SIGHUP, SIG_IGN);
				  //if (signal (SIGTERM, intHandler) == SIG_IGN) signal(SIGTERM, SIG_IGN);

				  //defer({printf("Bye\n");});


				  //throw runtime_error("Ohoh"); actually works

				  //try {
	while(1) {
		// Grab a connection from the queue
		auto addrlen = sizeof(sockaddr);

		int connection = accept(sockfd, (struct sockaddr*)&sockaddr, (socklen_t*)&addrlen);
		if (connection < 0) {
			//sleep_ms(100);
			std::cout << "Failed to grab connection. errno: " << errno << std::endl;
			exit(EXIT_FAILURE);
		} else {
			sockaddr_in sock = sockaddr;
			std::jthread t(start_session, connection, sock, addrlen);
			t.detach();
		}


	}
	//} catch(...) { cout << "main: catch error" << endl; }
}
