#include <filesystem>

#include "utils.h"
#include "gofer.h"

//extern void browse_gopher(Sock&);

using std::cout;
using std::endl;

void browse_random_site(Io& io)
{
	/*
	 * using namespace std::chrono;
	 using namespace std::chrono_literals;
	 using namespace std::filesystem ;
	 static string filename{expand_path(bbs_data, "/gopher-list.txt")}; 
	//RND=`echo /links | nc kamalatta.ddnss.de 70 | egrep ^1 | shuf -n 1 | cut -f3`

	struct stat result;
	if(stat(filename.c_str(), &result)==0)
	{
	auto mod_time = result.st_mtime;
	...
	}

	bool xists = exists(file);
	bool fetch_list = !xists;
	if(xists) {

	file_time_type ftime =  last_write_time(file);
	auto n = steady_clock::now();
	auto dur = n - ftime;
	//auto elapsed = std::chrono::duration_cast<ms_t>(no).count()
	}
	*/
	using std::cout;
	auto raw_contents = fetch("gopher://kamalatta.ddnss.de/1/links");
	gopher_page_t gp{decode_page(raw_contents)};
	if(gp.num_links == 0) {
		io.getchar("No links found. :( . Press any key to continue");
		//io.getchar();
		return;
	}

	string url;
	auto sel = (rand() % gp.num_links);
	for(auto& gl: gp.gopher_lines) {
		if(!(gl.type == '0' || gl.type == '1')) continue;
		if(sel == 0) {
			url = gl.url;
			break;
		}
		sel--;
	}
	assert(url.size() != 0);
	browse_gopher(io, url);
}

static const char* hdr = 
"┏━╸┏━┓┏━┓╻ ╻┏━╸┏━┓   ┏━╸╻  ╻┏━╸┏┓╻╺┳╸\r\n"
"┃╺┓┃ ┃┣━┛┣━┫┣╸ ┣┳┛   ┃  ┃  ┃┣╸ ┃┗┫ ┃ \r\n"
"┗━┛┗━┛╹  ╹ ╹┗━╸╹┗╸   ┗━╸┗━╸╹┗━╸╹ ╹ ╹ \r\n";


/*
 * gopher avail at
 * https://asciiart.website/index.php?art=animals/beavers
 * OK, it's actually a beaver, but close enough
 */

static const char* menu =
"              ___\r\n"
"           .=\"   \"=._.---.\r\n"
"         .\"         c ' Y'`p\r\n"
"        /   ,       `.  w_/\r\n"
"    jgs |   '-.   /     /\r\n"
"  _,..._|      )_-\\ \\_=.\\\r\n"
" `-....-'`------)))`=-'\"`'\"\r\n"
"b: browse the gopherhole on this site\r\n"
"r: random site\r\n"
"q: quit this page\r\n\r\n";

void handle_gopher_scr (Io& io)
{
	while(1) {
		io.hdr(hdr);
		io << menu;

		switch(io.getchar()) {
			case 'b':
				browse_gopher(io);
				break;
			case 'r':
				browse_random_site(io);
				break;
			case 'q': return;
		}
	}

}

