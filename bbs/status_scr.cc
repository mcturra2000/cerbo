#include <chrono>
#include "utils.h"


static const char* hdr = 
"┏━┓┏━╸┏━┓╻ ╻┏━╸┏━┓   ┏━┓╺┳╸┏━┓╺┳╸╻ ╻┏━┓\r\n"
"┗━┓┣╸ ┣┳┛┃┏┛┣╸ ┣┳┛   ┗━┓ ┃ ┣━┫ ┃ ┃ ┃┗━┓\r\n"
"┗━┛┗━╸╹┗╸┗┛ ┗━╸╹┗╸   ┗━┛ ╹ ╹ ╹ ╹ ┗━┛┗━┛\r\n";

static const char* menu = 
"\nr: refresh\r\n"
"q: quit\r\n";

void handle_status_scr(Io& io)
{
	static time_t now = time(0); 
	struct tm *mytime = localtime(&now);
	char buffer[25];
	strftime(buffer, sizeof(buffer), "%a %d-%b-%Y %X", mytime) ;
//static auto server_start_time = std::chrono::utc_clock::now();
	while(1) {
		io.hdr(hdr);
		string msg{std::to_string(numConns) + "\tconnections\r\n"};
		io << msg;
		msg = std::to_string(sessionsServed) + "\tsessions served since BBS start\r\n";
		io << msg;
		io << ("BBS started: "s + buffer + "\r\n");
		io << "Thread id\tStart time\n";
		io << string_sessions();
		io << menu;
		switch(io.getchar()) {
			case 'r' : break;
			case 'q' : return;
		}
	}
}
