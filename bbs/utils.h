#pragma once

#include <fstream>
#include <cassert>
#include <inttypes.h>
#include <stdint.h>
#include <cstring>
#include <string>
#include <string_view>
#include <chrono>
#include <vector>
#include <sys/socket.h>
#include <iostream>
#include <atomic>
#include <list>

using namespace std::string_literals;




enum ErrorCode { none = 0, recv_err, send_err, sess_abort,
	recv_ctl_c // user wants out straight away
	, scr_abort // abort a screen. Not considered fatal
};

static inline constexpr std::string_view bbs_data = "~/bbs-data";

inline std::atomic<int> numConns{0};
inline std::atomic<int> sessionsServed{0};
//inline int numConns = 666;

using std::string;
std::string formatted_time(const char* fmt);

class Io {
	public:
		virtual void operator<<(const char* str) = 0;
		virtual void operator<<(const string& str) = 0;
		//virtual void send(const char* str) = 0;
		//void send(const string& str) { send(str.c_str()); }
		virtual std::string getline(void) = 0;
		virtual int getchar(void) = 0;
		virtual void putchar(int c) = 0;
		int getchar(const string& prompt); // blocking
		void newscr();
		void hdr(const char* hdr);
		virtual ~Io() {};
};

void write_time (Io& io);

void make_dir(const std::string_view& dir, const std::string_view& tail = "");
std::string expand_path(const std::string_view& dir, const std::string_view& tail);

constexpr auto CR = '\r' ; // 0Dh - sent by telnet and syncterm
constexpr auto LF = '\n' ; // 0Ah - sent by telnet, but not syncterm


std::string utf8_unicode(int32_t uni);

void send_line(int conn, const std::string& msg);
void send_str(int conn, const std::string& msg);
void send_str(int conn, const char* msg);
void sendc (int conn, char c);
char recv_char (int conn);
//string recv_str (int conn, int max_len, bool echo = false);
string recv_str (Io& io, int max_len, bool echo = false);
void send_scr (int conn, const char* str);
char request_key (int conn, const string& msg);
char request_key(int conn, const char* msg);

void show_header (int conn, const char* header);
void sleep_ms(int ms);
int getkey (int conn, int timeout_ms = 1);

std::string slurp(const std::string& filename);



class Timer {
	public:
		Timer(int ms, bool trigger_instantly = true);
		bool expired();
	private:
		uint32_t m_ms;
		std::chrono::steady_clock::time_point now();
		bool first = true;
		std::chrono::steady_clock::time_point last = now();


};

void esc (int conn, int val, char c);
std::string esc (int val, char c);
void fg (int conn, int colour); // set foreground colour
std::string fg (int colour); // set foreground colour
void gotox (int conn, int x);
void gotoxy (Io& io, int x, int y);
std::string cyan(void);
std::string white(void);
std::string green(void);
std::string cls(void);
std::string home(void);

std::vector<std::string> split(const std::string& str, char sep);
std::vector<std::string> split(const std::string& str, char sep, int &n);

void browse_gopher(Io& io);

//class Sock  {
class Sock : public Io {
	public:
		//Sock(int conn) { m_conn = conn; };
		Sock() {};
		//~Io() {};
		~Sock() {};
		void operator<<(const char* str) { this->send(str); };
		//friend void operator<<(Sock& s, const char*str);
                void operator<<(const std::string& str) { this->send(str.c_str()); };
                Sock& ret(Sock& s) {return s;} ;
                std::string getline(void);
		int getchar(void) { return recv_char(m_conn); }
		void putchar(int c);

		int conn(void) {return m_conn;};
		int m_conn;
	private:
		void send(const char* str);
};

std::string run_command(std::string cmd);
std::string string_sessions(void);
char recv_one_of (Io& io, const char* opts);
