#pragma once

#include <optional>
#include <filesystem>
#include <string>

namespace fs = std::filesystem;

inline fs::path assetdir;

void assetdir_init(char *argv0);

class Asset {
	public:
		Asset(const char* filename) : m_filename(filename) {};
		std::string text();
	private:
		std::optional<std::string> data;
		std::string m_filename;
};
