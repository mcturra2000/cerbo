package main

import (
	"bufio"
	"io/ioutil"
	"fmt"
	"net"
	"strings"
	"time"
)


type ClientJob struct {
	cmd string
	conn net.Conn
}


func generateResponses(clientJobs chan ClientJob) {
	for {
		clientJob := <- clientJobs
		for start := time.Now(); time.Now().Sub(start) < time.Second; {
		}
		
		if strings.Compare("logout", clientJob.cmd) == 0 {
			clientJob.conn.Write([]byte("Bye"))
			break
		} else {
			fmt.Printf(clientJob.cmd)	
			clientJob.conn.Write([]byte(clientJob.cmd))
		}
	}
}
		
			
		


func main() {
	logo, err := ioutil.ReadFile("../bbs.txt")
	if err != nil { panic(err) }
	
	clientJobs := make(chan ClientJob)
	
	go generateResponses(clientJobs)
	
	in, err := net.Listen("tcp", ":3003")
	if(err != nil) { panic(err) }
	
	for {
	conn, err := in.Accept()
	if err != nil { panic(err) }
	
	go func () {
		buf := bufio.NewReader(conn)
		conn.Write([]byte("\033]H\033[23"))
		conn.Write([]byte("\033[33m"))
		conn.Write([]byte("Welcome to the BBS\n"))
		conn.Write([]byte(logo))
		
		for {
			conn.Write([]byte(">"))
			cmd, err := buf.ReadString('\n')
			if err != nil { break }
			cmd = strings.Replace(cmd, "\r\n", "", -1)
			conn.Write([]byte("You said:" + cmd + "\r\n"))
			
			
			clientJobs <- ClientJob{cmd, conn}
		}
		
	}()
}
}

