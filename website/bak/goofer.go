/*
gemini to gopher written in go
2022-06-03 created - only partially working
*/

package main

import (
	"bufio"
	"errors"
	"fmt"
	"net"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

const (
	CONN_HOST = "localhost"
	CONN_PORT = "7000"
	CONN_TYPE = "tcp"
	DATA_DIR  = "/home/pi/repos/cerbo/website/gemini"
	SITE	  = "\tdevilkin.chickenkiller.com\t70"
)

func main() {
	// Listen for incoming connections.
	l, err := net.Listen(CONN_TYPE, CONN_HOST+":"+CONN_PORT)
	if err != nil {
		fmt.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	// Close the listener when the application closes.
	defer l.Close()
	fmt.Println("Listening on " + CONN_HOST + ":" + CONN_PORT)
	for {
		// Listen for an incoming connection.
		conn, err := l.Accept()
		if err != nil {
			fmt.Println("Error accepting: ", err.Error())
			os.Exit(1)
		}
		// Handle connections in a new goroutine.
		go handleRequest(conn)
	}
}

func logger(text string) {
	fmt.Println(text)
}

func get_path(conn net.Conn) (string, error) {

	oops := func(msg string) (string, error) { return "", errors.New(msg) }

	buf := make([]byte, 100) // buffer to hold incoming data
	reqLen, err := conn.Read(buf) // Read the incoming connection into the buffer.
	//fmt.Println("reqLen:", reqLen)
	if err != nil { return "", err }
	//fmt.Println("buffer:", buf)
	request := string(buf[0:reqLen])
	request = strings.Replace(request, "\n", "", -1)
	request = strings.Replace(request, "\r", "", -1)

	logger( "Request: \"" + request + "\"")
	path_in := request
	//if len(fields) != 1 { return oops("bad header. Expected 1 field") }

	//path_in := fields[1]
	if path_in == "/" || path_in == "" { path_in = "index.gmi" }
	path, err := filepath.Abs(DATA_DIR + "/" + path_in)
	if err != nil { return "", err }

	fmt.Println("path requested:", path)

	// check for out-of-directory stuff
	if len(path) < len(DATA_DIR) { return oops("path name is too short") }
	if DATA_DIR != path[0:len(DATA_DIR)] { return oops("data root voilated") }

	info, err := os.Stat(path)
	if err != nil { return "", err }
	if info.IsDir() { return oops("Won't serve directories") }

	//fmt.Println("server=", fields[0])
	fmt.Println("loc=", path)
	fmt.Println("Received request ", string(buf[0:reqLen]))
	return path, err
}

func fixup_link(line string) (string) {
	re := regexp.MustCompile("\\s+")
	fields := re.Split(line, 3)
	fmt.Println(fields)
	if len(fields) != 3 {
		return "iLink format error:" +line
	}
	url := fields[1]
	ext := filepath.Ext(url)
	ext = strings.ToLower(ext)
	desc := fields[2]
	out := ""
	prefix := "0"
	switch ext {
		case ".gmi" : prefix = "1"
		case ".gif" : prefix = "g"
		case ".jpg" : prefix = "I"
		case ".htm" : prefix = "h"
		case ".html" : prefix = "h"
	}
	out = prefix + desc +"\t" + fields[1] +  SITE
	
	return out
}
func decorate_gmi(line string) (string) {
	//prefix := "i"
	//postfix := "\t\terror.host\1"
	out := ""
	if len(line) >2 && line[:2] == "=>" {
		out = fixup_link(line)
		logger("found link")
	} else {
		out = "i" + line + "\t\terror.host\t1"
	}
	return out
}

// Handles incoming requests.
func handleRequest(conn net.Conn) {
	defer conn.Close()

	oops := func(err_str string) {
		err_str = "4 " + err_str;
		fmt.Println(err_str)
		conn.Write([]byte(err_str + "\r\n"))
	}

	pathname, err := get_path(conn)
	if err != nil { oops(err.Error()) ; return }
	
	fp, err := os.Open(pathname)
	if err != nil { oops("file not found") ; return }
	defer fp.Close()


	ext := filepath.Ext(pathname)
	ext = strings.ToLower(ext)

	scanner := bufio.NewScanner(fp)
	for scanner.Scan() {		
		txt1 := scanner.Text()
		if ext == ".gmi" { txt1 = decorate_gmi(txt1) }
		//fmt.Println([]byte(txt1))
		txt1 += "\r\n"
		conn.Write([]byte(txt1))
	}

	if err := scanner.Err(); err != nil { oops(err.Error()) }
}
