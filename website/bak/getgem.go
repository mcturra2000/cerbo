/*
Retrieve a resource from gemini using Go.
Demonstrates the use of TLS. No verfication

2022-06-09	mcarter created
*/

package main

import (
	"fmt"
	"log"
	"crypto/tls"
	"io/ioutil"
	//"net/url"
	"os"
	//"strings"
	//"url"
)

func main() {
	log.SetFlags(log.Lshortfile)

	oops := func (err error) { log.Fatalln("OOPS:", err) }

	if len(os.Args[1:]) < 1 {
		fmt.Println("No file specified")
		return
	}



	urlStr := os.Args[1]
	u, err := MakeUrl(urlStr)
	if err != nil { oops(err) }
	//fmt.Println(u)

	conf := &tls.Config{
		InsecureSkipVerify: true, // don't fuss over the server's certificate
	}


	tcpStr := u.Host + ":" + u.Port ; // e.g. tozip.chickenkiller.com:1965
	//fmt.Println("TCP:", tcpStr)
	conn, err := tls.Dial("tcp", tcpStr, conf)
	if err != nil { oops(err) }
	defer conn.Close()

	full := u.Full; // e.g. gemini://tozip.chickenkiller.com:1965/index.gmi
	//fmt.Println("PATH:", full)
	n, err := conn.Write([]byte(full + "\r\n"))
	if err != nil {
		log.Println(n, err)
		return
	}

	result, err := ioutil.ReadAll(conn)
	full_response := string(result) // includes the status line
	if err != nil {
		log.Println(result, err)
		return
	}
	fmt.Println(full_response)
}
