#lang racket

;Add an entry to your tinylog
;write-up:
;gemini://blinkyshark.chickenkiller.com/2022-06-08-chirps.gmi

(require 2htdp/batch-io) ;; for read-lines
(require racket/cmdline)

(require "settings.rkt")
(require "stuff.rkt")
(require srfi/1)

(define (do-add)
  (define lines (read-lines tinylog-file))
  (define idx (list-index (λ(x) (string=? x "---")) lines))
  (define-values (before after) (split-at lines (incr idx)))
  (displayln "Write contents of chirp, using '.' on newline to finish")
  (define dstamp (get-output-from-command "date -R"))
  (define chirp (list ($+ "## " dstamp)))
  (let loop ()
    (define line (read-line))
    (unless (string=? line ".")
      (append-var! chirp line)
      (loop)))
  (append-var! chirp "")
  (append-var! chirp "")
  
  (define new-contents `(,@before ,@chirp ,@after))
  (write-file tinylog-file ($+ new-contents #:sep "\n"))
  (void))

(define (do-edit)
  (define cmd ($+ (getenv "EDITOR") tinylog-file #:sep " "))
  (system cmd))

(define (do-print)
  (define cmd ($+ "less" tinylog-file #:sep " "))
  (displayln cmd)
  (system cmd))

(define (chirp-main)
  (command-line
   #:program "chirp"
   #:once-any
   [("-a") "add-chirp" (do-add)]
   [("-e") "edit-chirps" (do-edit)]
   [("-p") "print chips" (do-print)]))

(chirp-main)

(exit)
  
