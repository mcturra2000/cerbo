BASE ?= hackinvest

DVI = $(BASE).dvi
#LTX = $(BASE).ltx
PDF = $(BASE).pdf

LTX = $(wildcard *.ltx)

.PHONY : all clean install

all : $(PDF) 



clean :
	rm -f *~ *.aux *.eps *.glo *.glg *.gls 
	rm -f *.idx *.ilg *.ind *.ist *.log *.lot *.toc
	rm -f *.dvi *.pdf
	rm -f *.out *.brf

pdf : $(PDF)
	
$(PDF) : $(LTX)
	pdflatex -shell-escape $(BASE).ltx -shell-escape
	makeindex $(BASE).idx
	makeglossaries $(BASE)
	pdflatex -shell-escape $(BASE).ltx
	#pdflatex $(LTX)

info:
	@echo $(LTX)
	
install :
	cp $(PDF) $(WWW_PDF_DIR)
