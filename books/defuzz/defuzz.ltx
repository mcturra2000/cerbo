\documentclass{article}

\usepackage{amsmath}
\usepackage{graphicx}


\newcommand{\bold}[1]{{\textbf{#1}}}



\begin{document}

\title{Simplification of defuzzification in Fuzzy Logic}
\author{Mark Carter}
\date{First published 15 September 2014\endgraf  Revised 19 November 2024}

\maketitle

\begin{abstract}
In the traditional Fuzzy Logic approach, each consequent is expressed as a fuzzy set. Typically, these sets are combined to form a complex shape whose COG (Centre of Gravity) is computed. This article argues that fuzzy consequents can be replaced by crisp consequents. By partitioning the solution space, a lot of elaborate modelling and computation is removed. An example is shown in which the simplified approach gives an answer which is very similar to a more elaborate process.
\end{abstract}

\section{Concepts in Fuzzy Logic}

This section provides an introduction to Fuzzy Logic for those who are unfamiliar with the subject\cite{intro1}.

\bold{TL} (Traditional logic) holds that a statement is either true or false in an absolute sense. \bold{FL} (Fuzzy logic) is a way of expressing a degree of truth. FL can be applied  to virtually any decision making process. Typical examples include control applications for physical devices, image processing, and whether or not to lend someone money.

FL doesn't require a model of the process, so it works well for complex systems whose mechanisms are not fully known. FL is a way to encode vague experience-based knowledge into a computable form, called \bold{rules}.

\bold{Example} A banker might take an applicant's credit rating and form a risk assessment of giving a loan. They may use the rules to determine if an applicant will pay back the loan or not:
\begin{verbatim}
If good credit rating, then low risk
If neutral credit rating, then medium risk
If bad credit then high risk
\end{verbatim}
The question now is: how does one quantify the credit ratings? In TL, we might say that the a credit score of 750 is more is good. Below that it is "not good". This is too binary, though. We want a more nuanced approach.

Take a look at figure \ref{fig:rule}. In it, membership of each bucket is dependent on project risk. The buckets are defined precisely. A risk of 80 lies exclusvely in the high set. A score of 50 lies 100\% in the normal set. A score of 70 lies partly in the normal set, and partly in the high set. This \emph{partiality} of membership is a core feature of FL. 

So, in our banking example, we might take a crisp score of 660 and produce a fuzzy variable of [0, 0.9, 0.1], which is read as the score of 660 lies 0\% in the low risk set, 90\% in the normal risk set, and 10\% in the high risk set. This is in contrast to TL, which is either wholly one set or the other. 

Defuzzification involves using a centroid calculation.

We do this by taking \bold{crisp inputs} and fuzzify them in a process known as \bold{fuzzification} using a \bold{fuzzifier}. The output is a \bold{fuzzy variable}. Many fuzzy variables can be fed into fuzzy logic rules, called \bold{inference}, producing a fuzzy variable. This variable undergoes the process of \bold{defuzzification} using a \bold{defuzzifier} to produce a crisp output.








\section{A typical problem}

Consider a Fuzzy Logic problem \cite{example} in assessing the risk of a software engineering project. Risk is classified as low, medium, and high by fuzzy sets, which are defined as shown in the figure below.

\begin{figure}
\includegraphics[width=12cm]{risk.eps}
\caption{Example rule}
\label{fig:rule}
\end{figure}


\cite{example} gives the result of evaluation the rules as follows:
\begin{equation}
\mu_{risk=low}(z) = 0.1
\label{eq:mu_low}
\end{equation}
\begin{equation}
\mu_{risk=normal}(z) = 0.2
\label{eq:mu_normal}
\end{equation}
\begin{equation}
\mu_{risk=high}(z) = 0.5
\label{eq:mu_high}
\end{equation}

Let us take these consequents as given, without worring how they are derived. Applying those results to the consequent fuzzy sets, a green area in the figure is obtained. Typically, the COG for that green area is computed to obtain a crisp value. \cite{example} calculates the answer as
\begin{equation}
\label{eq:risk_ori}
risk = 67.4\%.
\end{equation}

\section{An alternative approach}

Let us redefine the consequents, not as fuzzy sets, but as partitions of the solution. Taking out guide from the figure above, and using the intersection points of the set, define:
\begin{equation}
risk = 
\left\{
	\begin{array}{ll}
		low  & \mbox{if } 0 \leq risk \leq 30 \\
		medium & \mbox{if } 30 < risk < 70 \\
                high & \mbox{if } 70 \leq risk \leq 100
	\end{array}
\right.
\end{equation}

COGs are now easy to calculate, because they are defined in terms of rectangles. The low risk has $x=15$, corresponding to its the average of its lower and upper bound (0 and 30), with a value of ``weight'' $y=0.1 \times 30 = 3$ from equation \eqref{eq:mu_low}. Similarly, for medium risk, $x=50$, $y=0.2 \times 40 = 8$. 0.2 is the height of the box, and 40 is the width of the box (70-30). Finally, for high risk, $x=85$, $y=0.5 \times 30 = 15$.

The formula for the centre of mass for two bodies is given by \cite{gsu}:
\begin{equation}
x = \frac{m_1 x_1 + m_2 x_2}{m_1 + m_2}
\label{cog}
\end{equation}
and is easily generalised to three or more bodies. Using that equation, we therefore calculate:
\begin{equation}
risk = \frac{3 \times 15 + 8 \times 50 + 15 \times 85}{ 3 + 8 + 15} =66.2\%
\end{equation}
This compares favourably with the original result of 67.5\% given in \eqref{eq:risk_ori}.

\section{Discussion, implications, and conclusion}

In the preceeding sections, it has ben demonstrated that a crisp partitioning approach can yield similar results to traditional methods. Modelling is simpler, and one can think of changes in consequents as transitions across partitions. Models are easier to change and explore, too, as one simply has to shift the boundaries. Models should be easier to program, too, as they obviate at least some of the need of more elaborate programming toolkits. They are also conceptually simpler.

\begin{thebibliography}{1}

\bibitem{intro1}
What Is Fuzzy Logic? - Fuzzy Logic, Part 1
[online] Available at:
$<$https://www.youtube.com/watch?v=\_\_0nZuG4sTw$>$
[Accessed 19 November 2024].

\bibitem{gsu}
Center of Mass
[online] Available at:
$<$http://hyperphysics.phy-astr.gsu.edu/hbase/cm.html$>$
[Accessed 15-September 2014]

\bibitem{example}
Fuzzy Logic example.
[online] Available at:
$<$http://is.gd/K3A4mo$>$
[Accessed 15 September 2014].



\end{thebibliography}
\end{document}
