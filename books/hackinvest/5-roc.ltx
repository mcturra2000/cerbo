\chapter{Return on Capital}

\epigraph{Return on capital is the single most important thing}{--- \textup{Terry Smith}}


\section{Introduction}

In this section we explore ways of measuring returns on capital.

\section{ROA - Return on Assets}

ROA (Return On Assets) is defined as:
\begin{equation}
\text{ROA} = \text{Net Income}/\text{TA}
\end{equation}
where TA is total assets.

A high ROA suggests that a company is efficient at managing its balance sheet.

TA is the same as the sum of total liabilities and equity. So there is a theoretical mismatch in the calculation. This can be corrected by adding back the interest expense to net income.

A ROA over 5\% is generally considered good; over 20\% is excellent. ROAs should be considered within industries, as for example a software company is asset-light compared to a car manufacturer.


\section{ROCE - Return On Capital Employed}\index{ROCE}

ROCE (Return On Capital Employed) is defined as:
\begin{equation}
\text{ROCE} = \text{EBIT}/(\text{TA} - \text{CL})
\end{equation}
where TA is Total Assets, and CL is Current Liabilities.


\begin{table}[htbp]
\centering
\caption{Average 5-year ROCE by percentile for FTSE 100-Share Index (96 companies in sample). 
	Extracted Stockopedia April 2024.}
\begin{tabular}{rr}
\hline
Percentile \% & ROCE \% \\
\hline
90 & 19.1 \\
80 & 15.8 \\
70 & 14.0 \\
60 & 11.7 \\
50 & 7.4 \\
40 & 6.2 \\
30 & 4.6 \\
20 & 1.2 \\
10 & 0.3
\end{tabular}
\end{table}

\section{ROE - Return On Equity}\index{ROE}

ROE (Return On Capital Equity) is defined as:
\begin{equation}
\text{ROE} = \text{Net Profit}/(\text{Shareholder's equity})
\end{equation}

\begin{table}[!htbp]
\centering
\caption{Average 5-year ROE by percentile for FTSE 100-Share Index (96 companies in sample). 
	Extracted Stockopedia April 2024.}
\begin{tabular}{rr}
\hline
Percentile \% & ROE \% \\
\hline
90 & 32.9 \\
80 & 21.9 \\
70 & 18.7 \\
60 & 15.4 \\
50 & 11.5 \\
40 & 8.2 \\
30 & 6.7 \\
20 & 3.9 \\
10 & -0.6
\end{tabular}
\end{table}
\FloatBarrier


\section{ROITA - Return On Invested Tangible Assets}

This section discusses  \gls{ROITA}. In ``Free Cash Flow Isn't Everything'', \index{Gannon}Gannon \cite{gannonroita} said that Warren Buffett\index{Buffett} is interested in a company's return on net tangible assets, although it was unknown exactly how he calculates ``unleveraged return on tangible equity''. He reckons that
\begin{equation}
  ROITA=EBIT/NTA
\end{equation}
where NTA is Net Tangible Assets calculated as:
\begin{equation}
  NTA = (Receviables + Inventory + PPE)  - (AP +AE)
\end{equation}

where PPE is net Property, Plant and Equipment (i.e. after depraciation), AP is Accounts Payable, and AE is Accrued Expenses.

Exclude: cash and equivalents, prepayments, goodwill, `other' long term assets

Basically, you exclude non-interest bearing current assets/liabilities on the basis that it does not cost the shareholders anything. Gannon used an average of current and previous year's balance sheet figures in the denominator of the equation.

Based on figures for 2012 for WMT (Walmart), and using figures from the balance sheet for 2011 in order to create averages for NTA, we use ant EBIT of \$26.56bn and and NTA of \$100.62bn you obtain a ROITA of 26.40\%, which is a pre-tax return. Assuming a tax rate of 35\%, that would leave WMT with a normal return on \index{Unleveraged tangible equity}unleveraged tangible equity of about 17\%.

Gannon says that value investors overfocus on \index{Free cashflow}free cashflow. If a company can earn high ROITA, then it is better that it retain its earnings than distribute them as dividends. He offer the following thought experiment: let's say that an investor can earn a 10\% return on his money, ignoring taxes. He thinks that's a stretch in 2012, though - the number is probably closer to 7\%. Let's take it as 10\%, though. If a dollar in WMT's pocket cannot compound more than 15\% a year, then it's likely to be a poor investment. There is a lack of a margin of safety. You know youself better than WMT, its past is probably better than its future, and net income probably overstates owner earnings.

Generally, if you are investing in a business for its profitable future growth, you want that future growth to be at least 150\% of the annual growth you think you could provide using the same money.










\section{Expected Performance}

This is not a formula used in finance, but a concoction devised by me. So caveat emptor. Assuming that a share is fairly valued, then we might expect the total return over a year, t, to be given by
\begin{equation}
t = y + e
\end{equation}
where y is the current yield on a share, and e is the growth in earnings. Technically we should be using forward yield, which is a multiplicative factor on the earnings growth, but I consider the assumption on additivity to be acceptable. The current yield is a known quantity. We assume that the earnings growth is a function of a capital return, r (for example the ROE of a company), and the dividend cover, c. C is the ratio of earnings to dividends, and is a measure of the amount of earnings retained by the company, and presumably ploughed back into expanding earnings. The earnings growth would be:
\begin{equation}
e = r (1 - 1 /c)
\end{equation}
Putting this together:
\begin{equation}
t = y + r (1 - 1 /c)
\end{equation}
So, if a company had a yield of 5\%, a ROE of 20\%, and a dividend cover of 2, then the expected return one year out would be:
\begin{equation}
t = 5 + 20 (1 - 1 /c) = 15\%
\end{equation}

Furthermore, we might also expect the share price to exhibit a reversion to the mean of its PE. Let us call that p. Then the expanded form for total return becomes:
\begin{equation}
t = y + e + p
\end{equation}
As with yield and earnings expansion, the total return would be multiplicative with p, rather than additive. We keep with the simplifying assumption, which allows us to see the individual components of expected return.

In order to come up with a value for p, we assume that the current PE, $p_0$ returns to a normal PR, $p_n$, exponentially over a period n. Thus
\begin{equation}
p = (p_n / p_0 ) ^{1/n} -1
\end{equation}
Thus, if a share is on a PE of 13, and a fair value might be 15, which is expected to revert over a period of 2.5 years, then we would calculate:
\begin{equation}
p = (15 / 13 ) ^{1/2.5} -1 = 5.9\%
\end{equation}
So we might expect an additional 5.9\% uplift in share price performance in the first year of the share price. The value of $p_0$ is known, and  $p_n$ should be chosen sensibly. We might use an average of the PE that the stock has traded for over the last decade, but a more sensible value would be 15 - the long-term average PE of the market. Greenblatt reckons that stocks revert to fair value over a 2-3 year period, so choosing a value of $n=2.5$ seems sensible.

Putting the above together:
\begin{equation}
t = y + r (1 - 1 /c) + ((p_n / p_0 ) ^{1/n} -1)
\end{equation}
Showing the components separately gives us the opportunity to examine the likely contributions to share price performance. Below, I have tabulated some figures for companies trading in the UK, using $n=2.5$.

\begin{tabular}{|l|r|rrrr|rrr|r|}
\hline
EPIC & SP  &  r    & c    & $p_n$ & $p_0$ & y\%   & e\%    & p\% & t\% \\
\hline
BWNG & 238 & 19.9 & 2.18  & 9.75  & 8.3   & 5.5   & 10.8   & 6.6 &  22.9  \\
MCRO & 438 & 55.2 & 1.51  & 12.9  & 10.0  & 6.6   & 18.6   & 10.7 & 35.9 \\
SN.  & 592 & 20.8 & 4.37  & 15.0  & 12.3  & 1.9   & 16.0   &  8.2 & 26.1 \\
\hline
\end{tabular}

The above figures were taken at 23-May-2012. SP is the share price (in pence), and r, c, $p_0$ and y were taken from Sharelock Holmes. The implied returns (MCRO at nearly 36\%, for example), look too high, and are unlikely to be attained in practice. In chosing values for $p_n$, I have used the lower of 15 (the long-term market average PE) and the mean PE that the company has traded for in the past.



\fancy{Subsequent performance on 13 April 2013}

\textbf{Subsequent Performance} On 13-Apr-2013, the portfolio was revisited. The one-year performance of the shares listed above were: BWNG +84.0\%, MCRO +30.4\%, SN. +22.1\%. All of these shares performed better than the All-Share index: ASX +13.1\%. The results were excellent.

However, on 20 March 2024, things are not all they are cracked up to be. BWNG fell from 557p to 17p over the preceding 10 years. SN went from 927p to 1033p over the same period. On 19 March 2018, Stockopedia 
%(https://app.stockopedia.com/content/small-cap-value-report-mon-19-mar-2018-acrl-mcro-341093?order=createdAt&sort=desc&mode=threaded)
 reported that the share price of MCRO dropped from £20 to £8.82. MCRO was taken over in 2023Q1 
%(https://www.prnewswire.com/news-releases/micro-focus-shareholders-approve-all-cash-acquisition-by-opentext-301652706.html)
at an offer price of 532p (https://www.theguardian.com/business/2022/aug/26/shares-in-micro-focus-jump-90-on-news-of-opentext-5bn-canadian-takeover-deal).

The Guardian article said:
\begin{quote}
It helped reverse a slump in Micro Focus shares, which have tumbled in the wake of its £6.3bn takeover of Hewlett Packard Enterprise’s software division in 2017. Micro Focus struggled to integrate the business, which was bigger than itself and was the largest acquisition in its history. Revenue subsequently declined, with the company forced to issue a string of profit warnings in the years that followed.
\end{quote}

\textbf{Companies buying out companies much bigger than themselves often run into problems.}



If you had invested in the three shares above on 23 May 2012 until 20 March 2014 (assuming MCRO was sold at the buyout price), your mean return would be +1\%, compared to the index of +52\%.  \footnote{BWNG would be down 93\% (238p to 17p), MCRO uo 21\% (438p to 532p), and SN up 74\%. For a mean return of 1\%. Meanwhile, the ASX was up 52\% (~2765 to 4225).} Obviously owning only 3 shares is too concentrated in any case.

According to a Stockopedia report on 18 May 2019, BWNG had deteriorated significantly. It was making losses, and the 5 year average ROCE was 6.9\%. Its EV was £882m, and its market cap was £414m. With an EV over twice its market cap, it is clear that the company was overindebted at that stage. Analysts expected net profits of £60m for 2020. The company had net debt of £468m at 2019. The ratio of net debt to profit is 7.8X, which is manifestly too high. You would have had the opportunity to bail out at 146p at that point, avoiding a lot of damage.
