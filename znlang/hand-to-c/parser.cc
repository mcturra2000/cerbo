#include <cassert>
#include <string>
#include <ostream>
#include <stdio.h>
#include <iostream>
#include <cctype>
#include <functional>
#include <vector>


using namespace std;

typedef function<int(int)> fn_t;

enum toke_type { t_ws = 127, t_id, t_num, t_str, t_func };

string yytoken;
int linenum = 1;
int t_type = -1;

int yylex(void);

int id = 0;

typedef vector<string> strings_t;

strings_t data_tbl;

void add_data(string str) { data_tbl.push_back(str); }

void expect(const char* func_name, const char* str)
{
	if(str == yytoken) return;
	cerr << func_name << ": expected:" << str << ", got:" << yytoken << " in line " << linenum << endl;
	exit(1);
}

#define EXPECT(x) expect(__func__, x)


void expect_next(const char* func_name, const char* str)
{
	yylex();
	expect(func_name, str);
}


#define EXPECTNXT(x) expect_next(__func__, x)
int yylex()
{
	yytoken = "";
	static int c = -2; 
	if(c == -2) c = getchar();
	if(c <0) { return 0; }
	while(isspace(c)) { if(c== '\n') linenum++; putchar(c) ; c = getchar(); return yylex(); }

	if(isalpha(c)) { 
		while(isalnum(c)) {yytoken += c; c = getchar(); };
		if(yytoken == "func") {
			t_type = t_func;
		} else 
			t_type = t_id; 
		return 1;
	}
	if(isdigit(c)) { while(isdigit(c)) {yytoken += c; c = getchar(); }; t_type = t_num; return 1;}

	if(c== '"') {
again:
		c = getchar();
		//cout << "ho" << c ;
		assert(c != -1);
		assert(c != '\n');
		if(c== '"') {
			t_type = t_str;
			c = getchar();
			return 1;
		}
		yytoken += c;
		goto again;
	}

	yytoken = c; c = getchar(); t_type = c; return 1;

	//return 1;
	//span(isspace, t_ws) || span(isalpha, isalphanum, t_id) || span(isnum, t_num) ; // || any();
}

string do_atomic_expr(int& op)
{
	string expr;
	switch(t_type) {
		case t_str:
		       	add_data("data $string." + to_string(++id) + " = { b \"" + yytoken + "\", b 0 }");
			expr = "l $string." + to_string(id);
			//cout << "l $string." + to_string(id);
			break;
		case t_num:
			cout << "\t%expr." << ++id << " =w copy " << yytoken + "\n"; 
			expr = "w %expr." + to_string(id);
			// cout << "w " << yytoken;
			break;
		default:
		        cerr << __func__ << ": expected a string or num, got " << yytoken << " in line " << linenum << endl;
			exit(1);
	}
	expr += "";
	op = id;
	return expr;
}

string do_expr (void)
{
	int op1, op2;
	string expr = do_atomic_expr(op1);

	yylex();
	cerr << "do_expr: token: " << yytoken <<endl;
	if(yytoken == "+") {
		yylex();
		expr += do_atomic_expr(op2);
		cout << "\t%expr." + to_string(++id) + " =w add %expr." + to_string(op1)
                        + ", %expr." + to_string(op2) + "\n";
		expr = "w %expr." + to_string(id);
		yylex();
	}
	return expr;
}

// 43 ca/l -> ID ( args )
void do_call(void)
{
	assert(t_type == t_id);
	auto yytoken0{yytoken};
	EXPECTNXT("(");
	yylex();
	string expr = do_expr();
	EXPECT(")");
	cout << "\tcall $" << yytoken0 << "(" << expr << ")\n";
}

void do_stmt(void)
{
	//cout << "do_stmt\n";
	if(yytoken == "return")  {
		yylex();
		cout << "\tret " << yytoken << endl;
		EXPECTNXT(";");
		return;
	}

	do_call();
	EXPECTNXT(";");

}

void parse_function(void)
{
	yylex();
	assert(t_type==t_id);
	auto func_name = yytoken;
	//cout << "export function w $" << yytoken << "() {\n@start\n";
	EXPECTNXT("(");
	EXPECTNXT(")");
	EXPECTNXT("void");

	cout << "void " << func_name << "()" ;

	/*
	// function body
	while(yylex()) {
		if(yytoken == "}") break;
		do_stmt();
	}
	cout << "}\n";
	*/
}

/*
const char* hdr = 
"data $printnumstr = {b \"%d\", b 0}\n"
"export function w $printnum(w %num) {\n"
"@start\n"
"	call $printf(l $printnumstr, w %num)\n"
"	ret 0\n"
"}\n";
*/


void parse_top (void)
{
	while(yylex()) {
		switch(t_type) {
			case t_func :
				parse_function();
				//puts("found func");
				break;
			case t_str:
				cout << '\"' << yytoken << '\"' ; 
				break;
			default:
				cout << yytoken;
				break;
		}
		//EXPECT("fn");
	}

	for(auto& d: data_tbl) 
		cout << d << endl;
}

int main() {
	//cout << hdr ;
	parse_top();
	return 0;
}
