#include "zn.h"

extern int yyparse (void);


void dbg(const char* str)
{
	cout << str << endl;
}

int store_src(srcval_t srcval)
{
	srcvec.push_back(srcval);
	return srcvec.size() -1 ;
}

int store_string(const char* str)
{
	cout << "store_string: " << str << endl;
	stored_strings.push_back(str);
	for(auto s: stored_strings) cout << s << endl;

	return stored_strings.size() -1;
}

void decode(ConstInt obj) { cout << "ConstInt: " << obj.val << endl; }

void decode(string obj) {
	cout << "decode string\n";
}

void decode(FuncDef obj) {
	cout << "FuncDef: " << stored_strings[obj.name] <<
       		", comp_stmt: " << obj.cmpd_stmt << endl;
}
void decode(Return obj)
{
	cout << "Return: " << obj.expr << endl;
}
void decode(StmtList obj)
{
	cout << "StmtList: list: " << obj.list << ", stmt: " << obj.statement << "\n";
}

void decode(TransUnit obj) 
{ 
	cout << "TransUnit: trans_unit: "<<  obj.trans_unit <<
		", extn_decl " << obj.extn_decl << endl;
}

template <typename T>
void decode(T obj)
{
	cout << "decode: umimplemented "  << typeid(T).name() << endl;
}

void decode_src_tree(void)
{
	cout << "DECODING SOURCE TREE ...\n";
	int idx = 0;
	for(auto& el: srcvec) {
		cout << idx++ << "\t" ;
		std::visit([](auto&& obj) {decode(obj); }, el);
	}
	cout << "... finished\n";
}

void eval_idx(int idx);


void eval(ConstInt obj)
{
	cout << obj.val;
}

void eval(FuncDef obj)
{
	auto func_name = stored_strings[obj.name];
	cout << "export $" << func_name << "() {  // eval: FuncDef\n";
	eval_idx(obj.cmpd_stmt);
	cout << "}\n";
}

void eval(Return obj)
{
	cout << "\tret ";
	if(obj.expr >=0 ) eval_idx(obj.expr);
	cout << "\n";
}

void eval(StmtList obj)
{
	//cout << "//eval: StmtList\n";
	eval_idx(obj.list);
	eval_idx(obj.statement);
}

void eval(TransUnit obj)
{
	cout << "eval: TransUnit\n";
	eval_idx(obj.trans_unit);
	eval_idx(obj.extn_decl);
}

	template <typename T>
void eval(T obj)
{
	cout << "eval:unimplmented: " << typeid(T).name() ;
}

/*
   template <typename T>
   void eval(T obj)
   {
   cout << "eval: generic. You must complete.\n";
   }
   */

void eval_idx(int idx)
{
	//srcval_t top = srcvec[index];
	std::visit([](auto&& obj) {eval(obj); }, srcvec[idx]);
}

int main()
{
	int status = yyparse();
	assert(status == 0);

	if constexpr(true) decode_src_tree();

	// root node is yylval
	cout << "top_index=" << top_index << endl;
	eval_idx(top_index);
	//eval(top);

}
