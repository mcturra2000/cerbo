#pragma once

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <variant>
#include <vector>

using std::string;
using namespace std::literals::string_literals;
using namespace std;

extern int yylval;
typedef vector<string> strings_t;

inline int top_index; // where the top of the program is

//typedef struct {int val; } ConstInt; // a constant integer
class ConstInt {
	public: 
		ConstInt(int i) : val(i) {};
		int val;
};
typedef struct {int expr;} Return; // -1 if no expression

/*
class FuncDef {
	public:
		FuncDef(int name, int compound_statement) : m_name(name), m_compound_statement(compound_statement) {};
	//private:
		int m_name, m_compound_statement;
};
*/
typedef struct {int name; int cmpd_stmt;} FuncDef;

/*
class StmtList {
	public:
		StmtList(int statement_list, statement) m_statement_list(statement_list), m_statement(statement) {};
		int m_statement_list, m_statement;
};
*/
typedef struct {int list; int statement; } StmtList;
/*
class TransUnit {
	public:
		TransUnit(int translation_unit, int external_declaration) : m_translation_unit(translation_unit), 
		m_external_declaration(external_declaration) {};
		int m_translation_unit, m_external_declaration;
};
*/
typedef struct {int trans_unit; int extn_decl; } TransUnit;

// Yacc is too comilicated to figure out in terms of variant values
// so we just store everything in a vector and refer by index
typedef variant<string, ConstInt, FuncDef, Return, StmtList, TransUnit> srcval_t;
typedef vector<srcval_t> srcvec_t;
inline srcvec_t srcvec; // source vector
int store_src(srcval_t srcval);


// deprecate
inline strings_t stored_strings; // see store_string()
int store_string(const char* str);

void dbg(const char* str);
