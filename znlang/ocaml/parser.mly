%{
        open Parser
%}

%token FN
%token ID

%start<string> prog

%%

prog:
        | FN id=ID { id }
        ;

        
%%
