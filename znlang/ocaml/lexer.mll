(* scanner for a toy language *)

{
  open Parser
}

let digit = ['0'-'9']
let id = ['a'-'z'] ['a'-'z' '0'-'9']*

rule zinc = parse
        | [' ' '\t' '\n'] { zinc lexbuf }
        | digit+ as inum   { printf "integer: %s (%d)\n" inum (int_of_string inum);  }
        | digit+ '.' digit* as fnum { printf "float: %s (%f)\n" fnum (float_of_string fnum); }
        | "fn" { FN }
        | id as text { ID (text) }
        | eof		{ End_of_file }

{
        }

