grammar G {
	token TOP { (<func-decl> | <noise>)*   { print "top" }}
	rule func-decl { func  <id> { print "found func" }}
	token id	{ <[a..zA..Z_]> <[a..zA..Z_0..9]>*  { print "id is $/"; }}
	token noise { . {print $/ } }
}


class prog-actions
{
	method TOP ($/) {
}

my $prog = slurp "prog.zn";
my $m = actions.parse($prog, actions => prog-actions.new);
