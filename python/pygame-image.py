import pygame
import pygame as py
from pygame.locals import*

py.init();

w = 600
h = 600
screen = pygame.display.set_mode((w, h))
img = pygame.image.load("pluto1.bmp")

def render(x1, y1) :
	screen.blit(img, (x1, y1))

running = True
while running:
	screen.fill((0, 255, 0))
	render(0, 0)
	py.key.get_pressed() 
	for ev in py.event.get():
		if ev.type == pygame.KEYDOWN:
			running = False
	py.display.flip()
