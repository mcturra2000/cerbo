;; (use-modules (cerbo stats))

(define-module (cerbo stats)
  #:use-module (texinfo reflection)
  #:use-module (srfi srfi-42) ; for do-ec
  #:export (srfi rank what /2 /. circa cv displayln flatten gain% mid mid-odd median mean
                 growth growth% g10 g20 pc sd
                 retform ddiff ydiff))


(define (rank nums less)
  "Ranks numbers. (rank '(11 13 12) <) => (0 2 1)"
  (define iotas (iota (length nums)))
  (define (less1 a b) (less (car a) (car b)))
  (define lst (map  cons nums iotas))
  (define (less2 a b) (< (cddr a) (cddr b)))
  ;(write lst)
  (set! lst (sort lst less1))
  (set! lst (map cons iotas lst))
  (set! lst (sort lst less1))
  (set! lst (sort lst less2))
  (map car lst))



(define-syntax srfi
  (syntax-rules ()
    "Use an srfi module. E.g. (srfi srfi-111)"
    ((_ n)
     (use-modules (srfi n)))))

(define (flatten lst)
  (let loop ((lst lst) (acc '()))
    (cond
     ((null? lst) acc)
     ((pair? lst) (loop (car lst) (loop (cdr lst) acc)))
     (else (cons lst acc)))))
     
(define (display-module-interface module)
  (define names '())
  (module-for-each
    (lambda (name variable)
      (let* ((name-length (string-length (symbol->string name)))
	     (value (variable-ref variable))
	     (documentation (object-stexi-documentation value)))
        (set! names (cons name names))
	     
	;(format #t "~a\n" name)
	#t))
    (resolve-interface module))
  names)

(define (cv . nums)
	"Co-efficient of variation, %"
	(* 100.0 (/ (apply sd nums) (apply mean nums))))
	
(define (displayln x)
  (display x)
  (newline))


(define (sd . nums)
  "Sample std deviation. (Tested). See 08144."
  (define n (length nums))
  (define x1 (apply + nums)) 
  (define x2 (apply + (map * nums nums)))
  (define x3 (- (* n x2) (* x1 x1)))
  (define x4 (/ x3 n (- n 1)))
  ;(displayln x2)
  ;(displayln x3)
  ;(displayln n)
  (sqrt x4))

(define circa exact->inexact)

(define (pc num denom)
  "Percentage of num by denom. E.g. (pc 10 50) => 20.0"
  (exact->inexact (* 100 (/ num denom))))

(define (what)
  "Display all the procs exported"
  (define names (display-module-interface '(cerbo stats)))
  (set! names (map symbol->string names))
  (set! names (sort names string<?))
  (do-ec (:list x names) (displayln x)))


(define (gain% from to)
  "Percentage gain. E.g. (gain% 100 110) => 10.0"
  (exact->inexact (* 100 (- (/ to from) 1))))

(define (retform roce div-cov div-yield)
  (define r1 (* roce (- 1 (/ 1 div-cov))))
  (define r2 (+ r1 div-yield))
  (values r2 r1  div-yield))

(define (/2 n) (/ n 2))

  
(define (mid lst) 
  ;(display "mid-even")
  (define r1 (/2 (length lst)))
  (define r2 (- r1 1))
  (/2 (+ (list-ref lst r1) (list-ref lst r2))))

(define (mid-odd lst)
  ;(display "mid-odd")
  (list-ref lst (/ (- (length lst) 1) 2)))

(define (median . nums)
  (set! nums (flatten nums))
  (define n1 (sort nums <))
  (define med (if (odd? (length n1)) (mid-odd n1) (mid n1)))
  (circa med))

	
(define (mean . nums)
  (circa (/ (apply + nums) (length nums))))

(define (growth from to yrs)
  (expt (/ to from) (/ 1.0 yrs)))

(define (growth% from to years)
  "Growth in %. E.g. (growth% 100 121 2) => 10.0"
  (exact->inexact (* 100 (- (growth from to years) 1))))

(define (g10 from to) (growth% from to 10))
(define (g20 from to) (growth% from to 20))


(define (conv x) (car (mktime (car (strptime "%Y-%m-%d" x)))))
(define (diffs a b) (- (conv a) (conv b)))

(define* (ddiff from #:optional to)
"find the differences, in days, between two dates, e.g.
(ddiff \"2022-01-01\" \"2023-05-21\") \n 
omit 2nd argument to refer to current date"
	 (unless to (set! to (strftime "%Y-%m-%d" (localtime (current-time)))))
	 (round (/ (diffs to from) 60 60 24)))


(define* (ydiff from #:optional to)
	 "Same as ddiff, but returns diff in years"
	(/ (ddiff from to) 365.0))

(define (/. . lst)
	(circa (apply / (flatten lst)))) 
