## csv

https://github.com/NalaGinrut/guile-csv/tree/master

https://github.com/NalaGinrut/guile-csv.git

I changed the module from "csv csv" to "cerbo csv"

## files

```
(use-modules (cerbo files))
(read-lines "myfile.txt")
```

## macros

```
; will print out 10 to 15 inclusive
(for i 10 15 1 ; var from to step
     (format #t "~a\n" i))



(for-list var mylist
          (display var) 
          (display "\t"))
          
(define x 1)
(incr x) ; by 1
(display (incr x 2)) ; by 2          
```
