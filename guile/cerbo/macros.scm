(define-module (cerbo macros)
	#:export (for for-list incr))

(define-syntax incr
  (syntax-rules ()
    ((incr x by)
     (begin     
       (set! x (+ by x))     
       x))
    ((incr x)
     (incr x 1))))
	
(define-syntax for-list
  (syntax-rules ()
    ((for-list var lst expr ...)
     (for-each (lambda (var) expr ...)
               lst))))

(define-syntax for
  (syntax-rules ()
    ((for var a b step expr ...)
     (let ((var a))     
       (while (<= var b)      
              (begin expr ...)        
              (incr var step))))))
	
