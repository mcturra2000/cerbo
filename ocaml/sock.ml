(* 2025-02-10 Created Something of a gopher client *)

open Sys;;
open Unix;;
open In_channel;;

(*
let client  =
        print_endline "client started";
        if Array.length Sys.argv < 3 then begin
                prerr_endline "Usage: client <host> <port>";
        exit 2;
        end;
      let server_name = Sys.argv.(1)
      and port_number = int_of_string Sys.argv.(2) in
      let server_addr =
              try (gethostbyname server_name).h_addr_list.(0)
              with Not_found ->
                      prerr_endline (server_name ^ ": Host not found");
         exit 2 in
      let sock = socket PF_INET SOCK_STREAM 0 in
      connect sock (ADDR_INET(server_addr, port_number));
      print_endline "client says bye"
*)
       
(*        
let get_sockaddr host port =
        let saddr =
                try (gethostbyname host).h_addr_list.(0)
                with Not_found ->
                        prerr_endline (host ^ ": Host not found");
         exit 2 in
        ADDR_INET(saddr, port)
        
        get_sockaddr "localhost" 7000;
*)


let rec print_lines lines = 
        match lines with
        | [] -> ()
        | h :: t -> print_endline h ; print_lines t



let gopher host port = 
        let he = gethostbyname host in (* might not work *)
        let hal = he.h_addr_list.(0) in (* first inet_addr eg 192.168.1.105  *)
        let saddr = ADDR_INET( hal, port) in
        let (i, o) = open_connection saddr in
        output_string o "/index.gmi\r\n";
        flush o;
        let lines = input_lines i in
        print_lines lines; 
        (* let _ = flush i in *)
        close_out o;
        (* let _ = shutdown_connection i in *)
        ()
     
        
        
        let () = gopher "beelink.local" 70

let foo =
        print_endline "foo definitely called";
        print_endline "foo called";

        print_endline "Camels are bae" 
        (* Printf.printf "wazzup" *)


        (*
let () = 
        foo  ;
         client ; 
        print_endline "Camels are bae"
*)

let () = 
        print_endline "something"
